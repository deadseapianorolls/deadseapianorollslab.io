# deadseapianorolls

Uses [Zola](https://www.getzola.org/) for static site generation.

## legal

[![CC0 (public domain)](./static/img/cc-zero.svg
"CC0 (public domain)")](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, all copyright and related or neighboring
rights to [deadseapianorolls](https://deadseapianorolls.gitlab.io/) (the
contents and works contained in this repository) have been waived. For more
information, see <https://creativecommons.org/publicdomain/zero/1.0/>.
