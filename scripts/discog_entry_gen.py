#!/usr/bin/env python3

import collections, os, re, subprocess, sys

FILE_EXTENSION_RE = re.compile(r".+\.(flac|opus|mp3|ogg|wav|m4a|aac)")
ALBUM_RE = re.compile(r"^     *ALBUM +: (.+)$", flags=re.M | re.I)
TITLE_RE = re.compile(r"^     *TITLE +: (.+)$", flags=re.M | re.I)
TRACK_RE = re.compile(r"^     *track +: ([0-9]+)$", flags=re.M | re.I)
DURATION_RE = re.compile(
    r"^  Duration: ([0-9]{2}):([0-9]{2}):([0-9]{2}).([0-9]{2}),",
    flags=re.M | re.I,
)


### Data collection ###

tracks = collections.defaultdict(list)

for dirpath, dirnames, filenames in os.walk(sys.argv[1]):
    for filename in filenames:
        if not FILE_EXTENSION_RE.match(filename):
            continue

        filepath = os.path.join(dirpath, filename)
        if not os.path.isfile(filepath):
            continue

        ffmpeg_completion = subprocess.run(
            ["ffmpeg", "-i", filepath],
            stdout=subprocess.PIPE,  # Setting `stdout` and `stderr` like this
            stderr=subprocess.STDOUT,  # combines them into one.
            encoding="UTF-8",
        )
        stdout = ffmpeg_completion.stdout

        album_match = ALBUM_RE.search(stdout)
        title_match = TITLE_RE.search(stdout)
        track_match = TRACK_RE.search(stdout)
        duration_match = DURATION_RE.search(stdout)

        album = album_match.group(1)
        title = title_match.group(1)
        track = int(track_match.group(1))
        hours = int(duration_match.group(1))
        minutes = int(duration_match.group(2)) + 60 * hours
        seconds = int(duration_match.group(3))
        hundredths = int(duration_match.group(4))
        duration = (minutes, seconds, hundredths)

        slots = max(track - len(tracks[album]), 0)
        for _ in range(slots):
            tracks[album].append(None)

        if tracks[album][track - 1]:
            print(
                f'Duplicate track number found in "{album}".\n',
                file=sys.stderr,
            )
        tracks[album][track - 1] = (title, duration)


### Output ###

out = ""

for album_title, album_tracks in tracks.items():
    track_count = len(album_tracks)
    album_tracks = [t for t in album_tracks if t]
    if len(album_tracks) != track_count:
        print(
            f'Track numbering is probably incorrect for "{album_title}". '
            + "Maybe some track(s) are missing?\n",
            file=sys.stderr,
        )

    duration_hundredths = sum(
        60 * 100 * m + 100 * s + h for _, (m, s, h) in album_tracks
    )
    minutes = duration_hundredths // (60 * 100)
    seconds = duration_hundredths % (60 * 100) // 100
    hundredths = duration_hundredths % 100
    # round half even
    if hundredths > 50 or hundredths == 50 and seconds % 2 == 1:
        seconds += 1
        if seconds >= 60:
            seconds -= 60
            minutes += 1

    album_title_esc = album_title.replace('"', '\\"')
    out += f'[[releases]]\ntitle = "{album_title_esc}"\nyear = 0\ncover = ""\n'
    out += f'duration = "{minutes}:{str(seconds).zfill(2)}"\n\n'

    for track_title, (m, s, h) in album_tracks:
        # round half even
        if h > 50 or h == 50 and s % 2 == 1:
            s += 1
            if s >= 60:
                s -= 60
                m += 1

        out += f'    [[releases.tracks]]\n    title = "{track_title}"\n'
        out += f'    duration = "{m}:{str(s).zfill(2)}"\n'

    out += "\n"

print(out[:-1], end="")
