+++
title = "about"
description = "about deadseapianorolls"
+++

deadseapianorolls (dspr) is a site that [digitally
archives](https://en.wikipedia.org/wiki/Digital_preservation) musical
[records](https://en.wikipedia.org/wiki/Digital_recording) that cannot be
obtained from the source. Here, &ldquo;source&rdquo; refers to the artists,
[bands](https://en.wikipedia.org/wiki/Musical_ensemble) and/or [record
labels](https://en.wikipedia.org/wiki/Record_label) themselves. This includes
obtaining physical copies as well as digital ones; so, deadseapianorolls does
not provide records that are in print, nor does it provide ones that have
digital copies available from the record labels and/or artists/bands. Instead,
when presenting [discographies](https://en.wikipedia.org/wiki/Discography),
deadseapianorolls refers its readers to the places where they can legitimately
obtain those records that are available to own in one form or another.

deadseapianorolls is organized by artist/band, each one having its own entry
that contains all of their (known) discography and
[bootleg](https://en.wikipedia.org/wiki/Bootleg_recording) material.

### help is strongly encouraged

It is strongly encouraged that anyone who finds any of the following:

- Missing/absent recordings
- Inferior copies of recordings
- Incorrect, out-of-date, or missing information

&hellip;does [contact deadseapianorolls](/contact) with copies of missing
recordings, superior copies of recordings, and corrections/additions. Also note
that filling in missing image entries (usually, [cover
art](https://en.wikipedia.org/wiki/Cover_art)) and providing superior copies of
images is similarly appreciated. Because deadseapianorolls is, by nature, a
digital archive, **exact** [CD](https://en.wikipedia.org/wiki/Compact_disc)
[rips](https://en.wikipedia.org/wiki/Ripping) from **officially released** CDs
are preferred (as well as official internet releases in lossless formats).
However, in the unideal case, other sources are acceptable to use instead, e.g.
[vinyl](https://en.wikipedia.org/wiki/Gramophone_record) &ldquo;rips&rdquo;,
and other, more lossy media. deadseapianorolls places an emphasis on retaining
copies that are as close to &ldquo;the original&rdquo; as possible, so sloppy
methods and [lossy
compression](https://en.wikipedia.org/wiki/Lossy_compression) should be avoided
when at all possible. For compression, instead prefer
[FLAC](https://en.wikipedia.org/wiki/FLAC) for audio,
[WEBP](https://en.wikipedia.org/wiki/WebP) with [the `-lossless`
option](https://developers.google.com/speed/webp/docs/cwebp#options) (or just
[PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics), for
compatibility) for images, and [LZMA][lzma] (or other similar general-purpose
[lossless compression](https://en.wikipedia.org/wiki/Lossless_compression)
algorithms) for everything else.

### distribution listings

Each distribution listed on deadseapianorolls contains information about, among
other things, the formatting/encoding and source for that distribution. Not all
formats and encodings are created equal, and likewise, not all sources are
created equal. To aid comprehension, formats/encodings and sources are
color-coded using a set of three colors:

- White: for formats, lossless; for sources, official.
- <span class="lossy">Yellow-orange: for formats, lossy; for sources,
  unofficial.</span>
- <span class="very-lossy">Red: for formats, very lossy (for MP3 we arbitrarily
  define this as &ldquo;average bitrate less than 200kb/s&rdquo; &mdash; for
  other formats, this will be different, considering that MP3 is very
  inefficient); for sources, dubious.</span>

Do note that just because a format/encoding is higher-quality on its own, that
_does not necessarily_ mean that the actual content is higher-quality. That is
partly why sources are listed and are ranked according to these three tiers as
well. A lossless format from a <span class="dubious">dubious</span> source
could easily have been re-encoded from some <span class="lossy">lossy</span>
encoding, or any number of other mishaps could have occurred.

Distributions that have a source listed as &ldquo;\[net\]&rdquo; are sourced
from unknown, anonymous, and/or no-longer-extant internet sources.

Variable bitrate (VBR) distributions&rsquo; bitrates are listed as averages,
with an indication that the bitrate is variable.

Distributions that contain video are marked with the following character:
&#x1f4f9; (`U+1f4f9`).

### sources for discographical (and artist) information

The discographical information (release titles, release dates, track listings,
&amp;c.), as well as some of the artist-specific information (band membership,
dates of formation, &amp;c.) presented on deadseapianorolls is typically
sourced from some combination of the following resources:

- [MusicBrainz](https://musicbrainz.org/)
- [Discogs](https://www.discogs.com/)
- [Rate Your Music](https://rateyourmusic.com/)
- [Last.fm](https://www.last.fm/)
- [bandcamp](https://bandcamp.com/)

### Hornbostel&ndash;Sachs

[Hornbostel&ndash;Sachs
classifications](https://en.wikipedia.org/wiki/Hornbostel-Sachs) for some
musical instruments are included using HTML `<data>` elements (if you are using
a visual browser, this should show up as the main text having a dotted
underline, which you can hover over to reveal the associated title text). These
will be given as &ldquo;H&ndash;S&rdquo; followed by a series of numbers (and
possibly also dots, hyphens, and/or plus signs). Here is an example:

> The {{ hs(name="piano") }} is a kind of chordophone.

### legal

The intent of deadseapianorolls is to _not_ distribute recordings that can
otherwise be obtained from the artist/band/record label itself. That way, no
one&rsquo;s [revenue](https://en.wikipedia.org/wiki/Revenue) is lost, and
credit is always given wherever credit is applicable. In addition,
deadseapianorolls is entirely
[nonprofit](https://en.wikipedia.org/wiki/Nonprofit_organization); indeed, it
is, furthermore, entirely zero revenue. deadseapianorolls does not accept
[donations](https://en.wikipedia.org/wiki/Donation), does not
[advertise](https://en.wikipedia.org/wiki/Advertising), and does not have any
other forms of revenue. deadseapianorolls exists solely for educational
purposes, and for the advancement of [information preservation][preservation].

That being said, if you are a legitimate
[copyright](https://en.wikipedia.org/wiki/Copyright) holder of any of the
[works](https://en.wikipedia.org/wiki/Creative_work) that are
[linked](https://en.wikipedia.org/wiki/Hyperlink) to by deadseapianorolls, and
you would like to have one or more of said work(s) removed from
deadseapianorolls, **please [just contact deadseapianorolls](/contact) as
appropriate, and the work(s) will be expunged as soon as possible**.

To the extent possible under law, all copyright and related or neighboring
rights to [deadseapianorolls](https://deadseapianorolls.gitlab.io/) have been
waived. For more information, see
<https://creativecommons.org/publicdomain/zero/1.0/>.

For access to the source, [see
here](https://gitlab.com/deadseapianorolls/deadseapianorolls.gitlab.io).

[![CC0 (public domain)](/img/cc-zero-alt.svg
"CC0 (public domain)")](https://creativecommons.org/publicdomain/zero/1.0/)

#### deadseapianorolls logo

[The deadseapianorolls logo](/img/deadseapianorolls.svg) is due to Andrew Doane
from the Noun Project ([CC BY
3.0](https://creativecommons.org/licenses/by/3.0/)).

[lzma]: https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Markov_chain_algorithm
[preservation]: https://en.wikipedia.org/wiki/Preservation_(library_and_archival_science)
