+++
title = "source"
description = "source code for deadseapianorolls"
+++

To the extent possible under law, all copyright and related or neighboring
rights to [deadseapianorolls](https://deadseapianorolls.gitlab.io/) have been
waived. For more information, see
<https://creativecommons.org/publicdomain/zero/1.0/>.

For access to the source from which this website is built, [see
here](https://gitlab.com/deadseapianorolls/deadseapianorolls.gitlab.io).

[![CC0 (public domain)](/img/cc-zero-alt.svg
"CC0 (public domain)")](https://creativecommons.org/publicdomain/zero/1.0/)

### deadseapianorolls logo

[The deadseapianorolls logo](/img/deadseapianorolls.svg) is due to Andrew Doane
from the Noun Project ([CC BY
3.0](https://creativecommons.org/licenses/by/3.0/)).
