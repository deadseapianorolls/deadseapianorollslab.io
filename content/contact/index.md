+++
title = "contact"
description = "contact deadseapianorolls"
+++

<address>
deadseapianorolls<abbr
title="to be replaced with an “at” sign (U+0040)">ⓐ</abbr>tuta<abbr
title="to be replaced with a “dot”/“period”/“full stop” (U+002e)">⋅</abbr>io
</address>

And/or, if appropriate, [file an issue][issues] or even [a merge
request][merge-requests].

[issues]: https://gitlab.com/deadseapianorolls/deadseapianorolls.gitlab.io/-/issues
[merge-requests]: https://gitlab.com/deadseapianorolls/deadseapianorolls.gitlab.io/-/merge_requests
