+++
title = "TTNG"
date = 2020-04-25

[taxonomies]
genres = [
    "math pop",
    "math rock",
    "experimental rock",
    "post-hardcore",
    "midwest emo",
    "indie rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["t"]
first-release-dates = ["2003"]
release-dates = [
    "2003",
    "2006",
    "2007",
    "2008",
    "2009",
    "2011",
    "2013",
    "2016",
    "2018",
]
labels = [
    "Sargent House",
    "Big Scary Monsters",
    "Yellow Ghost Records",
    "Stiff Slack",
    "Little Elephant",
    "Klee",
    "Faux Discx",
    "Disc Makers",
]
countries = ["England"]
provinces = ["Oxfordshire, England"]
areas = ["Oxford, Oxfordshire, England"]

[extra]
last_edited = "2020-04-27"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>TTNG</b> (<abbr title="formerly known as">f.k.a.</abbr> <b>This Town Needs
Guns</b>) is an [English](/countries/england) {{ genre(name="math pop") }} band
that has been around since at least {{ time(t="2002") }}, although
they have undergone several lineup changes since then. {{ hs(name="Guitar")
}}ist <b>Tim Collis</b> has, strictly speaking, been the only constant member
of the band since {{ time(t="2002") }}, although his brother, {{
hs(name="drummer") }} <b>Chris Collis</b>, has been a member for the entire
time that <b>TTNG</b> has sounded like, well, <b>TTNG</b>.

<b>TTNG</b> are a staple of the {{ genre(name="math rock") }} genre, and as
such need little introduction. Though they never quite achieved the same kind
of mainstream success as fellow [Oxford](/areas/oxford-oxfordshire-england) {{
genre(name="math pop") }}pers <b>Foals</b>, they are about as widely popular as
you can be while still sticking to {{ genre(name="math rock") }}. Their origins
as a fairly standard early-2000s {{ genre(name="post-hardcore") }} act into
their maturity as a {{ genre(name="math rock") }}/{{ genre(name="math pop") }}
act, combined with some influence from {{ genre(name="midwest emo") }}, {{
genre(name="post-rock") }}, and the [Spanish](/countries/spain)-tinged {{
hs(name="guitar") }} stylings of <b>Tim Collis</b>, formed <b>TTNG</b> into the
ferociously unique, creative, and mature band that we know them as now.

If you aren&rsquo;t already familiar with <b>TTNG</b>, it is worth noting that
&ldquo;Animals&rdquo; was the album that launched <b>TTNG</b>&rsquo;s worldwide
success; it is, however, also highly recommended that you check out their other
works, including &ldquo;Disappointment Island&rdquo;, &ldquo;This Town Needs
Guns&rdquo;, &ldquo;Adventure, Stamina &amp; Anger&rdquo;,
&ldquo;13.0.0.0.0&rdquo;, &amp;c.

### notes on the distributions

The tracks on &ldquo;First Demo \[unreleased tracks\]&rdquo; were slated to be
released as part of the &ldquo;First Demo&rdquo;, but have, to this day, never
been officially recorded nor released. For this reason, they are placed in
their own entry of the discography.

The first two tracks of &ldquo;\[unreleased\]&rdquo; (dating to {{ time(t="2002") }}) and both tracks of &ldquo;First Demo \[unreleased
tracks\]&rdquo; are sourced from [Tom McEggs
Dennis](https://www.youtube.com/channel/UC3Sg5pzRMu5fbs72MhLWhMA)&rsquo;s
archival of [the &ldquo;Classic This Town Needs Guns&rdquo; MySpace
page](https://myspace.com/classicttng). There is a textual document as well as
a series of images associated with this archival, and those are reproduced as
part of the &ldquo;First Demo \[unreleased tracks\]&rdquo; distribution. For
the sake of [redundancy][redundancy] and ease of access, these auxiliary
documents will be reproduced here as well:

<details>
<summary>Classic TTNG Biography.txt</summary>

    Source of info, music, and photos:
    http://www.myspace.com/classicttng

    I am not the owner of this myspace page nor am I affiliated with it, but if you have any TTNG songs that would be deemed "classic" make sure you probably can't send them
    to whomever runs that myspace, as they havn't logged into it for over 5 years. I'd appreciate it if you could send the file or mediafire link to my email address
    (tommy_the_c@hotmail.com). It should be okay, as these songs aren't for sale anywhere. I will also make sure that anyone who wants these songs will recieve them via
    amail or by sending them a mediafire link. Thank you.

    Bio:
    ..This Town Needs Guns
    circa 2002-2003!..
    This myspace is dedicated to long-forgotten material by This Town Needs Guns. If you have anything to add, please send a message! For now, here's what there is...
    Two live tracks from One Mic at Oxford Brookes SU in 2002 (Better & Purge). This was back when This Town Needs Guns were a 3-piece.

    Plus two tracks released on a demo CD, recorded in November 2003 (I Will & All That's Between Us). Here's what Stu had to say about it:
    "We've been busy in the studio for the last couple of weeks trying to put a demo together. The whole recording process is taking much longer than we thought, although
    we're hoping to have finished recording by the end of the week. Hopefully then, we'll be able to get alot more gigs. This months special thanks (as there seems to be one every month)
    has to go out to Alex for helping us record the demo, without him we wouldn't have a bloody clue!" (01/12/2003)

    Photos are from the Eights Bar gig at Wheatley SU in November 2003. Here is what Stu had to say about that gig afterwards:
    "We played a last minute rushed gig last night in Wheatley. Sorry to everyone who we weren't able to tell the gig about (especially Bridget who is very upset), but in
    all honesty you didn't miss our greatest performance! We decided before going that it was about time we had some fun onstage. Unfortunately, 'fun' resulted in us not sounding our best
    (this however was expected, due to the underpowered sound rig at the 8's bar). The technical problems that have been plaguing us since we started continued, when my amp's channel switch
    got stuck and l was forced to play the last song, entirely distorted, but 'oh well,' l suppose its just unlucky. Not too sure what people thought of it. There were alot of funny looks and
    a couple of people walked out, but that is to be expected when you are playing 'heavy' music after a night of acoustic performances. We were asked to play again by the organiser (thanks for
    putting us on Keith). Once again thanks to everyone who played before us, but we really need to get some 'proper' gigs now." (05/11/2003)

    In early 2004, the band's lineup changed, to the point where Stu was considering changing the name of the band! Here's what he had to say about it:
    "Ok, so someone please remind me to never say anything about reporting 'good news' ever again. In the last month, two members of the band have now left. We had to cancel the gig on the
    8th of February [2004], but Simon and Ian agreed to play one last gig on the 29th of January. The gig was well shoddy, with the sound being pretty attrocious. Mics cut out, strings
    broke, and drum pedals stuck, but it was all in the name of fun. We got a fair review in OBscene, which we can't complain about."(27/02/2004)

    But this did turn out to be 'good news' in the end, as the band started to play some 'proper' gigs, and won the Battle of the Bands competition at the Wheatsheaf 3 months later! Nightshift
    soon stopped describing them as "ramshackle, run-down indie trash" (!) and said they were "heading in the right direction," which obviously proved to correct, as in 2006, DiS said, "Oxford is
    soon to be remembered as the city where this precocious young act began its rise to success" ... :)

    .. ..
    Member Since:
    December 17, 2006
    Members:
    2002-2003: Stu (vocals, guitar), Tim (guitar), Welsh Ian (bass), Simon (drums)
    Influences:
    I remember when Stu first heard "Stress On The Sky" by Biffy Clyro... his response was, "OMG this is what I want my band to sound like!" :)
    Sounds Like:
    Have a listen and see for yourself! Definitely not like "Incubus on acid," as some Oxford University reviewer described them after playing a Battle of the Bands heat in spring 2004.

</details>

* [Excited fans, Wheatley, {{ time(t="Nov
  2003", dt="2003-11") }}](Excited_fans-_Wheatley-_Nov_2003.jpg)
* [Rocking out in Wheatley](Rocking_out_in_Wheatley.jpg)
* [Simon and smiling fans, before the
  gig](Simon_and_smiling_fans-_before_the_gig.jpg)
* [Simon drums&hellip; at 8&rsquo;s bar](Simon_drums..._at_8-s_bar.jpg)
* [Stu practices his moves before going
  onstage](Stu_practices_his_moves_before_going_onstage.jpg)
* [Tim, Stu, Simon &amp; Ian&hellip;](Tim-_Stu-_Simon_and_Ian....jpg)

&ldquo;\[unreleased\]&rdquo; collects, in chronological order, various bootleg
versions of songs that have never officially been released by the band. They
represent &ldquo;release&rdquo; years of {{ time(t="2002") }}
(&ldquo;Better&rdquo; and &ldquo;Purge&rdquo;), {{ time(t="2010") }} (&ldquo;Al Fresco&rdquo;), {{ time(t="2011") }} (&ldquo;Ron Jeremy Beadle&rdquo;, a song written
for the occasion of <b>Stuart Smith</b>&rsquo;s departure from the band), {{ time(t="2015") }} (&ldquo;\[unreleased, 2015\]&rdquo;), and {{ time(t="2016") }} (&ldquo;\[unreleased, 2016\]&rdquo;). Note that
this does _not_ include the two tracks that were slated to be on the {{ time(t="2003") }} demo; these get their own special entry in the
discography.

&ldquo;Hippy Jam Fest&rdquo; (sometimes known as either &ldquo;Hippy Jam Fest
(The Likes Of Which Has Never Been Seen)&rdquo; or &ldquo;Hippy Jam Fest (The
Likes Of Which Has Never Been Seen Before)&rdquo;) was originally self-released
by the band on [CD-R](https://en.wikipedia.org/wiki/CD-R); the track listing of
this original version is represented here. However, the versions that can
actually be readily found are instead representative of the partial re-issue by
[Big Scary Monsters](/labels/big-scary-monsters), which only features the
tracks &ldquo;Hippy Jam Fest (The Likes Of Which Has Never Been Seen
Before)&rdquo;, &ldquo;Denial Adams&rdquo;, and &ldquo;Like Romeo And
Juliet&rdquo;. As a result, the other two tracks are of incredibly dubious
provenance and appear to either have had very poor recording quality, or were
mangled at some point(s) (or both&hellip;). Nevertheless, the two versions are
merged here (to jarring effect) in order to reconstruct the track sequence of
the original.

All of the tracks on <b>This Town Needs Guns</b>&rsquo;s side of their split
with <b>Cats And Cats And Cats</b> can be found on their self-titled LP.

Tracks 14 and 15 of &ldquo;Animals&rdquo; also appear on &ldquo;This Town Needs
Guns&rdquo;. Track 16 was released as a bonus track for the decade anniversary
of the album, and is free to download from the respective bandcamp page.

The 13<sup>th</sup> and final track of &ldquo;13.0.0.0.0&rdquo; is a bonus
track from the [Japanese](/countries/japan) edition, distributed by [Stiff
Slack](/labels/stiff-slack). The dspr distribution only contains this track,
since the other tracks are available from the corresponding bandcamp page.

{{ discog() }}

[redundancy]: https://en.wikipedia.org/wiki/Redundancy_(engineering)
