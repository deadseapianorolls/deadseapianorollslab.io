+++
title = "The Redneck Manifesto"
date = 2020-04-30

[taxonomies]
genres = [
    "math rock",
    "post-hardcore",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["r"]
first-release-dates = ["1999"]
release-dates = [
    "1999",
    "2000",
    "2001",
    "2002",
    "2004",
    "2006",
    "2010",
    "2018",
    "2019",
]
labels = [
    "Greyslate",
    "Red F Records",
    "The Richter Collective",
    "Trust Me I'm A Thief",
    "251 Records",
    "Road Relish",
    "Errol Records",
    "Stiff Slack",
    "Modern City Records",
    "Dropping Like Flys Records",
    "Australian Cattle Gods",
]
countries = ["Ireland"]
provinces = ["County Dublin, Ireland"]
areas = ["Dublin, County Dublin, Ireland"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>The Redneck Manifesto</b> are one of the premier acts of the {{
genre(name="math rock") }} canon. They are a band hailing from
[Dublin](/areas/dublin-county-dublin-ireland), alongside other illustrious
[Irish](/countries/ireland) {{ genre(name="math rock") }} acts such as
<b>Alarmist</b>, <b>BATS</b>, and <b>Adebisi Shank</b>. Being purely
instrumental and {{ hs(name="guitar") }}-centric, alternating between clean
soft-sounding arrangements and [fuzz][fuzz]ed-out {{
genre(name="post-hardcore") }} ones, and making sporadic use of {{
genre(name="electronic") }} elements, they could be thought of as the
[Irish](/countries/ireland) response to <b>Don Caballero</b>; <b>The Redneck
Manifesto</b>, however, have a very unique and instantly recognizable sound
that is very strongly melodic, and influenced by {{ genre(name="electronic") }}
music to some extent (c.f. <b>Jape</b>).

### notes on the distributions

The albums with names that start with &ldquo;trm:&rdquo; (e.g.
&ldquo;trm:one&rdquo;) are also known by various other names, like
&ldquo;Trmone&rdquo;, &rdquo;TRM:1&rdquo;, &ldquo;TRM:One&rdquo;, &ldquo;EP
One&rdquo;, &amp;c.

&ldquo;RMNMN&rdquo; contains five tracks, one from each member of the band. On
the packaging of the physical version, the track names are represented as
graphics rather than text.

{{ discog() }}

[fuzz]: https://en.wikipedia.org/wiki/Distortion_(music)
