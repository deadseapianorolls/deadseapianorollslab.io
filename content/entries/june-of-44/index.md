+++
title = "June of 44"
date = 2020-07-14

[taxonomies]
genres = [
    "post-hardcore",
    "experimental rock",
    "math rock",
    "post-rock",
    "punk",
    "jazz fusion",
    "slowcore",
    "indie rock",
    "rock",
    "pop",
    "jazz",
]
first-letters = ["j"]
first-release-dates = ["1995"]
release-dates = ["1995", "1996", "1997", "1998", "1999", "2020"]
labels = [
    "Quarterstick Records",
    "Konkurrent",
    "Atomic Recordings",
    "B-Core Disc",
]
countries = ["United States"]
provinces = ["Kentucky, United States"]
areas = ["Louisville, Kentucky, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>June of 44</b> was (and more recently, is) a {{ genre(name="post-hardcore")
}} band (usually thought of as {{ genre(name="math rock") }}, but also
responsible for early {{ genre(name="post-rock") }} and dipping into a variety
of other genres) formed in
[Louisville](/areas/louisville-kentucky-united-states) in {{ time(t="1994") }}. <b>June of 44</b>&rsquo;s music has humourously
been referred to as &ldquo;boat {{ genre(name="rock") }}&rdquo; because of the
nautical textual and visual themes that they used (particularly in their first
three albums). <b>June of 44</b> formed out of the breakup of {{
hs(name="guitar") }}ist and {{ hs(name="vocal") }}ist <b>Jeff
Mueller</b>&rsquo;s previous band, the legendary {{ genre(name="math rock") }}
outfit <b>Rodan</b>. The four members of <b>June of 44</b> have all been part
of other notable acts, usually in the realm of {{
genre(name="experimental rock") }} and {{ genre(name="post-hardcore") }}, e.g.
<b>HiM</b>, <b>Shipping News</b>, <b>Codeine</b>, <b>Lungfish</b>,
<b>Hoover</b>, and <b>The Crownhate Ruin</b>. <b>June of 44</b> played some
reunion shows in {{ time(t="2018") }}, and are releasing an album
(mostly of reworked old songs) {{ time(t="later this year
(2020)", dt="2020-08-07") }}. <b>June of 44</b> are remembered for being classic {{
genre(name="math rock") }}ers and {{ genre(name="post-rock") }}ers, and having
a broad sonic palette that further expanded what the
[Louisville](/areas/louisville-kentucky-united-states) {{
genre(name="post-hardcore") }} scene could sound like.

<b>June of 44</b> started off their recording career with the {{ time(t="1995", dt="1995-06-20") }} LP, &ldquo;Engine Takes to the Water&rdquo;.
This album has been criticised for apeing their
[Louisville](/areas/louisville-kentucky-united-states) {{
genre(name="math rock") }}/{{ genre(name="post-rock") }} predecessors,
<b>Slint</b> (particularly, of course, &ldquo;Spiderland&rdquo; ({{ time(t="1991", dt="1991-03-27") }})). Indeed, <b>June of 44</b>&rsquo;s career
lives in the shadow of &ldquo;Spiderland&rdquo;, but comparing them to
<b>Slint</b> (or even calling them a &ldquo;<b>Slint</b> rip-off&rdquo;!) is
certainly unfair &mdash; although tempting, due to
&ldquo;Spiderland&rdquo;&rsquo;s significance as one of the more well-known
albums of all time. Yes, &ldquo;Engine Takes to the Water&rdquo; does borrow
stylistic quirks of &ldquo;Spiderland&rdquo; (particularly in the first track,
&ldquo;Have a Safe Trip, Dear&rdquo;), like tense and angular {{
hs(name="guitar") }}-driven buildups with spoken {{ hs(name="vocals") }} that
explode into {{ genre(name="post-hardcore") }} yelping and
[distorted][distortion] {{ hs(name="guitar") }}s. But even &ldquo;Engine Takes
to the Water&rdquo; has more to offer &mdash; unlike &ldquo;Spiderland&rdquo;,
fans of {{ genre(name="post-rock") }} will have a hard time calling
&ldquo;Engine Takes to the Water&rdquo; &ldquo;a {{ genre(name="post-rock") }}
record&rdquo;. &ldquo;Engine Takes to the Water&rdquo; settles into much more
{{ genre(name="punk") }} grooves, with several of the songs largely chucking {{
genre(name="post-rock") }} out of the window in favor of a more straightforward
{{ genre(name="post-hardcore") }} or {{ genre(name="math rock") }}. Perhaps
more importantly, not all of <b>June of 44</b>&rsquo;s discography has to live
in this shadow &mdash; by their third album, the {{ time(t="1997", dt="1997-01-15") }} &ldquo;The Anatomy of Sharks&rdquo;, <b>June
of 44</b> had established their own sound. &ldquo;The Anatomy of Sharks&rdquo;
would lead to a full-length record, the {{ time(t="1998", dt="1998-01-20") }}
&ldquo;Four Great Points&rdquo;, which expanded <b>June of 44</b>&rsquo;s sound
to include prominent {{ hs(name="trumpet") }} playing from the band&rsquo;s {{
hs(name="bass guitar") }}ist, <b>Fred Erskine</b>, as well as {{
hs(name="violin") }} and {{ hs(name="synthesizer") }}s. <b>June of
44</b>&rsquo;s sound was now more recognizably {{ time(t="1990") }}s {{ genre(name="post-rock") }}, becoming eclectic
and pulling influence from {{ genre(name="electronic") }} music and {{
genre(name="jazz fusion") }}, while still retaining its {{
genre(name="math rock") }} origins. The {{ time(t="1999", dt="1999-06-08") }}
&ldquo;Anahata&rdquo; would intensify these other influences considerably,
removing any straightforward {{ genre(name="punk") }} attitude in favor of
repetitive and pulsing {{ hs(name="drums") }} &amp; {{ hs(name="bass guitar")
}} grooves that entertain an aesthetic somewhere between {{
genre(name="indie rock") }}/{{ genre(name="slowcore") }}, {{
genre(name="jazz fusion") }}, and the {{ genre(name="post-rock") }} of their
[Chicago](/areas/chicago-illinois-united-states) contemporaries <b>Tortoise</b>
(<b>Tortoise</b> founding member <b>Bundy K. Brown</b> was even a guest
musician on previous album &ldquo;Four Great Points&rdquo;). This removal of
almost all {{ genre(name="post-hardcore") }} influence has made
&ldquo;Anahata&rdquo; a polarising album, with many who were <b>June of 44</b>
fans up to that point disliking the album.

### notes on the distributions

&ldquo;In the Fishtank 6&rdquo; is sometimes listed as simply &ldquo;In the
Fishtank&rdquo;.

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
