+++
title = "planets"
date = 2020-05-18

[taxonomies]
genres = [
    "math rock",
    "brutal prog",
    "post-hardcore",
    "noise rock",
    "experimental rock",
    "progressive rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["p"]
first-release-dates = ["2007"]
release-dates = ["2007", "2010", "2011", "2017"]
labels = ["Distile Records"]
countries = ["United States"]
provinces = ["California, United States"]
areas = ["Napa County, California, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>planets</b> is(?) a {{ genre(name="math rock") }}/{{
genre(name="brutal prog") }} band from [Napa,
California](/areas/napa-county-california-united-states), not too far from the
[Sacramento](/areas/sacramento-california-united-states) of
[labelmates](/labels/distile-records) <b>O! The Joy</b>. Similar to much of the
work of [Sacramento](/areas/sacramento-california-united-states) {{
genre(name="brutal prog") }}gers <b>Hella</b>, <b>planets</b> operate as a
two-piece, getting by with just {{ hs(name="drums") }} and {{
hs(name="bass guitar") }} (rather than {{ hs(name="guitar") }}). <b>planets</b>
formed after the dissolution of
[<b>Swims</b>](https://swims.bandcamp.com/album/swims-ep), a similar band also
with <b>Paul Slack</b> on {{ hs(name="bass guitar") }}, but with <b>Mark
Rocha</b> on {{ hs(name="drums") }}. Compared to <b>Swims</b>, <b>planets</b>
are [noisier](/genres/noise-rock), more [brutal](/genres/brutal-prog), and even
more savagely technical and dissonant. Slack&rsquo;s treatment of the {{
hs(name="bass guitar") }} is ferocious, making use of every string and every
fret, and Crawford&rsquo;s {{ hs(name="drumming") }} is a fearsome match,
making the two collectively sound like a band of four. While relatively unknown
even to this day, the music of <b>planets</b> is excellently inventive; all
<b>planets</b> records are staples of the {{ genre(name="math rock") }} and {{
genre(name="brutal prog") }} genres.

<b>planets</b> were signed to the relatively short-lived
[French](/countries/france) {{ genre(name="math rock") }}-oriented record label
[Distile Records](/labels/distile-records) (not to be confused with the newer,
{{ genre(name="electronic") }}-focused [French](/countries/france) label by the
same name), alongside fellow [Californian](/provinces/california/united-states)
{{ genre(name="math rock") }}ers <b>O! The Joy</b>, [French](/countries/france)
fellow 2-piece {{ genre(name="math rock") }}/{{ genre(name="brutal prog")
}}gers <b>37500 Yens</b>, and <b>Paul Slack</b>&rsquo;s previous band
<b>Swims</b>, among others.

### notes on the distributions

[Rate Your Music lists a {{ time(t="2006") }}
release](https://rateyourmusic.com/release/ep/planets/planets/), but this
release is not represented anywhere else, so it may just be spurious (hence its
absence in this listing). Note that <b>Swims</b>&rsquo;s first and last record
([their self-titled EP](https://swims.bandcamp.com/album/swims-ep)) was
released in {{ time(t="December of 2006", dt="2006-12-05") }}, which makes
the authenticity of this entry unlikely. <b>planets</b> is confused by Rate
Your Music (among other sources) with another, distinct,
[Californian](/provinces/california-united-states) band by the name of
<b>P&nbsp;L&nbsp;a&nbsp;N&nbsp;E&nbsp;T&nbsp;S</b>.

Most sources list the release year of &ldquo;Planets&rdquo; as {{ time(t="2008") }}, but this was the year that [Distile
Records](/labels/distile-records) released it. The &ldquo;original&rdquo;
release was handmade by the band themselves some time in {{ time(t="2007") }}.

&ldquo;TURBO JAMZ&rdquo; is also known as &ldquo;TURBO JAMZ!&rdquo;. There is
only one track listed, but it consists of four tunes, in the following order:

1. DON’T SET YOURSELF UP
2. MODERN LOVE
3. HUSTLE
4. RED OUT!

&ldquo;JIVE FROM JETHRO&rdquo; was released in {{ time(t="2017", dt="2017-02-25") }}, but was recorded live between {{ time(t="2009") }} and {{ time(t="2010") }}.

{{ discog() }}
