+++
title = "nuito"
date = 2020-07-09

[taxonomies]
genres = [
    "math rock",
    "post-rock",
    "brutal prog",
    "experimental rock",
    "post-hardcore",
    "progressive rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["n"]
first-release-dates = ["2006"]
release-dates = ["2006", "2007", "2008", "2014"]
labels = ["StudioNUITO", "FlatWell Records"]
countries = ["Japan"]
provinces = ["Kyoto Prefecture, Japan"]
areas = ["Kyoto, Kyoto Prefecture, Japan"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="webp") }}

<b>nuito</b> are(?) a {{ genre(name="math rock") }}/{{ genre(name="post-rock")
}} power trio from [Kyoto](/areas/kyoto-kyoto-prefecture-japan), formed some
time in {{ time(t="2004") }}. They are most well-known for their
only full-length record, the {{ time(t="2008", dt="2008-01-01") }}
&ldquo;Unutella&rdquo;, although before that, they released two little-known
EPs. These EPs are apparently quite rare, and I could only track down one of
the two (and a lossy copy of it, at that).

<b>nuito</b>&rsquo;s style seamlessly blends the kind of ADHD-driven {{
genre(name="brutal prog") }}/{{ genre(name="math rock") }} characteristic of
both other [Japanese](/countries/japan) acts like <b>Ruins</b> and
<b>Korekyojinn</b> (<b>是巨人</b>) as well as
[American](/countries/united-states) acts like
[<b>planets</b>](/entries/planets) and <b>Ahleuchatistas</b> (now <b>Lighted
Stairs</b>) with a more pensive {{ genre(name="post-rock") }}-inspired sound,
comparable to the {{ genre(name="post-rock") }}/{{ genre(name="math rock") }}
fusion of bands like <b>toe</b> and <b>Giraffes? Giraffes!</b>.
&ldquo;Unutella&rdquo; is a classic of the {{ genre(name="math rock") }} canon,
and also serves as a useful introduction for those new to the genre of {{
genre(name="brutal prog") }}.

{{ discog() }}
