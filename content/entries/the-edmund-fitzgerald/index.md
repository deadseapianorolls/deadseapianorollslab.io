+++
title = "The Edmund Fitzgerald"
date = 2020-04-28

[taxonomies]
genres = [
    "math rock",
    "post-rock",
    "experimental rock",
    "post-hardcore",
    "midwest emo",
    "indie rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["e"]
first-release-dates = ["2003"]
release-dates = ["2003", "2004", "2005"]
labels = [
    "Vacuous Pop",
    "Noisestar",
    "Try Harder Records",
    "Truck Records",
    "Stupid Cat Records",
]
countries = ["England"]
provinces = ["Oxfordshire, England"]
areas = ["Oxford, Oxfordshire, England"]

[extra]
last_updated = "2020-07-02"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>The Edmund Fitzgerald</b> was a {{ genre(name="math rock") }} band from
[Oxford](/areas/oxford-oxfordshire-england) that is best known for being the
predecessor of {{ genre(name="math pop") }} (and later, {{ genre(name="indie
rock") }}) band <b>Foals</b>. <b>The Edmund Fitzgerald</b>&rsquo;s sound was
less {{ genre(name="pop") }}py than what <b>Foals</b> would later be, and their
music is centered around dynamic, long-form songs (in the {{
genre(name="post-rock") }} style). Some of the band&rsquo;s music (q.v. the
first six tracks off of the &ldquo;\[compiled demos\]&rdquo;) warrants use of
the {{ genre(name="midwest emo") }} label here, for the dynamism combined with
twinkly {{ hs(name="guitar") }}s, the {{ hs(name="vocal") }} style, and {{
genre(name="punk") }} attitude suggest it &mdash; a comparison could perhaps be
made to <b>The Van Pelt</b>. Their other music is more clearly {{
genre(name="math rock") }}, focusing on the interplay of noodly {{
hs(name="guitar") }}s (sometimes clean, sometimes [distorted][distortion]) with
athletic {{ hs(name="drumming") }}, in a style comparable to bands like <b>The
Purkinje Shift</b> and <b>Don Caballero</b> (although not instrumental, unlike
those two bands). In {{ time(t="2005") }}, <b>Lina Simon</b> quit
the band, opting not to pursue a musical career at the time. <b>Yannis
Philippakis</b> and <b>Jack Bevan</b> would go on to form <b>Foals</b>.

Gallery of images associated with <b>The Edmund Fitzgerald</b>:

* [Yannis &amp; Lina](yannis-lina.jpg)
* [Yannis &amp; Jack](yannis-jack.jpg)
* [Full band performing](full-band.jpg)
* [Kodak of the band](kodak.jpg)
* [Another band photo](band.jpg)
* [Yannis &amp; Jack \[1\]](yannis-jack-1.jpg)

### notes on the distributions

The tracks on &ldquo;\[compiled demos\]&rdquo; are generally of uncertain date,
as none of them were officially released as far as I know. They are probably
mostly from {{ time(t="2003") }}, so that is the date used here.

The distributions for &ldquo;Truck 2003 I am Five&rdquo;, &ldquo;The Edmund
Fitzgerald / Youthmovie Soundtrack Strategies&rdquo;, &ldquo;Silver Rocket
SR50&rdquo;, and &ldquo;Noisestar Sessions #05 And #06&rdquo; all only contain
<b>The Edmund Fitzgerald</b>&rsquo;s contributions to each of those albums,
respectively.

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
