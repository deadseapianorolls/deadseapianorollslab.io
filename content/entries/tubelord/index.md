+++
title = "tubelord"
date = 2020-06-05

[taxonomies]
genres = [
    "math pop",
    "math rock",
    "experimental rock",
    "post-hardcore",
    "post-punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["t"]
first-release-dates = ["2007"]
release-dates = ["2007", "2008", "2009", "2010", "2011"]
labels = [
    "Hassle Records",
    "Big Scary Monsters",
    "Pink Mist",
    "Banquet Records",
    "One Inch Badge Records",
    "Alcopop! Records",
    "Gravity Dip",
    "Popular Records",
]
countries = ["England"]
provinces = ["Greater London, England"]
areas = ["Kingston upon Thames, Greater London, England"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>tubelord</b> was a {{ genre(name="math pop") }} band from [Kingston upon
Thames](/areas/kingston-upon-thames-greater-london-england), part of the
[English](/countries/england) {{ genre(name="math pop") }} scene, alongside
others like [<b>TTNG</b>](/entries/ttng),
[<b>saleontomorrow</b>](/entries/saleontomorrow), [<b>NGOD</b>](/entries/ngod),
[<b>Tabar</b>](/entries/tabar), <b>Colour</b> (with whom <b>tubelord</b>
briefly shared a member), <b>pennines</b>, <b>Foals</b>, <b>Fish Tank</b>,
&amp;c. In many ways, <b>tubelord</b> were the ultimate {{
genre(name="math pop") }} band, exemplifying many of the various stylistic
quirks of the genre, while also producing extremely unique and recognizable
music that stands out as some of the best that {{ genre(name="math pop") }}
&mdash; and indeed, {{ genre(name="math rock") }} more generally &mdash; has to
offer. The only two constant members of the band were lead {{ hs(name="vocal")
}}ist &amp; {{ hs(name="guitar") }}ist <b>Joey Fourr</b> (<b>Joseph
Prendergast</b> at the time) and {{ hs(name="drummer") }} &amp; backup {{
hs(name="vocal") }}ist <b>David Catmur</b>. After the breakup of
<b>tubelord</b>, <b>Joey Fourr</b> would embark on a solo career, and <b>James
Elliot Field</b> would go on to join [Cornwall](/provinces/cornwall-england) {{
genre(name="indie rock") }}ers <b>Tall Ships</b>.

Humourously, <b>tubelord</b>&rsquo;s single, &ldquo;I Am Azerrad&rdquo; (better
known for its later appearance on &ldquo;Our First American Friends&rdquo;),
caught the attention of the eponymous <b>Michael Azerrad</b>, who was
particularly fazed by the lyrics &ldquo;I see today, I see you, Azerrad /
I&rsquo;ve read the clues, they lead me to your head / I&rsquo;ll kill today,
I&rsquo;ll kill you, Azerrad&rdquo;. Azerrad wrote [an article on the subject
for SPIN magazine][spin] ([archived][spin-archived]).

<b>tubelord</b>&rsquo;s sound enjoyed a progression throughout their career.
<b>tubelord</b> started with the more firmly {{ genre(name="post-hardcore")
}}/{{ genre(name="math rock") }} sound of their earlier work, with powerful and
often [distorted][distortion] [block chords][block-chord] combined with snaky
{{ hs(name="guitar") }} and {{ hs(name="bass guitar") }} lines, and both clean
and screamed {{ hs(name="vocals") }}. Their later sound incorporated more
elements of {{ genre(name="post-punk") }} (c.f. <b>The Dismemberment Plan</b>
for another example of music that hybridizes {{ genre(name="math rock") }} and
{{ genre(name="post-punk") }}), including more prominent use of {{
hs(name="synthesizer") }}s (although their earlier work had some) and less
emphasis on unclean vocals. Common to all of <b>tubelord</b>&rsquo;s work,
however, is the influence of [through-composition][through-composed] (a feature
not uncommon in {{ genre(name="math pop") }} generally) and a commitment to an
overall {{ genre(name="math pop") }} sound that combines catchy hooks with
unusual rhythms and song structures within the context of {{ genre(name="punk")
}} music.

### notes on the distributions

&ldquo;SQUARE&rdquo; is sometimes listed as &ldquo;SQUARE EP&rdquo;,
&ldquo;Square&rdquo;, or &ldquo;Square EP&rdquo;.

The 7&Prime; version of &ldquo;Feed Me a Box of Words&rdquo; only features the
first two tracks.

<b>Tellison</b>&rsquo;s contribution to their split with <b>tubelord</b> is
sometimes also listed as &ldquo;Wasp's Nest&rdquo;, &ldquo;Wasp Nest&rdquo;, or
&ldquo;Wasps Nest&rdquo;.

The 7&Prime; version of &ldquo;I Am Azerrad&rdquo; only has the first two
tracks. The CD-R version lacks the final track. The digital version has all
four tracks.

&ldquo;For the Grandparents&rdquo; is sometimes listed as &ldquo;One For the
Grandparents&rdquo;.

&ldquo;Tezcatlipōca&rdquo; is sometimes listed as &ldquo;Tezcatlipoca&rdquo;.
Not all releases of &ldquo;Tezcatlipōca&rdquo; have the bonus demo track,
&ldquo;De²&rdquo;. The track in question is sometimes listed as
&ldquo;De2&rdquo;.

&ldquo;Cows To The West, Cities To The East&rdquo; from &ldquo;One Inch Badge
Split Series Volume 4&rdquo; is sometimes listed with the same title as the
original rendition of the song, &ldquo;Cows to the East, Cities to the
West&rdquo; (the cardinal directions being reversed) from &ldquo;Our First
American Friends&rdquo;. The deadseapianorolls distribution for &ldquo;One Inch
Badge Split Series Volume 4&rdquo; only includes this track.

&ldquo;r o m a n c e&rdquo; is sometimes listed as &ldquo;Romance&rdquo;.

{{ discog() }}

[spin]: https://www.spin.com/2008/10/lyrical-assassin/
[spin-archived]: https://web.archive.org/web/20200206061110/https://www.spin.com/2008/10/lyrical-assassin/
[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
[block-chord]: https://en.wikipedia.org/wiki/Block_chord
[through-composed]: https://en.wikipedia.org/wiki/Through-composed_music
