+++
title = "The Brave Little Abacus"
date = 2020-03-29

[taxonomies]
genres = [
    "midwest emo",
    "math rock",
    "baroque pop",
    "math pop",
    "experimental rock",
    "post-hardcore",
    "indie rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["b"]
first-release-dates = ["2008"]
release-dates = ["2008", "2009", "2010", "2011", "2012"]
labels = ["Quote Unquote Records"]
countries = ["United States"]
provinces = ["New Hampshire, United States"]
areas = ["Rockingham County, New Hampshire, United States"]

[extra]
last_edited = "2020-07-19"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="webp") }}

<b>The Brave Little Abacus</b> used to have a bandcamp page &mdash;
unfortunately, it is long gone, and their music is surprisingly difficult to
get your hands on, especially considering the huge underground popularity the
band has enjoyed.

tBLA&rsquo;s popularity and positive critical reception is not for no reason:
the music of tBLA is very unique, well-composed, intricate, and passionate. The
real barrier to entry here is the unfortunately [lo-fi
production](https://en.wikipedia.org/wiki/Lo-fi_music) and the vocals. The
vocals are superb, but unique enough that it will be a matter of individual
taste as to whether you love them or find them annoying. The production, on the
other hand &mdash; while many among us do enjoy some lo-fi production from time
to time &mdash; of tBLA&rsquo;s music is honestly deeply unfortunate, because
their arrangements, and the wildly varied textures that they make use of,
deserve much better treatment so that we could properly hear them in all their
glory. tBLA&rsquo;s music has been called &ldquo;the &lsquo;Pet Sounds&rsquo;
of {{ genre(name="midwest emo") }}&rdquo;, and if that&rsquo;s true, it&rsquo;s
only too bad that tBLA could not enjoy the kind of world-class production that
<b>The Beach Boys</b> did.

### notes on the distributions

Back when their bandcamp page existed, I got a recommendation and I went and
downloaded most of their material. For some reason, I did not listen to it for
a long time, which was clearly a mistake. The far worse mistake, however, is
that I downloaded their music from their bandcamp in 320kb/s MP3 format&hellip;
This was back before I realized that even CBR 320kb/s MP3 fails to achieve
transparency, and before I realized the importance of preserving original
copies. Of course, if anyone has the original FLACs from bandcamp or can rip
some CDs, [that would be much appreciated](/contact).

There is also an alternate, lossless version of &ldquo;Masked
Dancers&hellip;&rdquo; that was obtained from Soulseek, supposedly a CD rip. A
quick look at a spectrogram shows significant activity at &geq;20kHz,
suggesting that this version is probably legitimate (viz. that it is not a
lossy copy that was re-encoded into FLAC).

A distribution of note is the release of their final album,
&ldquo;Okumay&rdquo;. This was their only release to be on a record label (viz.
Quote Unquote Records), and the record label has since released the album
gratis on their website, although only as a VBR-0 MP3 copy, unfortunately.
Also, it is out of print.

Also included are some live/bootleg recordings, which are to be found on
bandcamp, but are **not** official &mdash; they have all been curated and
posted onto bandcamp by [quak](https://twitter.com/quakpdf). The bandcamp
recordings are supposedly the exact audio taken from various handheld
recordings of live shows, cut up into tracks for listening convenience. I
cannot guarantee that the exact audio (or exact copies of careful
digitizations) were actually used for these, so they are marked as dubious.
&ldquo;Live at the Silent Barn&rdquo; in particular is linked to by YouTube
uploader Seamus, pointing to the &ldquo;original file&rdquo;, hosted at
Mediafire as a CBR 160kb/s MP3. I presume that this is the source of the FLACs
hosted on the corresponding bandcamp page.

{{ discog() }}
