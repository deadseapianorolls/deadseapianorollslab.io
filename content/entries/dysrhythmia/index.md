+++
title = "Dysrhythmia"
date = 2020-06-29

[taxonomies]
genres = [
    "brutal prog",
    "progressive metal",
    "experimental metal",
    "progressive rock",
    "experimental rock",
    "metal",
    "math rock",
    "technical thrash metal",
    "post-hardcore",
    "thrash metal",
    "rock",
    "punk",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["2000"]
release-dates = [
    "2000",
    "2001",
    "2002",
    "2003",
    "2004",
    "2006",
    "2007",
    "2009",
    "2012",
    "2016",
    "2019",
]
labels = [
    "Relapse Records",
    "Translation Loss Records",
    "Profound Lore Records",
    "Nerve Altar",
    "Acerbic Noise Development",
    "Rice Control",
    "Tranquility Base",
    "Eyesofsound",
    "Ritual Records",
]
countries = ["United States"]
provinces = ["Pennsylvania, United States", "New York, United States"]
areas = [
    "Philadelphia, Pennsylvania, United States",
    "New York City, New York, United States",
]

[extra]
last_edited = "2020-07-30"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Dysrhythmia</b> is a {{ genre(name="brutal prog") }}/{{
genre(name="progressive metal") }} band from
[Philadelphia](/areas/philadelphia-pennsylvania-united-states) &mdash; and now,
[Brooklyn](/areas/new-york-city-new-york-united-states) &mdash; formed in {{ time(t="August of 1998", dt="1998-08") }}. Originally an informal collaboration
between {{ hs(name="guitar") }}ist <b>Kevin Hufnagel</b> and {{ hs(name="bass
guitar") }}ist <b>Clayton Ingerson</b>, <b>Dysrhythmia</b> became a {{
genre(name="rock") }} band in earnest with the addition of {{
hs(name="drummer") }} <b>Jeff Eber</b> in {{ time(t="early
1999", dt="1999-02") }}. It was with this lineup that <b>Dysrhythmia</b> released their
first three full-length albums, including &ldquo;Pretest&rdquo;, their first
release with [Relapse Records](/labels/relapse-records). At {{ time(t="the end of 2004", dt="2004-11") }}, however, founding member <b>Clayton
Ingerson</b> left the band and was replaced by <b>Colin Marston</b> on {{
hs(name="bass guitar") }}. Marston has engineered most of the band&rsquo;s work
since then, in Menegroth: The Thousand Caves, his recording studio in
[Queens](/areas/new-york-city-new-york-united-states). Both <b>Clayton
Ingerson</b> and <b>Kevin Hufnagel</b> have played as part of {{
genre(name="doom metal") }} outfit <b>While Heaven Wept</b>. Both <b>Kevin
Hufnagel</b> and <b>Colin Marston</b> are also members of the new (reunited)
lineup of [Canadian](/countries/canada) [tech
death](/genres/technical-death-metal) outfit <b>Gorguts</b>. <b>Jeff Eber</b>
has been a member of [New York](/areas/new-york-city-new-york-united-states) {{
genre(name="jazz metal") }}/{{ genre(name="avant-prog") }} outfit
<b>Zevious</b> since its inception in {{ time(t="2007") }}.
<b>Colin Marston</b> has created and/or been a member of a variety of other
experimental music (usually, but not always, {{ genre(name="metal") }}/{{
genre(name="rock") }}-oriented) projects, most notably <b>Krallice</b>,
<b>Behold&hellip; The Arctopus</b>, <b>Infidel? / Castro!</b>, and
<b>Indricothere</b> (a solo project).

The music of <b>Dysrhythmia</b> has been consistently &ldquo;difficult&rdquo;,
insofar as it is characterised, in the minds of most people, as being technical
&amp; abrasive instrumental {{ genre(name="metal") }}/{{ genre(name="rock") }}.
It is, however, considerably more rooted in traditional approaches to musical
composition &mdash; associated with {{ genre(name="progressive rock")
}}/[metal](/genres/progressive-metal), ultimately {{
genre(name="western classical") }} music, and even {{ genre(name="math rock")
}} &mdash; than the music of, say, <b>Behold&hellip; The Arctopus</b>.
Somewhat humorously,
[RateYourMusic](https://rateyourmusic.com/artist/dysrhythmia) consistently
labels <b>Dysrhythmia</b>&rsquo;s music as &ldquo;{{ genre(name="math rock")
}}&rdquo;. While <b>Dysrhythmia</b> do show signs of influence from fellow
[Pennsylvania](/provinces/pennsylvania-united-states) natives <b>Don
Caballero</b> &mdash; particularly in their earlier works &mdash; the actual
influence of {{ genre(name="post-hardcore") }} is minimal, and
<b>Dysrhythmia</b>&rsquo;s music (again, particularly later work) is much
better characterised by labels like {{ genre(name="progressive metal") }} and
even {{ genre(name="brutal prog") }}. Or, perhaps having one album produced by
<b>Steve Albini</b> (&ldquo;Pretest&rdquo;) is enough to be considered a
&ldquo;{{ genre(name="math rock") }} band&rdquo; forever&hellip;

In any case, <b>Dysrhythmia</b> are a band that put equal emphasis on all three
components of their [power trio](https://en.wikipedia.org/wiki/Power_trio), and
their music is perhaps most unique due to the harmonic and melodic
sensibilities of {{ hs(name="guitar") }}ist <b>Kevin Hufnagel</b> (and later,
{{ hs(name="bass guitar") }}ist <b>Colin Marston</b>). Arrangement-wise,
<b>Dysrhythmia</b>&rsquo;s music often sees Hufnagel&rsquo;s {{
hs(name="guitar") }}work floating fairly freely, playing in counterpoint to the
{{ genre(name="bass guitar") }} and allowing the {{ genre(name="bass guitar")
}} to have a very clear voice in the music. Their music also sometimes features
precomposed solos, betraying the influence of {{ genre(name="progressive rock")
}}/[metal](/genres/progressive-metal). And, of course, the song structures at
play in <b>Dysrhythmia</b>&rsquo;s music are complex, often simultaneously
appearing to be both
[through-composed](https://en.wikipedia.org/wiki/Through-composed_music) and
formal. At this point, their musical output to date is well-known and
well-respected enough to make them behemoths of instrumental {{
genre(name="experimental rock") }}.

As an interesting sidenote, their latest release as of this writing
(&ldquo;Terminal Threshold&rdquo;) is _somewhat_ of a non-sequitur to the works
leading up to it. This album alone has lead to the band being tagged here with
&ldquo;{{ genre(name="technical thrash metal") }}&rdquo; (tech thrash).
&ldquo;Terminal Threshold&rdquo; owes much more influence to classic
[Texan](/provinces/texas-united-states) [tech
thrash](/genres/technical-thrash-metal) outfit <b>Watchtower</b> than previous
<b>Dysrhythmia</b> efforts (although, <i>cf.</i> <b>Watchtower</b> {{
hs(name="guitar") }}ist <b>Ron Jarzombek</b>&rsquo;s instrumental project,
<b>Blotted Science</b>). The {{ genre(name="bass guitar") }} is _much_ less
present here than in previous works, and the compositions are much more compact
(the longest cut clocking in at just {{ time(t="4:31", dt="PT4M31S") }}).
Even the approach to audio production is different &mdash; still as slick and
steeped in modern techniques as &ldquo;The Veil Of Control&rdquo;, but with a
more classic-sounding focus on lows and low-mids (and thus apparently quieter
sound).

### notes on the distributions

The recordings of &ldquo;Annihilation II&rdquo; and &ldquo;Annihilation
I&rdquo; that appear on <b>Dysrhythmia</b>&rsquo;s split with
<b>XthoughtstreamsX</b> are different from those that appear on
&ldquo;Pretest&rdquo;.

The recording of &ldquo;And Just Go&rdquo; that appears on
<b>Dysrhythmia</b>&rsquo;s split with <b>Technician</b> is different from the
one that appears on &ldquo;Pretest&rdquo;.

The final track on &ldquo;Pretest&rdquo; (&ldquo;An Ally To
Comprehension&rdquo;) is a [Japan](/countries/japan)-only bonus track. A
re-recording of this tune appears on &ldquo;Barriers and Passages&rdquo;. The
deadseapianorolls distribution for &ldquo;Pretest&rdquo; includes only this
track.

The final track on &ldquo;Barriers and Passages&rdquo; (&ldquo;Void&rdquo;) is
a [Japan](/countries/japan)-only bonus track. The deadseapianorolls
distribution for &ldquo;Barriers and Passages&rdquo; includes only this track.

{{ discog() }}
