+++
title = "noumenon"
date = 2020-04-17

[taxonomies]
genres = [
    "math rock",
    "noise rock",
    "post-hardcore",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["n"]
first-release-dates = ["2006"]
release-dates = ["2006", "2010", "2012", "2019"]
labels = ["Big Scary Monsters", "Naked Ally Records", "Deep Space Objects"]
countries = ["United States"]
provinces = ["Illinois, United States"]
areas = ["Chicago, Illinois, United States"]

[extra]
last_edited = "2020-04-23"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>noumenon</b> was a quite [noisy](/genres/noise-rock) {{
genre(name="math rock") }} band from a city that could reasonably be called the
world capital of {{ genre(name="math rock") }}:
[Chicago](/areas/chicago-illinois-united-states). A self-described &ldquo;party
math&rdquo; band, <b>noumenon</b>&rsquo;s music is loud, raucous, relentlessly
[math](/genres/math-rock)y, and often downright catchy. Their music is largely
instrumental, although gang vocals are occasionally used.

### notes on the distributions

[<b>noumenon</b>&rsquo;s bandcamp](https://noumenon.bandcamp.com/) used to have
an additional compilation release listed, although it can no longer be found.
Back when it existed, I downloaded it &mdash; unfortunately, the same problem
occured here that occured with [<b>The Brave Little
Abacus</b>](/entries/the-brave-little-abacus). At the time, I downloaded in CBR
320kb/s MP3, so that&rsquo;s all I have here. The compilation has been split
into its components: two tracks from the band&rsquo;s {{ time(t="2006") }} demo (which was never officially released, as far
as I know), one track representing their side of the split with <b>Talons</b>,
and two tracks representing their side of the split with
[<b>Rooftops</b>](/entries/rooftops).

&ldquo;Tro&rdquo; is listed with a release date of {{ time(t="2019-11-22", dt="2019-11-22") }}; although this is correct, it is
misleading insofar as the material had already been recorded over a decade
prior. It is a posthumous release.

{{ discog() }}
