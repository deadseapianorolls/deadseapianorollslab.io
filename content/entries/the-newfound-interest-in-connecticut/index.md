+++
title = "The Newfound Interest In Connecticut"
date = 2020-04-27

[taxonomies]
genres = [
    "midwest emo",
    "post-rock",
    "post-hardcore",
    "experimental rock",
    "indie rock",
    "pop-punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["n"]
first-release-dates = ["2002"]
release-dates = ["2002", "2003", "2005"]
labels = ["We Are Busy Bodies", "Underpin Collective"]
countries = ["Canada"]
provinces = ["Ontario, Canada"]
areas = ["Greater Toronto Area, Ontario, Canada"]

[extra]
last_edited = "2020-08-10"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>The Newfound Interest In Connecticut</b> was an early 2000s
[Canadian](/countries/canada) {{ genre(name="midwest emo") }} band that have
retrospectively earned much acclaim and respect, for their masterful
combination of {{ genre(name="midwest emo") }} with {{ genre(name="post-rock")
}} on their {{ time(t="2005") }} LP, &ldquo;Tell Me About The Long Dark Path
Home&rdquo;. The band had humble beginnings as a band playing {{
genre(name="pop-punk") }} in the style of {{ genre(name="midwest emo") }},
similarly to e.g. <b>Jimmy Eat World</b> (c.f. esp. &ldquo;Clarity&rdquo; ({{
time(t="1999", dt="1999-02-23") }})). Already in their first album, there were
hints at the {{ genre(name="post-rock") }} to come, but it was not until their
{{ time(t="2005") }} LP that this would come to fruition &mdash; the first
track on said LP is already a {{ time(t="7-minute", dt="PT7M") }} {{
genre(name="post-rock") }} instrumental. Drawing another comparison to
&ldquo;Clarity&rdquo; here is tempting due to the last track on that album
(&ldquo;Goodbye Sky Harbor&rdquo;), but the {{ genre(name="post-rock") }} of
<b>The Newfound Interest In Connecticut</b> is vastly different.

<b>The Newfound Interest In Connecticut</b>&rsquo;s {{ genre(name="midwest
emo") }}/{{ genre(name="post-rock") }} hybridization not only predates other,
more well-known practitioners (e.g. <b>CSTVT</b> and <b>The World Is a
Beautiful Place &amp; I Am No Longer Afraid to Die</b>), but it is also more
creatively diverse in its sonic textures, and manages to fully evoke the power
of the greatest &ldquo;{{ genre(name="post-rock") }}&rdquo; bands _and_ the
power of the greatest {{ genre(name="midwest emo") }} bands within just one
album. The title of &ldquo;Tell Me About The Long Dark Path Home&rdquo; is
evocative, as the music itself seems almost frigid and bleak, but nevertheless
achieves stirringly emotional textures and climaxes. All of this, combined with
the occasionally [math](/genres/math-rock)y tendencies of the album (albeit not
enough to warrant a {{ genre(name="math rock") }} label &mdash; only as much as
is typical of {{ genre(name="midwest emo") }}), makes it feel like
&ldquo;Spiderland&rdquo; ({{ time(t="1991", dt="1991-03-27") }}) if it was
written by a {{ genre(name="midwest emo") }} band that listened a lot to fellow
[Torontonians](/areas/greater-toronto-area-ontario-canada) [<b>Do Make Say
Think</b>](/entries/do-make-say-think) (esp. considering the experimentation
with {{ hs(name="wind instruments") }}).

According to [some hearsay][hearsay] ([archived][hearsay-archived]), the origin
of the band&rsquo;s name is an online relationship that one of the band members
had with someone in [Connecticut](/provinces/connecticut-united-states).

### notes on the distributions

The distributions for &ldquo;Less Is More Or Less&rdquo; {{
time(t="have been updated", dt="2020-08-10") }} with a CD rip from Soulseek,
thanks to [/u/Ceasing](https://reddit.com/user/Ceasing).

<b>The Newfound Interest In Connecticut</b>&rsquo;s side of the split with
<b>Plant The Bomb</b> is also found on &ldquo;Tell Me About The Long Dark Path
Home&rdquo;; the track length from the latter is used in our listing here for
the &ldquo;$4 Split&rdquo;.

The hidden track at the end of &ldquo;Tell Me About The Long Dark Path
Home&rdquo; is distributed in MP3; the rest is FLAC.

{{ discog() }}

[hearsay]: https://sophiesfloorboard.blogspot.com/2020/03/the-newfound-interest-in-connecticut.html
[hearsay-archived]: https://web.archive.org/web/20200427093317/https://sophiesfloorboard.blogspot.com/2020/03/the-newfound-interest-in-connecticut.html
