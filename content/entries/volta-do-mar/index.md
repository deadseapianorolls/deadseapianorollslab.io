+++
title = "Volta do Mar"
date = 2020-08-25

[taxonomies]
genres = [
    "math rock",
    "post-rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "rock",
    "pop",
]
first-letters = ["v"]
first-release-dates = ["2000"]
release-dates = ["2000", "2001", "2003", "2005", "2015"]
labels = ["Arborvitae Records"]
countries = ["United States"]
provinces = ["Illinois, United States"]
areas = ["Chicago, Illinois, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Volta do Mar</b> was a {{ genre(name="math rock") }}/{{
genre(name="post-rock") }} band from
[Chicago](/areas/chicago-illinois-united-states) that formed, in earnest, some
time in {{ time(t="1998") }}. The band only released a single full-length
record, &ldquo;At the Speed of Light or Day&rdquo; ({{ time(t="2001",
dt="2001-10-23") }}), although they released a significant amount of other
material as well, most of which is compiled on &ldquo;03&gt;98&rdquo; ({{
time(t="2005", dt="2005-06-21") }}). <b>Volta do Mar</b> formed out of the
dissolution of [Chicago](/areas/chicago-illinois-united-states) {{
genre(name="experimental rock") }} band <b>Hashbrown</b>, in which {{
hs(name="bass guitar") }}ist <b>Jeff Wojtysiak</b> and {{ hs(name="guitar")
}}ist <b>Phil Taylor</b> both played. All of their releases were on the
now-defunct [Champaign-Urbana](/areas/champaign-urbana-illinois-united-states)
record label [Arborvitae Records](/labels/arborvitae-records) (&ldquo;Team
AV&rdquo;), a small label perhaps best known for releasing
<b>piglet</b>&rsquo;s &ldquo;lava land&rdquo; ({{ time(t="2005",
dt="2005-07-12") }}).

See [Pitchfork&rsquo;s review of <b>Volta do Mar</b>&rsquo;s debut {{
time(t="2000") }} EP][pitchfork] ([archived][pitchfork-archived]).

Speaking of <b>piglet</b>, fans of <b>piglet</b> who are not already aware of
<b>Volta do Mar</b> (as <b>Volta do Mar</b> are, unfortunately, somewhat
neglected within the {{ genre(name="math rock") }} canon) are sure to love some
of <b>Volta do Mar</b>&rsquo;s similarities with their more well-known
labelmates: the (largely) instrumental nature of the music, the focus on
electric {{ hs(name="bass guitar") }} and its contrast with a single electric
{{ hs(name="guitar") }}, and [experimental](/genres/experimental-rock) but
melodic compositions that borrow as much from {{ genre(name="post-rock") }} as
they do from {{ genre(name="post-hardcore") }}. One of the main differences, of
course, is that <b>Volta do Mar</b> did it first. In addition, <b>Volta do
Mar</b> focus on {{ hs(name="bass guitar") }} nearly to the point of obsession;
the band had two {{ hs(name="bassist") }}s, one typically playing a
five-stringer, and the other typically playing a six-stringer. Furthermore, in
contrast to the extremely tightly-knit compositional style of <b>piglet</b>,
<b>Volta do Mar</b> take more conscious influence from local {{
genre(name="post-rock") }} pioneers like <b>Tortoise</b> and [<b>June of
44</b>](/entries/june-of-44) when it comes to their winding and meandering song
structures. <b>Volta do Mar</b> are surely one of the pioneers of the {{
genre(name="math rock") }} and {{ genre(name="post-rock") }} genres as we know
them today, even if their names are not as often enshrined in canon as those of
the artists that they influenced.

### notes on the distributions

&ldquo;Volta do Mar&hellip;&rdquo; is sometimes listed as simply &ldquo;Volta
do Mar&rdquo;. All of the tracks from this album were re-released as part of
the {{ time(t="2005", dt="2005-06-21") }} compilation record,
&ldquo;03&gt;98&rdquo;.

Three of the four <b>Volta do Mar</b> tracks off of &ldquo;Konrad Friedrich
Wilhelm Zimmer&rdquo; appear re-released as part of the {{ time(t="2005",
dt="2005-06-21") }} compilation record, &ldquo;03&gt;98&rdquo;.

&ldquo;03&gt;98&rdquo; is sometimes listed as &ldquo;03 &gt; 98&rdquo; or as
&ldquo;3&gt;98&rdquo;. The sixth track from that album, &ldquo;The Lawless Days
of Drinking Whiskey in 10th Grade&rdquo;, is sometimes listed as simply
&ldquo;Lawless Days of Drinking Whiskey in 10th Grade&rdquo;.

{{ discog() }}

[pitchfork]: https://pitchfork.com/reviews/albums/8486-volta-do-mar-ep/
[pitchfork-archived]: https://web.archive.org/web/20170512143224/http://pitchfork.com/reviews/albums/8486-volta-do-mar-ep/
