+++
title = "Le Pré Où Je Suis Mort"
date = 2020-04-08

[taxonomies]
genres = [
    "screamo",
    "post-rock",
    "post-hardcore",
    "math rock",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["l"]
first-release-dates = ["2007"]
release-dates = ["2007", "2010"]
labels = [
    "Ape Must Not Kill Ape",
    "Denovali Records",
    "Communication Is Not Words",
    "Moment Of Collapse Records",
    "Arctic Radar",
    "IFB Records",
]
countries = ["Switzerland"]
provinces = ["Geneva, Switzerland"]
areas = ["Geneva, Geneva, Switzerland"]

[extra]
last_edited = "2020-07-23"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Le Pré Où Je Suis Mort</b> (in English: <i>The Field Where I Died</i>) was a
[Swiss](/countries/switzerland) {{ genre(name="screamo") }} band that
unfortunately left us with fairly little material. Playing {{
genre(name="screamo") }} in what some might call &ldquo;the
[French](/countries/france) style&rdquo;, <b>Le Pré Où Je Suis Mort</b>
incoporated elements of {{ genre(name="post-rock") }}, leading to their
sometimes lengthy songs. For their being a {{ genre(name="post-hardcore") }}
band with an ear for clear syncopation and odd time signatures, they are here
also labeled as part of the {{ genre(name="math rock") }} genre. I admit my
bias towards this sound, as some of my favorite {{ genre(name="screamo") }} is
in this &ldquo;European&rdquo; or &ldquo;[French](/countries/france)&rdquo;
style, but <b>Le Pré Où Je Suis Mort</b> are clearly in the top of the pile,
and I strongly recommend a listen to anyone interested in {{
genre(name="screamo") }}.

### notes on the distributions

The band &ldquo;<b>Men As Trees</b>&rdquo; that appears on the {{
time(t="2010", dt="2010-07-15") }} split, is now known as <b>Locktender</b>.

{{ discog() }}
