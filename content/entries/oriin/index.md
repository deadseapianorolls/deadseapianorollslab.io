+++
title = "Oriin"
date = 2020-04-22

[taxonomies]
genres = [
    "post-rock",
    "experimental rock",
    "ambient",
    "rock",
    "pop",
]
first-letters = ["o"]
first-release-dates = ["2009"]
release-dates = ["2009"]
labels = ["futurerecordings"]
countries = ["United States"]
provinces = ["Texas, United States"]
areas = ["Rio Grande Valley, Texas, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Oriin</b> (perhaps [Finnish](https://en.wikipedia.org/wiki/Finnish_language)
for &ldquo;stallion(s)&rdquo; or &ldquo;thorns&rdquo;?) was an instrumental {{
genre(name="post-rock") }} band from the [Rio Grande
Valley](/areas/rio-grande-valley-texas-united-states) region of
[Texas](/provinces/texas-united-states). Although their sound is reminiscent of
the archetypical &ldquo;{{ genre(name="post-rock") }}&rdquo; band (c.f. fellow
[Texans](/provinces/texas-united-states) <b>Explosions in the Sky</b> and
<b>This Will Destroy You</b>), the music of <b>Oriin</b> is committed to an
ethereal, [reverb](https://en.wikipedia.org/wiki/Reverberation)-drenched,
almost meditative sound &mdash; even in the more &ldquo;intense&rdquo; bits.
Indeed, the writing and arrangements of <b>Oriin</b>&rsquo;s music are
uncomplicated &mdash; occasionally to the point of minimalism &mdash; and the
music is largely tranquil, thus making them more similar in some ways to fellow
[labelmates](/labels/futurerecordings) [<b>the tumbled
sea</b>](/entries/the-tumbled-sea); for this reason they are here tagged with
the &ldquo;{{ genre(name="ambient") }}&rdquo; genre (despite having a drummer
as a core member of the band).

<b>Oriin</b> only ever released a single record: the {{ time(t="2009", dt="2009-02-27") }} LP &ldquo;all.things.are.numbers.&rdquo;,
released on the [futurerecordings](/labels/futurerecordings) label.
[futurerecordings](/labels/futurerecordings) was a label started by <b>Adam
Nanaa</b> of the highly influential early-1990s {{ genre(name="post-hardcore")
}} band <b>Indian Summer</b>. Sometime in {{ time(t="2019") }},
[futurerecordings](/labels/futurerecordings)&rsquo;s bandcamp and Facebook
pages (among other things) were deleted.

The exact status of the fourth member of <b>Oriin</b> is unclear. According to
their Last.fm page, the band&rsquo;s original lineup had <b>Daniel Gonzalez</b>
on {{ hs(name="bass guitar") }}. Discogs claims that Gonzalez was still the
band&rsquo;s {{ hs(name="bassist") }} at the time of recording their LP.
RateYourMusic lists &ldquo;Adrian&rdquo; as the {{ hs(name="bassist") }} &amp;
{{ hs(name="violinist") }} of the band. Their Facebook page lists
&ldquo;Larry&rdquo; as the {{ hs(name="bassist") }} of the band.

### notes on the distributions

&ldquo;all.things.are.numbers.&rdquo; is also sometimes known as &ldquo;All.
Things. Are. Numbers.&rdquo; and/or &ldquo;All Things Are Numbers&rdquo;; here
we use the spelling preferred by
[futurerecordings](/labels/futurerecordings)&rsquo;s bandcamp page.

{{ discog() }}
