+++
title = "Richard Def and the Mos Pryors"
date = 2020-06-01

[taxonomies]
genres = [
    "math rock",
    "post-hardcore",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["r"]
first-release-dates = ["2010"]
release-dates = ["2010"]
labels = ["Swerp Records"]
countries = ["United States"]
provinces = ["Illinois, United States"]
areas = ["Chicago, Illinois, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="webp") }}

<b>Richard Def and the Mos Pryors</b> (named for [<b>Mos
Def</b>](https://www.discogs.com/artist/5648-Mos-Def) and [<b>Richard
Pryor</b>](https://en.wikipedia.org/wiki/Richard_Pryor)) was a {{
genre(name="math rock") }} band from [Chicago,
Illinois](/areas/chicago-illinois-united-states), best known as one of
[<b>Nnamdi Ogbonnaya</b>](https://nnamdiogbonnaya.bandcamp.com/)&rsquo;s
earlier projects. The band only ever released one single and one LP. The latter
of these was reissued by the now-defunct [Swerp Records](/labels/swerp-records)
(also responsible for releases by some of [<b>Nnamdi
Ogbonnaya</b>](https://nnamdiogbonnaya.bandcamp.com/)&rsquo;s other projects,
as well as <b>Ratboys</b>&rsquo;s {{ time(t="2011") }} demo EP) in
{{ time(t="2011") }}, not long after the album was originally
self-released by the band on their bandcamp page.

The music of <b>Richard Def and the Mos Pryors</b> is riff-driven and dynamic
{{ genre(name="math rock") }} accompanied by [<b>Nnamdi
Ogbonnaya</b>](https://nnamdiogbonnaya.bandcamp.com/)&rsquo;s unique and varied
vocal stylings. The music is playful, with a {{ genre(name="punk") }} energy
that is constantly shifting (due to the influence of {{ genre(name="math rock")
}}) and never gets boring. Highly recommended for {{ genre(name="math rock") }}
fans and fans of [<b>Nnamdi
Ogbonnaya</b>](https://nnamdiogbonnaya.bandcamp.com/)&rsquo;s other music.

{{ discog() }}
