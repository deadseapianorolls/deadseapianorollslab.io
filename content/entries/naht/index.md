+++
title = "NAHT"
date = 2020-08-01

[taxonomies]
genres = [
    "post-hardcore",
    "pop-punk",
    "experimental rock",
    "midwest emo",
    "indie rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["n"]
first-release-dates = ["1996"]
release-dates = [
    "1996",
    "1997",
    "1998",
    "1999",
    "2000",
    "2001",
    "2002",
    "2006",
    "2007",
]
labels = [
    "Time Bomb Records",
    "Toy's Factory",
    "Less Than TV",
    "Secreta Trades",
    "391tone",
    "Balcony Inc.",
    "Straight Up Records",
    "Carnage",
    "Pizza Of Death Records",
    "Inherited Alliance",
    "ZK Records",
    "Deep Elm Records",
]
countries = ["Japan"]
provinces = ["Tokyo, Japan"]
areas = ["Tokyo, Tokyo, Japan"]
+++

{{ artist() }}

<!-- more -->

<b>NAHT</b> was a {{ genre(name="post-hardcore") }}/{{ genre(name="pop-punk")
}} band from [Tokyo](/areas/tokyo-tokyo-japan) that formed in {{
time(t="April of 1995", dt="1995-04") }}. The band formed out of some {{
genre(name="hardcore") }} bands in [the area](/areas/tokyo-tokyo-japan) like
<b>God&rsquo;s Guts</b> and <b>Volume Dealers</b>. They started relatively
early on in their career (fall of {{ time(t="1996") }}) opening for seminal
[Washington,
D.C.](/areas/district-of-columbia-district-of-columbia-united-states) {{
genre(name="post-hardcore") }} act <b>Fugazi</b> when they came to
[Japan](/countries/japan), and would go on to play alongside and work with many
other [Dischord Records](/labels/dischord-records) acts. Outside of
[Japan](/countries/japan), <b>NAHT</b>&rsquo;s influence was rather limited,
but _within [Japan](/countries/japan)_, they had considerable influence in the
development of {{ genre(name="post-hardcore") }} and the {{ genre(name="pop")
}}ification (or {{ genre(name="pop-punk") }}ification) of it. <b>NAHT</b> are
here labelled with the &ldquo;{{ genre(name="midwest emo") }}&rdquo; genre, but
it should be noted that <b>NAHT</b> arrived at their kind of fusion between {{
genre(name="post-hardcore") }} and {{ genre(name="indie rock") }} (and {{
genre(name="pop-punk") }}, in this case) independently of
[American](/countries/united-states) acts like <b>Sunny Day Real Estate</b> and
<b>Jimmy Eat World</b>. <b>NAHT</b>&rsquo;s primary influences were bands like
<b>Fugazi</b> and <b>Drive Like Jehu</b> (and perhaps also <b>Rocket from the
Crypt</b>!), and the sounds that <b>NAHT</b> came up with are not to be
confused with those of their [American](/countries/united-states) counterparts.
It is likely that if you are listening to a [Japanese](/countries/japan) {{
genre(name="post-hardcore") }} (including {{ genre(name="math rock") }}) band,
that band owes some of their influence and overall sound to <b>NAHT</b> (and/or
to <b>Envy</b>, in the case of {{ genre(name="screamo") }}!).

See [the Japanese Wikipedia article on the
band](https://ja.wikipedia.org/wiki/NAHT) for more information.

### notes on the distributions

The last two tracks on &ldquo;Slake&rdquo; are bonus tracks exclusive to the {{
time(t="1998", dt="1998-02-28") }} CD reissue.

&ldquo;Galvanize Me! (Danceteria For Basic I.O.S. Mix)&rdquo; is sometimes
listed as &ldquo;Galvanize Me~ -Danceteria For Basic I.O.S. Mix-&rdquo; or as
&ldquo;Galvanize Me! -Danceteria For Basic I.O.S. Mix-&rdquo;.

{{ discog() }}
