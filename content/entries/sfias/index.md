+++
title = "SFIAS"
date = 2020-04-20

[taxonomies]
genres = [
    "noise",
    "drone",
    "harsh noise",
    "glitch",
    "electronic",
    "ambient",
    "sound collage",
]
first-letters = ["s"]
first-release-dates = ["2006"]
release-dates = ["2006", "2007", "2008"]
labels = []
countries = []
provinces = []
areas = []
+++

{{ artist() }}

<!-- more -->

This is a bit of an unusual entry.

All known biographical information on <b>SFIAS</b> is summed up in this
description, from <b>SFIAS</b>&rsquo;s Last.fm entry:

> SFIAS is composed of at least one houseless man with a 1 gig Memorex
> travel-drive and public library computer access. This "music" is public
> domain for any non commercial use. SFIAS marks his/their albums all rights
> reversed. any track submited by anyone except former members of SFIAS will be
> placed on the next release all rights reversed and not crediting the Author.
> SFIAS does not want money from this world because they feel all things of
> this world belongs to the Devil.

It is suspected that <b>SFIAS</b> is a solo project of one <b>Jakob Virgil</b>,
who was also responsible for the upload of <b>SFIAS</b>&rsquo;s discography.

<b>SFIAS</b>&rsquo;s music is characterized by a combination of software-driven
[synthetic](https://en.wikipedia.org/wiki/Software_synthesizer) {{
genre(name="noise") }} generation and very [low
fidelity](https://en.wikipedia.org/wiki/Lo-fi_music). <b>SFIAS</b>&rsquo;s
music is sonically bleak, often grating, and sometimes seemingly pointless.
<b>SFIAS</b>&rsquo;s discography is quite immense; the seemingly most popular
albums include &ldquo;Barbarlo&rdquo;, &ldquo;Man Moon Cop&rdquo;, and
&ldquo;KOVÁRNA A OBROBNA&rdquo;, so those might be good places to start. Or
maybe not.

### notes on the distributions

All entries in the discography point to the same link: a single archive.org
entry entitled &ldquo;Opsomming&rdquo;. <i>Opsomming</i> is
[Dutch](https://en.wikipedia.org/wiki/Dutch_language) for
&ldquo;enumeration&rdquo;, that is, that entry contains all extant <b>SFIAS</b>
tracks. This archive.org entry is not due to deadseapianorolls; a fan of
<b>SFIAS</b> (or maybe a member, or even the sole creator behind <b>SFIAS</b>?)
by the name of <b>Jakob Virgil</b> uploaded what could be recovered of the
tracks that <b>SFIAS</b> used to have on their Last.fm page (in CBR 128kb/s
MP3) for free.

Most of the works have no date associated with them. Ones that have a date
associated with them (however loosely or dubiously) are listed with that date.

{{ discog() }}
