+++
title = "Pecola"
date = 2020-04-12

[taxonomies]
genres = [
    "math rock",
    "post-hardcore",
    "experimental rock",
    "noise rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["p"]
first-release-dates = ["1995"]
release-dates = ["1995", "1996", "1998", "1999"]
labels = [
    "Cheap Precious Metal",
    "Slipped Disc",
    "Teenage U.S.A. Recordings",
    "Skull Geek",
    "Kosher Rock Records",
    "Lil' Red Wagon",
    "Monstro Records",
]
countries = ["Canada"]
provinces = ["Ontario, Canada"]
areas = ["Greater Toronto Area, Ontario, Canada"]

[extra]
last_edited = "2020-09-22"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Pecola</b> was a [Canadian](/countries/canada) {{ genre(name="math rock") }}
band active during the last half of the 1990s. Molded somewhat in the style of
[San Diego,
California](/areas/san-diego-county-california-united-states)&rsquo;s <b>Drive
Like Jehu</b> (especially considering the {{ hs(name="vocals") }}, which is a
good thing), <b>Pecola</b> took <b>Drive Like Jehu</b>&rsquo;s
[math](/genres/math-rock)y {{ genre(name="post-hardcore") }} experimentation
and intensified it even further, to excellent results. At one point,
<b>Pecola</b> even toured with other illustrious bands such as <b>Polvo</b> and
fellow [Torontonians](/areas/greater-toronto-area-ontario-canada) [<b>Do Make
Say Think</b>](/entries/do-make-say-think).

The band is named for the protagonist of Toni Morrison&rsquo;s &ldquo;[The
Bluest Eye](https://en.wikipedia.org/wiki/The_Bluest_Eye)&rdquo; ({{
time(t="1970") }}). The name of their {{ time(t="1996") }} EP &ldquo;Dat
Hoang&rdquo; derives from a
[Vietnamese](https://en.wikipedia.org/wiki/Vietnamese_language) phrase roughly
equivalent to the English &ldquo;[John
Smith](https://en.wikipedia.org/wiki/Placeholder_name)&rdquo;.

AllMusic has a biography of this band which you can read [here][allmusic]
([archived][allmusic-archived]).

### notes on the distributions

<b>Pecola</b>&rsquo;s split with <b>Smallmouth</b> is missing from
deadseapianorolls so far. As usual, if you can get a copy of this, please
[contact deadseapianorolls](/contact)! The distribution of
<b>Pecola</b>&rsquo;s self-titled cassette is due to a Discogs user by the name
of [theorchidshow](https://www.discogs.com/user/theorchidshow), and was
&ldquo;\[s\]hared with the permission of one member of Pecola&rdquo;.

The distribution of &ldquo;Planet Of Ugly&rdquo; comes encoded as CBR 224kb/s
MP3, but the quality is _much_ lower than you might expect from that. It
appears to just be a very poor digitization. Also, the 192kb/s version of
&ldquo;The Mexican&rdquo; is higher quality than the corresponding 256kb/s
version. The 256kb/s version appears to have been re-encoded from some
lower-bitrate MP3 (perhaps roughly 160 or even 128kb/s).

{{ discog() }}

[allmusic]: https://www.allmusic.com/artist/pecola-mn0001788252/biography
[allmusic-archived]: https://web.archive.org/web/20160612001503/https://www.allmusic.com/artist/pecola-mn0001788252/biography
