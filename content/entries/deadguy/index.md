+++
title = "Deadguy"
date = 2020-05-04

[taxonomies]
genres = [
    "mathcore",
    "metalcore",
    "post-hardcore",
    "metal",
    "punk",
    "experimental metal",
    "experimental rock",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["1994"]
release-dates = ["1994", "1995", "1996", "2000"]
labels = [
    "Victory Records",
    "Hawthorne Street Records",
    "Engine",
    "Blackout! Records",
    "Dada Records",
]
countries = ["United States"]
provinces = ["New Jersey, United States"]
areas = ["Middlesex County, New Jersey, United States"]

[extra]
last_edited = "2020-07-30"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Deadguy</b> are a band that &mdash; one would hope &mdash; needs no
introduction, but here we are, {{ time(t="25 years", dt="P9131D") }} after
the release of the seminal &ldquo;Fixation on a Coworker&rdquo;! <b>Deadguy</b>
was a fairly early {{ genre(name="metalcore") }} band hailing from [New
Jersey](/provinces/new-jersey-united-states) that formed after the breakup of
fellow [New Jersey](/provinces/new-jersey-united-states) {{
genre(name="metalcore") }}rs <b>Rorschach</b> ({{ hs(name="guitar") }}ist
<b>Keith Huckins</b> of <b>Rorschach</b> was a founding member of
<b>Deadguy</b>). <b>Rorschach</b> were a pretty legendary band themselves,
being one of the first (along with [Ohio](/provinces/ohio-united-states)
natives <b>Integrity</b>) to pioneer the genre of {{ genre(name="metalcore")
}}. <b>Deadguy</b>, however, managed to sound remarkably modern (while still
being in the old-school &ldquo;metallic hardcore&rdquo; style), and were a key
innovator of the {{ genre(name="mathcore") }} genre, arguably being the first
{{ genre(name="mathcore") }} band. Between the breakneck, thrash-inspired
riffs, the abrupt changes of [metre][metre], and the use of
[dissonant](https://en.wikipedia.org/wiki/Consonance_and_dissonance),
screeching [chords][chord] often formed around
[tritones](https://en.wikipedia.org/wiki/Tritone), <b>Deadguy</b> paved the way
for {{ genre(name="mathcore") }}. Suffice it to say that without
<b>Deadguy</b>, there would be no <b>The Dillinger Escape Plan</b> (also from
[New Jersey](/provinces/new-jersey-united-states)) and no <b>Botch</b>.

&ldquo;Fixation on a Coworker&rdquo; was the last <b>Deadguy</b> release
featuring founding members <b>Keith Huckins</b> and <b>Tim Singer</b>, who
departed from the band to form <b>Kiss It Goodbye</b> (also {{
genre(name="mathcore") }}/{{ genre(name="metalcore") }}) with former
<b>Rorschach</b> members <b>Thom Rusnack</b> and <b>Andrew Gormley</b>.

An official <b>Deadguy</b> documentary is slated to be released in late {{ time(t="2020") }}. You can watch the trailer (released {{ time(t="2020-04-15", dt="2020-04-15") }}) [on YouTube
here](https://www.youtube.com/watch?v=fYytaazlsWg). There is also an associated
website here: <https://www.deadguykillingmusic.com/>
([archived][deadguy-archived]).

### notes on the distributions

&ldquo;Whitemeat&rdquo; is sometimes listed as &ldquo;White Meat&rdquo;.

On some releases, &ldquo;Screamin' With The Deadguy Quintet&rdquo; does not
list the final track, &ldquo;Prosthetic Head&rdquo;.

{{ discog() }}

[metre]: https://en.wikipedia.org/wiki/Metre_(music)
[chord]: https://en.wikipedia.org/wiki/Chord_(music)
[deadguy-archived]: https://web.archive.org/web/20200503232050/https://www.deadguykillingmusic.com/
