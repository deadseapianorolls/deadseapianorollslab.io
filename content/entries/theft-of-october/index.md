+++
title = "Theft Of October"
date = 2020-04-01

[taxonomies]
genres = [
    "dream pop",
    "math pop",
    "math rock",
    "indie rock",
    "midwest emo",
    "post-punk",
    "post-hardcore",
    "acoustic rock",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["t"]
first-release-dates = ["2011"]
release-dates = ["2011", "2013", "2016"]
labels = []
countries = ["Montenegro"]
provinces = ["Kotor, Montenegro"]
areas = ["Kotor, Kotor, Montenegro"]

[extra]
last_edited = "2020-04-23"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Theft Of October</b> was a solo project of a Montenegrin man by the name of
<b>Denis Balavac</b>. Although clearly influenced by {{ genre(name="math rock")
}} and {{ genre(name="math pop") }}, ToO&rsquo;s sound is unique due to, among
other things, the incorporation of {{ genre(name="dream pop") }} aesthetics and
the unusual use of {{ hs(name="drumkit") }} and {{ hs(name="piano") }}.
ToO&rsquo;s sound changed from the earlier {{ genre(name="math rock") }}/{{
genre(name="dream pop") }}/{{ genre(name="midwest emo") }} (see the first two
releases) to later {{ genre(name="indie rock") }}/{{ genre(name="acoustic
rock") }}/{{ genre(name="dream pop") }} while still retaining {{
genre(name="math rock") }} stylings like noodly {{ hs(name="guitar") }}s (see
&ldquo;Fears&rdquo;).

### notes on the distributions

[Discogs lists the self-titled demo][discogs] ([archived][discogs-archived])
with a release date of {{ time(t="2015") }}, although it existed
on YouTube in {{ time(t="2013") }}, so I&rsquo;ve counted {{ time(t="2013") }} as the &ldquo;actual&rdquo; date for that reason.
It is unclear which tracks belong in the first release and which in the second,
except that the first two tracks of &ldquo;Theft Of October&rdquo; are clearly
in the right place. The last two or three tracks on &ldquo;Theft Of
October&rdquo; are earlier demo tracks that may belong in the first release,
but effectively you can consider the first two releases to comprise all of
ToO&rsquo;s &ldquo;demo material&rdquo;. The tracks are split among the
releases to reflect Discogs&rsquo;s listing.

&ldquo;Fears&rdquo; was never released officially <i>per se</i>, but the album
was uploaded to YouTube by Balavac himself on {{ time(t="2019-09-02", dt="2019-09-02") }}. Nevertheless, the release date is
listed as {{ time(t="2016") }} because that is the date given by
Balavac. No song titles were given, so the track names are here left blank.

Unfortunately, the first two releases here are quite dubious because they were
ripped from YouTube (from videos that are unfortunately no longer available on
the site) and then transcoded into CBR 256kb/s MP3.

&ldquo;Fears&rdquo; was uploaded by Balavac himself to YouTube, so I have taken
the liberty of ripping the exact audio that is hosted on YouTube. And, in
addition, for ease-of-use I also include the same audio split into six
(untitled) tracks. Unfortunately, there is no good way that i know of to split
this audio file into separate files in a 100% lossless way; that is why the
uncut version and the cut-into-tracks versions are both included. Below are the
exact commands used to obtain the cut-into-tracks version of
&ldquo;Fears&rdquo; (some sources on the internet claim that the following
method is lossless, but
[inverting](https://en.wikipedia.org/wiki/Phase_cancellation) the cut version
and playing alongside the original in [Audacity](https://www.audacityteam.org/)
suggests otherwise):

```bash
# <https://github.com/ytdl-org/youtube-dl/>
youtube-dl -x "https://www.youtube.com/watch?v=rUbp2vz41II"
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss    0 -to  128 -map 0:a -c copy ./Fears1.opus
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss  128 -to  310 -map 0:a -c copy ./Fears2.opus
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss  310 -to  649 -map 0:a -c copy ./Fears3.opus
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss  649 -to  796 -map 0:a -c copy ./Fears4.opus
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss  796 -to 1056 -map 0:a -c copy ./Fears5.opus
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.opus" -ss 1056 -to 9999 -map 0:a -c copy ./Fears6.opus
```

Also, I could not find the original album art through various internet
searches, so the &ldquo;Fears&rdquo; album art given here was obtained as
follows:

```bash
youtube-dl "https://www.youtube.com/watch?v=rUbp2vz41II"
ffmpeg -i "Theft of October - Fears (2016) EP-rUbp2vz41II.webm" -vf "select=eq(n\,0)" -vframes 1 fears.png
# cropped in GIMP <https://gimp.org/> to remove letterboxing
```

The cover art for &ldquo;Theft Of October&rdquo; [is due to <b>Yvette
Young</b>](https://yvetteyoung.wordpress.com/2013/08/16/theft-of-october/)
([archived][yy-archived]).

{{ discog() }}

[discogs]: https://www.discogs.com/Theft-Of-October-Theft-Of-October/release/7793288
[discogs-archived]: https://web.archive.org/web/20170717061801/https://www.discogs.com/Theft-Of-October-Theft-Of-October/release/7793288
[yy-archived]: https://web.archive.org/web/20200411224455/https://yvetteyoung.wordpress.com/2013/08/16/theft-of-october/
