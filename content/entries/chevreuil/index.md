+++
title = "Chevreuil"
date = 2020-07-13

[taxonomies]
genres = [
    "math rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "post-rock",
    "rock",
    "pop",
]
first-letters = ["c"]
first-release-dates = ["2000"]
release-dates = ["2000", "2001", "2003", "2004", "2006"]
labels = [
    "RuminanCe",
    "Ottonecker",
    "Sickroom Records",
    "Stiff Slack",
    "Effervescence",
    "Gentlemen Records",
]
countries = ["France"]
provinces = ["Loire-Atlantique, France"]
areas = ["Nantes, Loire-Atlantique, France"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Chevreuil</b> (in English: [roe
deer](https://en.wikipedia.org/wiki/Roe_deer)) was a {{ genre(name="math rock")
}} band from [Nantes](/areas/nantes-loire-atlantique-france), formed in {{ time(t="late 1998", dt="1998-11") }}. They dissolved around {{ time(t="2006") }} after having released four LPs, among other things.
<b>Chevreuil</b> consisted of <b>Julien Fernandez</b> on {{ hs(name="drums") }}
(he also played {{ hs(name="drums") }} for another [French](/countries/france)
{{ genre(name="math rock") }} outfit by the name of <b>Passe Montagne</b>) and
<b>Tony Chauvin</b> (who has a solo project by the name of
<b>Percevalmusic</b>) on {{ hs(name="guitar") }} and later also on {{
hs(name="analogue synthesizer") }}s. When performing, <b>Tony Chauvin</b> would
hook up his instrument(s) to four amplifiers pointing in four directions, and
the band would then play while surrounded by the audience on all sides. Their
last two LPs (&ldquo;Chateauvallon&rdquo; and &ldquo;(((Capoëira)))&rdquo;)
&mdash; as well as their &ldquo;Science&rdquo; EP &mdash; were produced by
<b>Steve Albini</b>.

It would seem that <b>Chevreuil</b> had a lot in common with another band that
has an entry here: [<b>Cheval de Frise</b>](/entries/cheval-de-frise). They
both were [French](/countries/france) {{ genre(name="math rock") }} outfits,
both consisted of a {{ hs(name="guitar") }} &amp; {{ hs(name="drums") }} duo,
both were on the [RuminanCe](/labels/ruminance) label, both formed and
dissolved at around the same times, and both had band names starting with
`Chev`! But if you were expecting music like that of <b>Cheval de Frise</b>,
you would be barking up the wrong tree. <b>Chevreuil</b> play a groove-oriented
style of {{ genre(name="math rock") }}, with <b>Tony Chauvin</b> making use of
a [looper pedal][looper-pedal] to create layered grooves over <b>Julien
Fernandez</b>&rsquo;s {{ hs(name="drumming") }}. Because of this orientation
around grooves, and the instrumental nature of <b>Chevreuil</b>&rsquo;s music,
they have often been labeled as {{ genre(name="post-rock") }} in addition to {{
genre(name="math rock") }}. Their music is, however, {{ genre(name="punk") }}
music (particularly, {{ genre(name="math rock") }}) at its core, and the focus
of their compositions is on creating highly technical interlocking rhythms.
<b>Chevreuil</b> make heavy use of both [polymeter][polymeter] and
[polyrhythm][polyrhythm], producing dizzying grooves that can often be
difficult to even follow. &ldquo;(((Capoëira)))&rdquo; is perhaps the
band&rsquo;s magnum opus, so if you check anything out by this band,
&ldquo;(((Capoëira)))&rdquo; is a good place to start.

### notes on the distributions

In the listing and distribution below, the final (untitled) track of
&ldquo;Sport&rdquo; is the hidden track, and the track before it
(&ldquo;Uventene&rdquo;) is mostly silence, with the actual tune being played
at the beginning of the track. The original {{ time(t="2001", dt="2001-02") }} version of &ldquo;Sport&rdquo; lacks both of
these tracks, and some versions lack the hidden track.

The original version of &ldquo;Ghetto Blaster&rdquo; does not have the track
&ldquo;Lead Ingénieur&rdquo;, and also lists the final track as &ldquo;Super
Jarre Rumo&rdquo;. In the listing and distribution below, the {{ time(t="2004", dt="2004-04") }} reissue is represented.

The {{ time(t="2005", dt="2005-03-08") }} [Sickroom
Records](/labels/sickroom-records) reissue of &ldquo;Chateauvallon&rdquo; has
an extra {{ time(t="57-second", dt="PT57S") }} track at the beginning
entitled &ldquo;Multipliance&rdquo;. The listing and distribution below do not
reflect this reissue, but rather the original [RuminanCe](/labels/ruminance)
release. &ldquo;Rock'n'roll Garnison&rdquo; is sometimes listed as
&ldquo;Rock'n roll Garnison&rdquo; or &ldquo;Rock'nrollgarnison&rdquo;.
&ldquo;Baseball Player&rdquo; is sometimes listed as
&ldquo;Baseballplayer&rdquo;. &ldquo;Forteresse Courage&rdquo; is sometimes
listed as &ldquo;Forteressecourage&rdquo;.

The distribution listed here for the split between <b>Chevreuil</b> and fellow
[Nantes](/areas/nantes-loire-atlantique-france) {{ genre(name="math rock")
}}ers <b>Room 204</b> only includes <b>Room 204</b>&rsquo;s contribution.

&ldquo;(((Capoëira)))&rdquo; is sometimes listed as
&ldquo;(((Capoeira)))&rdquo;, &ldquo;Capoëira&rdquo;, and/or
&ldquo;Capoeira&rdquo;.

{{ discog() }}

[looper-pedal]: https://en.wikipedia.org/wiki/Loop_%28music%29#Modern_looping
[polymeter]: https://en.wikipedia.org/wiki/Metre_(music)#Polymetre
[polyrhythm]: https://en.wikipedia.org/wiki/Polyrhythm
