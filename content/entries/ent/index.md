+++
title = "Ent"
date = 2020-04-11

[taxonomies]
genres = [
    "math rock",
    "experimental rock",
    "post-rock",
    "post-hardcore",
    "punk",
    "rock",
    "pop",
]
first-letters = ["e"]
first-release-dates = ["2000"]
release-dates = ["2000", "2001", "2002"]
labels = ["Omnibus"]
countries = ["United States"]
provinces = ["California, United States"]
areas = ["Sacramento, California, United States"]

[extra]
last_edited = "2020-07-24"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Ent</b> was an early 21<sup>st</sup>-century
[American](/countries/united-states) {{ genre(name="math rock") }} band that,
although largely unknown to this day, was one of the first bands (alongside,
for example, [Pittsburgh](/areas/pittsburgh-pennsylvania-united-states) natives
<b>Don Caballero</b>) to expound the often-instrumental 21<sup>st</sup>-century
style of {{ genre(name="math rock") }}, focusing on integrating intricate clean
(as opposed to distorted) {{ hs(name="guitar") }} and {{ hs(name="bass guitar")
}} grooves into the ultimately {{ genre(name="post-hardcore") }} origins of the
genre. <b>Ent</b> was also an early band in the 2000&rsquo;s
[Sacramento](/areas/sacramento-california-united-states) {{
genre(name="math rock") }} scene, which includes other classic {{
genre(name="math rock") }} bands such as <b>Hella</b>,
[<b>Dilute</b>](/entries/dilute), and <b>Tera Melos</b>.

Although <b>Ent</b> remains somewhat obscure, their music is nothing short
of superb. I highly recommend their self-titled album, and also checking out
<b>Carson McWhirter</b>&rsquo;s later works (both solo and with other groups).

<b>Ian Hill</b> is not to be confused with <b>Zach Hill</b>, another drummer
from the same scene.

### notes on the distributions

&ldquo;Ent&rdquo; was ripped from YouTube videos, using `youtube-dl -x` to rip
the highest-quality audio possible.

The other releases besides the self-titled are [due to an anonymous Reddit
user][reddit] ([archived][reddit-archived]), who uploaded any and all record of
Ent that they had.

All releases besides the two &ldquo;official&rdquo; releases are bundled as a
single distribution, for convenience.

The track listed last in the &ldquo;[collected]&rdquo; entry was included as
part of the original Reddit distribution, but is probably not actually related
to <b>Ent</b>. It appears to actually be by a different
[Sacramento](/areas/sacramento-california-united-states) band called <b>Samus
Aran</b>, included on a [2000](/release-dates/2000) compilation of
[Sacramento](/areas/sacramento-california-united-states) {{ genre(name="rock")
}} bands entitled &ldquo;[Rock Out With Your Sac Out][rock-out]&rdquo;
([archived][rock-out-archived]), distributed by [Joda
Records](/labels/joda-records).

{{ discog() }}

[reddit]: https://reddit.com/r/mathrock/comments/110w40/music_from_ent_old_sacramento_math_rock_band/
[reddit-archived]: https://web.archive.org/web/20200411223346/https://old.reddit.com/r/mathrock/comments/110w40/music_from_ent_old_sacramento_math_rock_band/
[rock-out]: https://www.discogs.com/Various-Rock-Out-With-Your-Sac-Out/release/14534618
[rock-out-archived]: https://web.archive.org/web/20200411223628/https://www.discogs.com/Various-Rock-Out-With-Your-Sac-Out/release/14534618
