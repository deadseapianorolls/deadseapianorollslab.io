+++
title = "Snöras"
date = 2020-07-26

[taxonomies]
genres = [
    "screamo",
    "post-hardcore",
    "math rock",
    "punk",
    "experimental rock",
    "rock",
    "pop",
]
first-letters = ["s"]
first-release-dates = ["2006"]
release-dates = ["2006", "2008", "2015"]
labels = [
    "Fysisk Format",
    "Anomalie Records",
    "Take It Easy Policy",
    "Great Northern Records",
    "Not Another Record Label",
]
countries = ["Norway"]
provinces = ["Oslo, Norway"]
areas = ["Oslo, Oslo, Norway"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Snöras</b> (in [English](https://en.wikipedia.org/wiki/English_language):
&ldquo;avalanche&rdquo;, although usually spelled
[<i>snøras</i>](https://nn.wikipedia.org/wiki/Sn%C3%B8ras)) was a {{
genre(name="screamo") }} band from [Oslo](/areas/oslo-oslo-norway) that formed
some time in {{ time(t="2005") }}. <b>Snöras</b> was primarily the project of
<b>Yngve Hilmo</b>, who did the songwriting, {{ hs(name="vocals") }}, and much
of the instrumentation. However, the member listing above also reflects other
musicians who have been members of <b>Snöras</b> (particularly when playing
live), especially <b>Mads Hornsletten</b>, who played most of the {{
hs(name="drums") }} for <b>Snöras</b> in addition to being the {{
hs(name="drummer") }} for <b>Kaospilot</b>, another {{ genre(name="screamo") }}
outfit also from [Oslo](/areas/oslo-oslo-norway). <b>Snöras</b> released a
trilogy of full-length records, with the first two installments in {{
time(t="2006") }} and {{ time(t="2008", dt="2008-09-08") }}, respectively,
followed by the final installment in {{ time(t="2015", dt="2015-05-01") }}.

<b>Snöras</b>&rsquo;s music is typically characterised by punchy eighth-note
chords and somewhat high tempi during heavier sections, which make use of the
full range of the {{ hs(name="guitar") }} and enjoy typically heavy syncopation
and off-kilter rhythmic stabs due to some influence of {{ genre(name="math
rock") }}. This is accompanied by the pained {{ hs(name="screams") }} &amp; {{
hs(name="yelps") }}, and the often complex and dynamic song structures typical
of {{ genre(name="screamo") }}. Although the vocal style and the consistent use
of these eighth-note block chords creates a somewhat homogeneous texture
throughout the first two albums, the exact way that these chords are composed
and the complex latent rhythmic structures make <b>Snöras</b>&rsquo;s music
unusually melodic/catchy, angular, and varied. Add in the mellower and more
texturally experimental sections that break this texture up, and you have the
approach that made <b>Snöras</b> one of the best bands that {{
genre(name="screamo") }} has to offer. The final installment in the trilogy,
&ldquo;Life in the Gutter&rdquo; ({{ time(t="2015", dt="2015-05-01") }}),
retains some of the qualities of the first two installments (if you listen
closely enough), but has an intentionally _very_
[lo-fi](https://en.wikipedia.org/wiki/Lo-fi_music) aesthetic that makes
picking out individual notes rather difficult; as self-described: &ldquo;The
album goes back to square one: No frills. No fancy stuff. Just a mess of drums,
guitar and screaming straight to tape&rdquo;.

{{ discog() }}
