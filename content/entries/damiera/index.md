+++
title = "Damiera"
date = 2020-08-18

[taxonomies]
genres = [
    "math pop",
    "post-hardcore",
    "math rock",
    "experimental rock",
    "pop-punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["2005"]
release-dates = ["2005", "2006", "2008"]
labels = [
    "Equal Vision Records",
    "Tamerlane Recordings",
    "Built on Strength Records",
]
countries = ["United States"]
provinces = ["New York, United States"]
areas = ["Erie County, New York, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Damiera</b> was a {{ genre(name="math pop") }}/{{
genre(name="post-hardcore") }} band from [Buffalo, New
York](/areas/erie-county-new-york-united-states) that formed in {{
time(t="early 2005", dt="2005-02") }}. The band formed out of the dissolution
of [Fredonia, New York](/areas/chautauqua-county-new-york-united-states) outfit
<b>League</b>; {{ hs(name="guitar") }}ist and {{ hs(name="vocal") }}ist
<b>David Raymond</b> and {{ hs(name="guitar") }}ist <b>Matthew Kipp</b>, both
of <b>League</b>, formed the band with {{ hs(name="drummer") }} <b>Bradley
McRae</b> and {{ hs(name="bass guitar") }}ist <b>Mark Henry</b>. The band
immediately set out to produce their debut self-titled {{ time(t="2005") }} EP,
and later released their debut LP (&ldquo;M(US)IC&rdquo; ({{ time(t="2006",
dt="2006-05-16") }}), pronounced &ldquo;us in music&rdquo;) on now-defunct
[Tamerlane Recordings](/labels/tamerlane-recordings). In {{ time(t="2007") }},
the band signed to [Equal Vision](/labels/equal-vision-records) and re-released
&ldquo;M(US)IC&rdquo; on that label. The band took a two-month hiatus during
{{ time(t="2007") }}, and came back in {{ time(t="June of that year",
dt="2007-06-13") }} with a new lineup. The band released their second LP and
final record, &ldquo;Quiet Mouth Loud Hands&rdquo;, on [Equal
Vision](/labels/equal-vision-records) in {{ time(t="2008", dt="2008-06-24") }}.
<b>David Raymond</b> went on to form
[Chicago](/areas/chicago-illinois-united-states) {{
genre(name="experimental rock") }} otufit <b>Hidden Hospitals</b> in {{
time(t="2011") }}.

See [punknews.org for a partial biography of the band][punknews]
([archived][punknews-archived]).

<b>Damiera</b>&rsquo;s music is characterised by its focus on duelling {{
hs(name="guitar") }}s and on <b>David Raymond</b>&rsquo;s {{ genre(name="pop")
}}py [clean {{ hs(name="vocals") }}][clean-vocals]. Much of
<b>Damiera</b>&rsquo;s music (particularly on &ldquo;Damiera&rdquo; ({{
time(t="2005") }}) and &ldquo;M(US)IC&rdquo; ({{ time(t="2006",
dt="2006-05-16") }})) is [polyphonic][polyphonic], with multiple melodies
interleaved within the arrangements. <b>Damiera</b> are, in some ways,
comparable to [Philadelphia](/areas/philadelphia-pennsylvania-united-states) {{
genre(name="post-hardcore") }} outfit [<b>Hot Cross</b>](/entries/hot-cross),
with their clear {{ genre(name="post-hardcore") }}/{{ genre(name="math rock")
}} influences and duelling {{ hs(name="guitar") }}work that is angular and
technically demanding &mdash; but <b>Damiera</b>&rsquo;s origins in {{
genre(name="pop-punk") }} (rather than in {{ genre(name="screamo") }}) come
through in the {{ hs(name="vocals") }}, harmonic sensibilities, and in the
mangled {{ genre(name="pop") }} song structures that are characteristic of {{
genre(name="math pop") }}. With &ldquo;Quiet Mouth Loud Hands&rdquo; ({{
time(t="2008", dt="2008-06-24") }}), <b>Damiera</b>&rsquo;s sound shifted
somewhat, shedding some (but certainly not all) of the {{
genre(name="math rock") }} influence; this would foreshadow <b>David
Raymond</b> and <b>Steve Downs</b>&rsquo;s later work as members of <b>Hidden
Hospitals</b>.

### notes on the distributions

&ldquo;Quiet Mouth Loud Hands&rdquo; is sometimes known by other similar names,
including &ldquo;QuietMouthLoudHands&rdquo; and &ldquo;Quiet Mouth, Loud
Hands&rdquo;.

{{ discog() }}

[punknews]: https://www.punknews.org/bands/damiera
[punknews-archived]: https://web.archive.org/web/20150912233100/https://www.punknews.org/bands/damiera
[clean-vocals]: https://en.wikipedia.org/wiki/Singing
[polyphonic]: https://en.wikipedia.org/wiki/Polyphony
