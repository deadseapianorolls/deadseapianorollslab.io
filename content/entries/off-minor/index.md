+++
title = "Off Minor"
date = 2020-04-23

[taxonomies]
genres = [
    "screamo",
    "math rock",
    "post-hardcore",
    "experimental rock",
    "jazz punk",
    "punk",
    "post-rock",
    "rock",
    "jazz",
    "pop",
]
first-letters = ["o"]
first-release-dates = ["2001"]
release-dates = ["2001", "2002", "2004", "2005", "2008"]
labels = [
    "EarthWaterSky Connection",
    "Level Plane",
    "Golden Brown Records",
    "PurePainSugar",
    "Paramnesia Records",
    "Narshardaa Records",
    "Appliances & Cars",
    "Kickstart My Heart Records",
    "Yellow Ghost Records",
    "oto Records",
]
countries = ["United States"]
provinces = ["New York, United States"]
areas = ["New York City, New York, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Off Minor</b> (named for the <b>Thelonious Monk</b> composition) was one of
two major bands that formed in the wake of <b>Saetia</b>&rsquo;s demise. The
other such band was <b>Hot Cross</b>; the original {{ hs(name="bassist") }} of
<b>Off Minor</b>, <b>Matt Smith</b>, left <b>Off Minor</b> to play in <b>Hot
Cross</b>. {{ hs(name="Drummer") }} <b>Steve Roche</b>&rsquo;s brother,
<b>Kevin Roche</b>, replaced Smith and this solidified the <b>Off Minor</b>
lineup. Hopefully I don&rsquo;t have to specify who <b>Saetia</b> were; if
you&rsquo;re not familiar, just enter &ldquo;{{ genre(name="screamo") }}&rdquo;
into your search engine of choice and you&rsquo;ll find them very quickly.
<b>Off Minor</b>, like <b>Saetia</b>, were an
[NYC](/areas/new-york-city-new-york-united-states)-based band. Also like
<b>Saetia</b>, <b>Off Minor</b> are behemoths of [the genre](/genres/screamo),
and &ldquo;The Heat Death Of The Universe&rdquo; (alongside their other albums)
is considered one of the finest {{ genre(name="screamo") }} records ever
produced.

While <b>Saetia</b> already had their own share of {{ genre(name="math rock")
}} and {{ genre(name="jazz punk") }} influences &mdash; after all, they too
were named for a {{ genre(name="jazz") }} composition &mdash; <b>Off Minor</b>
took these influences in a more saturated form and brought them to new heights.
Playing as a [power trio](https://en.wikipedia.org/wiki/Power_trio), the music
of <b>Off Minor</b> is instrumentally equal parts {{ hs(name="bass guitar") }},
{{ hs(name="guitar") }}, and {{ hs(name="drums") }}, rather than focusing on
any particular instrument(s). Their music delicately weaves these instruments
together, often producing outright
[counterpoint](https://en.wikipedia.org/wiki/Counterpoint), but within the
context of {{ genre(name="screamo") }}. The influence of {{ genre(name="math
rock") }} is manifest in the intricate part-writing,
[rhythms](https://en.wikipedia.org/wiki/Syncopation), [complex time
signatures][complex-time], &amp;c.; the influence of {{ genre(name="jazz punk")
}} is manifest in the [harmonic
palette](https://en.wikipedia.org/wiki/Jazz_harmony),
[phrasing](https://en.wikipedia.org/wiki/Musical_phrasing), and occasional use
of [improvisation](https://en.wikipedia.org/wiki/Improvisation).

### notes on the distributions

The first release listed here (&ldquo;Off Minor / i am the Resurrection&rdquo;)
includes not just the original split record (the first 13 tracks), but also the
live bonus tracks from the self-titled(?) reissue of <b>Off Minor</b>&rsquo;s
side of the split.

The dspr distribution of &ldquo;Off Minor / Ampere&rdquo; only contains <b>Off
Minor</b>&rsquo;s side of the split. For <b>Ampere</b>&rsquo;s side, check out
the third track of the compilation listed in the other distribution.

{{ discog() }}

[complex-time]: https://en.wikipedia.org/wiki/Time_signature#Complex_time_signatures
