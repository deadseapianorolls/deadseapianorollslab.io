+++
title = "Evan Brewer"
date = 2020-05-10

[taxonomies]
genres = [
    "progressive rock",
    "funk rock",
    "jazz fusion",
    "jazz-funk",
    "experimental rock",
    "funk metal",
    "experimental metal",
    "electronic",
    "progressive metal",
    "western chamber",
    "funk",
    "jazz",
    "western classical",
    "metal",
    "rock",
    "rhythm and blues",
    "pop",
]
first-letters = ["e"]
first-release-dates = ["2011"]
release-dates = ["2011", "2013"]
labels = ["Sumerian Records"]
countries = ["United States"]
provinces = ["Tennessee, United States"]
areas = ["Nashville, Tennessee, United States"]

[extra]
last_edited = "2020-07-30"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Evan Brewer</b> is an [American](/countries/united-states) {{ hs(name="bass
guitar") }}ist, best known for his work as part of {{
genre(name="progressive metal") }}/{{ genre(name="technical death metal")
}}/{{ genre(name="metalcore") }} bands such as <b>The Faceless</b>,
<b>Animosity</b>, <b>Entheos</b>, and <b>Reflux</b>. He has also, however,
released excellent solo work. His first solo album, &ldquo;Alone&rdquo;, is
truly a solo album and _solely_ features sounds produced by Brewer using
electric {{ hs(name="bass guitar") }}s. The influence of the Wootens is clear
on this album, although it is effectively a {{ genre(name="funk") }}y {{
genre(name="western chamber") }} music album, with influences from {{
genre(name="progressive rock") }}/{{ genre(name="progressive metal") }}. His
second solo album features performances by <b>Navene Koperweis</b> (<b>Animals
as Leaders</b>, <b>Animosity</b>, <b>Entheos</b>, <b>Navene K</b>) and
<b>Jeremiah Abel</b>, and incorporates {{ genre(name="electronic") }}
influences into its {{ genre(name="progressive rock") }}/{{
genre(name="jazz-funk") }} sound. Both are highly unique albums that are worth
listening to, if only to see whether or not you like the unique things that
they have to offer.

{{ discog() }}
