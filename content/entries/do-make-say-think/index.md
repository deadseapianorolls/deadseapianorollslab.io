+++
title = "Do Make Say Think"
date = 2020-06-21

[taxonomies]
genres = [
    "post-rock",
    "experimental rock",
    "ambient",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["1995"]
release-dates = [
    "1995",
    "1997",
    "1999",
    "2000",
    "2002",
    "2003",
    "2007",
    "2008",
    "2009",
    "2017",
]
labels = [
    "Constellation",
    "B-Side Sound Productions",
    "Resonant",
    "Independent Label Council Japan",
    "Zankyo Record",
]
countries = ["Canada"]
provinces = ["Ontario, Canada"]
areas = ["Greater Toronto Area, Ontario, Canada"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Do Make Say Think</b> is a {{ genre(name="post-rock") }} band from
[Toronto](/areas/greater-toronto-area-ontario-canada), first formed in {{ time(t="1995") }}. While their debut (self-titled) album was not
originally released on [Constellation](/labels/constellation) in {{ time(t="1997") }}, it was reissued by the label {{ time(t="the next year", dt="1998-09-21") }}, and the band has worked almost
exclusively with [Constellation](/labels/constellation) since then. <b>Do Make
Say Think</b> consists of several multi-instrumentalists and features a wide
variety of instrumentation in their music, but they are perhaps best known
instrumentation-wise for their {{ hs(name="bass guitar") }} work and double {{
hs(name="drumming") }} (<b>James Payment</b> and <b>Dave Mitchell</b> both {{
hs(name="drumming") }} for the band). The band is closely associated with
<b>Broken Social Scene</b>, an [experimental](/genres/experimental-rock) {{
genre(name="indie rock") }} collective featuring <b>Ohad Benchetrit</b> and
<b>Charles Spearin</b> of <b>Do Make Say Think</b>. Having released seven
(eight, if you count &ldquo;The Whole Story Of Glory&rdquo;) full-length
records as of this writing, <b>Do Make Say Think</b> continues to be one of the
most influential, well-respected, and unique bands associated with the
&ldquo;{{ genre(name="post-rock") }}&rdquo; label.

DMST were originally very inspired by the music of seminal
[Chicago](/areas/chicago-illinois-united-states) {{ genre(name="post-rock")
}}ers <b>Tortoise</b> (and members of DMST have since worked with
<b>Tortoise</b> {{ hs(name="drummer") }} <b>John McEntire</b> as part of
<b>Broken Social Scene</b>). <b>Do Make Say Think</b>&rsquo;s {{ time(t="1997") }} self-titled album retains the {{ genre(name="dub")
}} and {{ genre(name="krautrock") }} influences of <b>Tortoise</b>, albeit with
DMST&rsquo;s trademark double-{{ hs(name="drumming") }} sound, significant use
of {{ hs(name="wind instruments") }}, some oddly catchy melodies and basslines,
and even a heavy release-of-buildup section (see: &ldquo;Disco &amp;
Haze&rdquo;). Later releases somewhat downplayed these {{
genre(name="electronic") }} influences, favoring a style even more influenced
by american folk music than &ldquo;Do Make Say Think&rdquo; already was. The
influence of deeply {{ genre(name="electronic") }} forms of music like the
aforementioned {{ genre(name="dub") }} and {{ genre(name="krautrock") }},
combined with the influence of american folk music, the band&rsquo;s
complex/unusual but coherent (and often essentially monothematic(!))
compositional structures, use of a wide variety of instrumentation and of
improvisation, unique double-{{ hs(name="drumming") }} and {{
hs(name="bass guitar") }} styles, and essentially {{ genre(name="punk") }}
&amp; [DIY](https://en.wikipedia.org/wiki/DIY_ethic) ethic (similar to
labelmates <b>Godspeed You! Black Emperor</b> and <b>A Silver Mt. Zion</b>) has
made <b>Do Make Say Think</b> the fiercely unique and respected {{
genre(name="post-rock") }} band that we know today.

### notes on the distributions

The cassette listed here as &ldquo;\[demo\]&rdquo; has an uncertain release
date (as it was never officially &ldquo;released&rdquo; <i>per se</i>, being a
simple cassette demo); it may have been released in {{ time(t="1995") }} (and this is the date given by Discogs), or
possibly in {{ time(t="1996") }} or perhaps even sometime in {{ time(t="1997") }} before the release of their debut. It was uncovered
in {{ time(t="2011") }}, with the details and some recordings
posted to [the Weird Canada
blog](https://weirdcanada.com/2011/07/departures-do-make-say-think-demo/)
([archived][wc-archived]).

&ldquo;Besides&rdquo; was a vinyl-only release, released by
[Resonant](/labels/resonant) rather than
[Constellation](/labels/constellation). According to [the album&rsquo;s
Wikipedia page][besides], the liner notes read:

> Theres not much to do in Toronto on Tuesday night. Mabie yer friends and
> their problems have got the best of you and if its February on top of that,
> yer doomed. The highest of praise for the 8-track comes at these moments. The
> songs recorded are the purest of inspiration. Not intended for anyone elses
> ears but your own at the time. They are "the undeniable truths". Because
> after the stolen bikes, stoned Sunday afternoons, bounced rent cheques,
> awkward conversations with x-girlfriends, sunsets, pet deaths, first kisses
> and last goodbyes itsa good idea to hit play &amp; record. Yes.

&ldquo;The Whole Story Of Glory&rdquo; was a [Japan](/countries/japan)-only
tour CD.

On &ldquo;Stubborn Persistent Illusions&rdquo;, &ldquo;As Far as the Eye Can
See&rdquo; is the alternative title for &ldquo;d=3.57&radic;h&rdquo;. The title
used here is the one associated with the digital release (presumably used for
its ability to be accurately represented in Unicode&hellip;), as listed on the
official bandcamp page.

{{ discog() }}

[besides]: https://en.wikipedia.org/wiki/Besides_(EP)
[wc-archived]: https://web.archive.org/web/20170415130325/https://weirdcanada.com/2011/07/departures-do-make-say-think-demo/
