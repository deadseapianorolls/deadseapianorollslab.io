+++
title = "Rooftops"
date = 2020-04-18

[taxonomies]
genres = [
    "math rock",
    "post-rock",
    "experimental rock",
    "post-hardcore",
    "midwest emo",
    "punk",
    "indie rock",
    "rock",
    "pop",
]
first-letters = ["r"]
first-release-dates = ["2008"]
release-dates = ["2008", "2009"]
labels = ["Clickpop Records", "Topshelf Records"]
countries = ["United States"]
provinces = ["Washington, United States"]
areas = ["Bellingham, Washington, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Rooftops</b> was a {{ genre(name="math rock") }}/{{ genre(name="post-rock")
}} hybrid band from [Bellingham, Washington,
U.S.A.](/areas/bellingham-washington-united-states) Although &mdash; as far as
I know &mdash; they only released one proper album (most sources only list this
album) and a split, they are fairly well-known for their expertly beautiful
combination of elements of {{ genre(name="math rock") }}, {{
genre(name="post-rock") }}, and even hints of {{ genre(name="midwest emo") }}.

<b>Rooftops</b> hailed from [Bellingham,
Washington](/areas/bellingham-washington-united-states), a medium-sized city in
between the metro areas of [Seattle](/areas/seattle-washington-united-states)
and [Vancouver, B.C.](/areas/vancouver-british-columbia-canada) They signed to
[Clickpop Records](/labels/clickpop-records), also based in
[Bellingham](/areas/bellingham-washington-united-states), in {{ time(t="December of 2008", dt="2008-12") }}. This was after releasing their
split with [<b>noumenon</b>](/entries/noumenon), which presumably was
self-released (not on any label). In {{ time(t="November of
2009", dt="2009-11") }}, [Clickpop Records](/labels/clickpop-records) released their debut
LP, &ldquo;A Forest of Polarity&rdquo;, an album whose song titles are all
anagrams of one another. {{ time(t="The following year", dt="2010") }},
[Topshelf Records](/labels/topshelf-records) (re-)issued the LP, which is why
some sources erroneously list its release year as
{{ time(t="2010") }}.

Two of the members of <b>Rooftops</b> (Drew Fitchette &amp; Wendelin
Wohlgemuth) would go on to form <b>Detlef</b>, another band also in the {{
genre(name="math rock") }} style.

A series of photos taken at a <b>Rooftops</b> show on
{{ time(t="2008-11-22", dt="2008-11-22") }} at The Rogue Hero (now defunct),
due to [Paul Turpin](https://www.flickr.com/people/paulturpin/), can be found
here:

<!-- markdownlint-disable MD033 -->
<ul class="inline">
  <li><a href="the-rogue-hero-00.jpg">00</a></li>
  <li><a href="the-rogue-hero-01.jpg">01</a></li>
  <li><a href="the-rogue-hero-02.jpg">02</a></li>
  <li><a href="the-rogue-hero-03.jpg">03</a></li>
  <li><a href="the-rogue-hero-04.jpg">04</a></li>
  <li><a href="the-rogue-hero-05.jpg">05</a></li>
  <li><a href="band.jpg">06</a></li>
  <li><a href="the-rogue-hero-07.jpg">07</a></li>
  <li><a href="the-rogue-hero-08.jpg">08</a></li>
  <li><a href="the-rogue-hero-09.jpg">09</a></li>
  <li><a href="the-rogue-hero-10.jpg">10</a></li>
  <li><a href="the-rogue-hero-11.jpg">11</a></li>
  <li><a href="the-rogue-hero-12.jpg">12</a></li>
  <li><a href="the-rogue-hero-13.jpg">13</a></li>
  <li><a href="the-rogue-hero-14.jpg">14</a></li>
  <li><a href="the-rogue-hero-15.jpg">15</a></li>
  <li><a href="the-rogue-hero-16.jpg">16</a></li>
</ul>
<!-- markdownlint-enable MD033 -->

### notes on the distributions

[<b>noumenon</b>](/entries/noumenon)&rsquo;s bandcamp used to have
an additional compilation release listed, although it can no longer be found.
Back when it existed, I downloaded it &mdash; unfortunately, the same problem
occured here that occured with [<b>The Brave Little
Abacus</b>](/entries/the-brave-little-abacus). At the time, I downloaded in CBR
320kb/s MP3, so that&rsquo;s all I have here. The compilation included all 4
tracks from the [<b>noumenon</b>](/entries/noumenon)/<b>Rooftops</b> split, so
they are included here.

{{ discog() }}
