+++
title = "Pigment Vehicle"
date = 2020-05-17

[taxonomies]
genres = [
    "post-hardcore",
    "math rock",
    "noise rock",
    "experimental rock",
    "post-punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["p"]
first-release-dates = ["1988"]
release-dates = ["1988", "1992", "1993", "1996", "1998"]
labels = ["Incentive Records", "Wrong Records", "Sudden Death Records"]
countries = ["Canada"]
provinces = ["British Columbia, Canada"]
areas = ["Greater Victoria, British Columbia, Canada"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Pigment Vehicle</b> were a very early {{ genre(name="math rock") }} band
from [Victoria, B.C.](/areas/greater-victoria-british-columbia-canada)
Information on this band, and recordings of their music, are scarce; see
[Fecking Bahamas&rsquo; coverage][fb] ([archived][fb-archived]) and the [Live
Victoria thread][lv] linked therein ([archived][lv-archived]). They are
associated with fellow
[Victorians](/areas/greater-victoria-british-columbia-canada) <b>Nomeansno</b>,
playing in a similarly [noisy](/genres/noise-rock), sometimes goofy, and
sometimes [mathy](/genres/math-rock) style of {{ genre(name="post-hardcore")
}}/{{ genre(name="post-punk") }}/{{ genre(name="punk") }}. If
<b>Nomeansno</b>&rsquo;s works like &ldquo;Sex Mad&rdquo; ({{ time(t="1986")
}}), &ldquo;Small Parts Isolated and Destroyed&rdquo; ({{ time(t="1988") }}),
and &ldquo;Wrong&rdquo; ({{ time(t="1989") }}) were proto-{{
genre(name="math rock") }}, then the works of their apprentice, <b>Pigment
Vehicle</b>, were arguably early artifacts of {{ genre(name="math rock") }}
proper.

&ldquo;Hockey Night in Saskatoon&rdquo;, the band&rsquo;s {{ time(t="1988") }}
cassette demo, is arguably already [mathy](genres/math-rock) enough to count as
a full-blown {{ genre(name="math rock") }} album, which would make them a very
early entry into the {{ genre(name="math rock") }} canon. For {{
genre(name="math rock") }} fans, their first fully-formed effort in this
respect is probably &ldquo;Perfect Cop Mustache&rdquo;. &ldquo;Murder's Only
Foreplay When You're Hot For Revenge&rdquo; is similarly essential and perhaps
their &ldquo;best-known&rdquo; record, although this distinction may not be
meaningful, since whatever small influence <b>Pigment Vehicle</b> may have had,
it unfortunately did not stay with time.

### notes on the distributions

<b>Pigment Vehicle</b>&rsquo;s contribution to &ldquo;It Came From
Victoria&rdquo; is the 3<sup>rd</sup> track on the record overall. The same
<b>Pigment Vehicle</b> recording also shows up on &ldquo;Perfect Cop
Mustache&rdquo;, but in the distributions here, they are compressed
differently, so both are included.

&ldquo;Murder's Only Foreplay When You're Hot For Revenge&rdquo; lists a
thirteenth track (&ldquo;Boeuf Mon Amour&rdquo;), but there are reportedly only
twelve tracks on the original CD.

{{ discog() }}

[fb]: https://feckingbahamas.com/math-rock-retrospective-pigment-vehicle-1988-1998
[lv]: https://livevictoria.com/?action=browse_messageboard&expand_user_info=433795&current_board=33&current_subject=379140
[fb-archived]: https://web.archive.org/web/20191101201451/http://feckingbahamas.com/math-rock-retrospective-pigment-vehicle-1988-1998
[lv-archived]: https://web.archive.org/web/20191105094717/https://livevictoria.com/?action=browse_messageboard&expand_user_info=433795&current_board=33&current_subject=379140
