+++
title = "Storm & Stress"
date = 2020-04-21

[taxonomies]
genres = [
    "math rock",
    "free improvisation",
    "experimental rock",
    "post-hardcore",
    "free jazz",
    "jazz punk",
    "punk",
    "post-rock",
    "avant-prog",
    "jazz",
    "progressive rock",
    "rock",
    "pop",
]
first-letters = ["s"]
first-release-dates = ["1997"]
release-dates = ["1997", "2000"]
labels = ["Touch and Go"]
countries = ["United States"]
provinces = ["Pennsylvania, United States", "Illinois, United States"]
areas = [
    "Pittsburgh, Pennsylvania, United States",
    "Chicago, Illinois, United States",
]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="png") }}

<b>Storm &amp; Stress</b> (often spelled with no spaces and/or with
&ldquo;and&rdquo; instead of &ldquo;&amp;&rdquo;) was a {{
genre(name="math rock") }}/{{ genre(name="free improvisation") }} hybrid band
originally from [Pittsburgh,
Pennsylvania](/areas/pittsburgh-pennsylvania-united-states), although they
later relocated to [Chicago,
Illinois](/areas/chicago-illinois-united-states). <b>Storm &amp; Stress</b> are
often thought of as a side project of <b>Don Caballero</b>, although the music
of <b>Storm &amp; Stress</b> is &mdash; while still {{ genre(name="math rock")
}} &mdash; quite different from that of <b>Don Caballero</b>. The music of
<b>Storm &amp; Stress</b> is largely textural, focusing on the minute sonic
details of the {{ hs(name="guitar") }}, {{ hs(name="drums") }}, {{
hs(name="bass guitar") }}, and sparse {{ hs(name="vocals") }}. While <b>Storm
&amp; Stress</b>&rsquo;s music is not the _only_ music to cross between {{
genre(name="math rock") }} and improvisation (c.f. some
[Sacramento](/areas/sacramento-california-united-states)-scene bands like
<b>Hella</b>, <b>Tera Melos</b>, and <b>Dilute</b>, as well as the
[Japanese](/countries/japan) band <b>Ruins</b>, and
[Asheville](/areas/asheville-north-carolina-united-states) natives
<b>Ahleuchatistas</b>/<b>Lighted Stairs</b>), it is extremely unique in its
_commitment_ to {{ genre(name="free improvisation") }} and its overall sound.
Both of <b>Storm &amp; Stress</b>&rsquo;s albums are landmarks of {{
genre(name="math rock") }} history.

<b>George Draguns</b> originally played {{ hs(name="bass guitar") }} for the
band, but the main lineup was formed when <b>Draguns</b> was replaced by
<b>Eric Emm</b> (a.k.a. <b>Eric Ehm</b> or <b>Eric M. Topolsky</b>) on {{
hs(name="bass guitar") }}. <b>Ian Williams</b> also played {{ hs(name="guitar")
}} in <b>Don Caballero</b> (and still plays in <b>Battles</b>). <b>Kevin
Shea</b> is a {{ genre(name="jazz") }} {{ hs(name="drummer") }} who has been
part of many other groups, most notably <b>Talibam!</b> (and <b>Form and
Mess</b> alongside original {{ hs(name="bassist") }} <b>George Draguns</b>, a
clear allusion to <b>Storm &amp; Stress</b>). <b>Eric Emm</b> is one half of
the duo <b>The Brothers</b>, a.k.a. <b>Golan Globus</b>.

Also of note are the guests that <b>Storm &amp; Stress</b> featured on their
albums. Both albums feature guest performances from <b>Micah Gaugh</b>, and
&ldquo;Under Thunder &amp; Fluorescent Lights&rdquo; additionally features
guest performance from well-known {{ genre(name="jazz") }} {{
hs(name="drummer") }} <b>Jim Black</b>.

### notes on the distributions

Both albums were originally released by [Touch and Go](/labels/touch-and-go),
but are now out of print.

{{ discog() }}
