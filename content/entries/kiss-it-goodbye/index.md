+++
title = "Kiss It Goodbye"
date = 2020-07-19

[taxonomies]
genres = [
    "metalcore",
    "mathcore",
    "post-hardcore",
    "punk",
    "experimental metal",
    "experimental rock",
    "metal",
    "rock",
    "pop",
]
first-letters = ["k"]
first-release-dates = ["1996"]
release-dates = ["1996", "1997", "1999"]
labels = ["Revelation Records"]
countries = ["United States"]
provinces = ["Washington, United States"]
areas = ["Seattle, Washington, United States"]

[extra]
last_edited = "2020-07-30"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Kiss It Goodbye</b> was a {{ genre(name="metalcore") }}/{{
genre(name="mathcore") }} band from
[Seattle](/areas/seattle-washington-united-states) that formed in {{
time(t="1996") }} out of the ashes of <b>Rorschach</b> and the departure of {{
hs(name="vocal") }}ist <b>Tim Singer</b> and {{ hs(name="guitar") }}ist
<b>Keith Huckins</b> from [<b>Deadguy</b>](/entries/deadguy). Both
<b>Rorschach</b> and [<b>Deadguy</b>](/entries/deadguy) were [New
Jersey](/provinces/new-jersey-united-states) {{ genre(name="metalcore") }}
acts, but upon <b>Singer</b> &amp; <b>Huckins</b>&rsquo;s departure from
[<b>Deadguy</b>](/entries/deadguy), they moved to
[Seattle](/areas/seattle-washington-united-states) to start a band with former
<b>Rorschach</b> members <b>Andrew Gormley</b> ({{ hs(name="drums") }}) and
<b>Tom Rusnak</b> ({{ hs(name="bass guitar") }}). <b>Huckins</b> was also a
former <b>Rorschach</b> member. Some time before the recording of the first
three tracks on &ldquo;Choke&rdquo; (but after the recording of the
&ldquo;Target Practice <abbr title="backed with">b/w</abbr> Preacher&rdquo; {{
time(t="1997") }} single), <b>Huckins</b> left the band and was replaced by
<b>Demian Johnston</b>, who had recently done a split with legendary [Tacoma,
Washington](/areas/tacoma-washington-united-states) {{ genre(name="mathcore")
}} outfit <b>Botch</b> as part of his band <b>Nineironspitfire</b>. The first
three tracks of &ldquo;Choke&rdquo; were recorded with <b>Johnston</b> in {{
time(t="1998") }}, but <b>Tim Singer</b> left the band and thus <b>Kiss It
Goodbye</b> was dissolved. &ldquo;Choke&rdquo; was released {{
time(t="posthumously", dt="1999-11-02") }} as an EP. <b>Johnston</b>,
<b>Gormley</b>, and <b>Rusnak</b> would all go on to form another {{
genre(name="mathcore") }} outfit by the name of <b>Playing Enemy</b>.

Musically, any fans of <b>Rorschach</b> and/or
[<b>Deadguy</b>](/entries/deadguy) will, naturally enough, love <b>Kiss It
Goodbye</b>. <b>Kiss It Goodbye</b> mixes the kind of &ldquo;{{
genre(name="metal") }}lic [hardcore](/genres/hardcore-punk)&rdquo; songwriting
and arrangements of early {{ genre(name="metalcore") }} with the trademark
pissed-off and melodramatic {{ hs(name="yelling") }} of <b>Tim Singer</b>, and
with some of the chaotic twists and turns of early {{ genre(name="mathcore")
}}, to boot. Unfortunately, their only full-length, &ldquo;She Loves Me, She
Loves Me Not&hellip;&rdquo; has (in my opinion) rather lackluster
[mastering][mastering]. Fortunately, &ldquo;Choke&rdquo; &mdash; and
[&ldquo;Fixation on a Coworker&rdquo;](/entries/deadguy), for that matter
&mdash; were [mastered][mastering] by <b>Alan Douches</b>, and as a result
sound pretty snappy.

### notes on the distributions

&ldquo;Be Afraid&rdquo; was a cassette demo.

&ldquo;Choke&rdquo; was released posthumously, and features both tracks from
their earlier &ldquo;Target Practice <abbr title="backed with">b/w</abbr>
Preacher&rdquo; {{ time(t="1997") }} single. For that reason, the single is not
represented in the discography below.

{{ discog() }}

[mastering]: https://en.wikipedia.org/wiki/Mastering_(audio)
