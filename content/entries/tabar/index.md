+++
title = "Tabar"
date = 2020-04-26

[taxonomies]
genres = [
    "math pop",
    "math rock",
    "midwest emo",
    "indie rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "rock",
    "pop",
]
first-letters = ["t"]
first-release-dates = ["2010"]
release-dates = ["2010"]
labels = []
countries = ["England"]
provinces = ["Derbyshire, England", "Staffordshire, England"]
areas = [
    "Swadlincote, Derbyshire, England",
    "Burton upon Trent, Staffordshire, England",
]
+++

{{ artist() }}

<!-- more -->

<b>Tabar</b> was an [English](/countries/england) {{ genre(name="math pop") }}
band from the [English](/countries/england) {{ genre(name="math pop") }} scene
of the late 2000s and early 2010s. <b>Tabar</b> was unfortunately short lived,
and we have little more than {{ time(t="10 minutes", dt="PT10M") }} of total
recorded material from them. Nevertheless, <b>Tabar</b> offered an excellent {{
genre(name="pop") }}pified take on {{ genre(name="math rock") }} that is very
well-written and features excellent {{ hs(name="vocal") }} performances from
<b>Emily Harris</b> (now of <b>Atlas</b>). All three <b>Tabar</b> songs are
tightly composed, compact 3&ndash;4 minute tunes that waste no time and feature
the kind of intricate {{ hs(name="guitar") }}work that we expect from {{
genre(name="math pop") }}. <b>Tabar</b> are proof that {{
genre(name="math rock") }} music can be clearly {{ genre(name="math rock") }}
without _specifically_ making use of [odd/complex time
signatures][complex-time-sigs]; despite only making use of 3/4 and 4/4, the
music of <b>Tabar</b> is still rhythmically complex, never letting up on the
heavy [syncopation](https://en.wikipedia.org/wiki/Syncopation) and counterposed
melodies.

Take a listen to their discography &mdash; it&rsquo;s only {{ time(t="10 minutes", dt="PT10M") }}!

### notes on the distributions

Tabar distributed their album using DistroKid, which includes auto-generated
YouTube uploads. The distribution listed here was extracted from said YouTube
videos with `youtube-dl -x`, in order to get the highest quality audio rip.

The song &ldquo;Gimma a Reason&rdquo; is sometimes listed as &ldquo;Gimme a
Reason&rdquo;.

{{ discog() }}

[complex-time-sigs]: https://en.wikipedia.org/wiki/Time_signature#Complex_time_signatures
