+++
title = "NGOD"
date = 2020-04-22

[taxonomies]
genres = [
    "math pop",
    "pop",
    "indie rock",
    "math rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "rock",
]
first-letters = ["n"]
first-release-dates = ["2010"]
release-dates = ["2010", "2011", "2014"]
labels = ["Enjoyment Records", "Sony Music Entertainment"]
countries = ["England"]
provinces = ["West Yorkshire, England"]
areas = ["Bradford, West Yorkshire, England"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>NGOD</b> was (and is? q.v. <b>Howl</b>), in some sense, two different bands.
Originally, <b>NGOD</b> was a {{ genre(name="math pop") }} band of the late
2000s/early 2010s [English](/countries/england) {{ genre(name="math pop") }}
scene, a scene that included <b>TTNG</b>, <b>tubelord</b>, <b>Foals</b>,
<b>Colour</b>, <b>Tangled Hair</b>, <b>Fish Tank</b>, <b>Love Among The
Mannequins</b>, <b>Wot Gorilla?</b>, and <b>Vasco Da Gama</b>, among others.
Think {{ genre(name="math rock") }}, but in the style of {{ genre(name="pop")
}}-{{ genre(name="rock") }}. All of the {{ genre(name="post-hardcore") }}/{{
genre(name="math rock") }} stylings are there, including punchy [block
chords](https://en.wikipedia.org/wiki/Block_chord), [complex time
signatures](https://en.wikipedia.org/wiki/Time_signature#Complex_time_signatures),
the occasional [screamed/yelled/gang][screaming] vocals, mixed use of
[distorted][distortion] and undistorted/clean {{ hs(name="guitar") }} tones,
and intricate {{ hs(name="guitar") }} lines. But all of this is wrapped up in
silky-smooth [clean singing](https://en.wikipedia.org/wiki/Singing), grooves
that are often oddly [danceable](https://en.wikipedia.org/wiki/Dance), and
intentionally catchy [refrains][chorus].

Somewhere around {{ time(t="2012") }}&ndash;{{ time(t="2013") }} or so, <b>NGOD</b> must have had a change of heart.
Presumably inspired by the mainstream success of fellow
[English](/countries/england) {{ genre(name="math pop") }}pers <b>Foals</b>,
and discouraged by the presumably poor earnings made from playing music in an
obscure genre, <b>NGOD</b> did an about-face away from their previously {{
genre(name="post-hardcore") }}/{{ genre(name="math rock") }} stylings towards a
straight-ahead and polished take on {{ genre(name="pop") }}-{{
genre(name="rock") }}. This landed them a record deal with
[Sony](/labels/sony-music-entertainment), and their new music even made its way
into [FIFA 17](https://en.wikipedia.org/wiki/FIFA_17). They then changed their
name, rebranding to &ldquo;<b>Howl</b>&rdquo;. This was _after_ having already
released music in the new style under the <b>NGOD</b> name, however.

Unfortunately, quickly after having a change of heart in {{ time(t="2012") }} or so, <b>NGOD</b> opted to expunge their previous
work from the internet, deleting their old bandcamp page and YouTube videos.
What remains of their old material (and some early demos of new material) is
provided here. It&rsquo;s hard to call <b>NGOD</b>&rsquo;s early material a
&ldquo;staple&rdquo; or &ldquo;classic&rdquo; of the {{ genre(name="math pop")
}} genre insofar as it is not very well-known; however, it comes highly
recommended (particularly the two officially released records, &ldquo;XL&rdquo;
and &ldquo;Bait Head&rdquo;) to anyone who is interested in {{
genre(name="math pop") }} or likes {{ genre(name="math pop") }} even a little
bit.

### notes on the distributions

&ldquo;\[YouTube rips (2011)\]&rdquo; contains a collection of audio from old
<b>NGOD</b> YouTube videos (in no particular order), presumably obtained in
{{ time(t="2011") }} through the services of a
&ldquo;YouTube-video-to-MP3&rdquo; service or similar.

&ldquo;[collected demos]&rdquo; collects, in chronological order, the
post-&ldquo;Bait Head&rdquo; demo tracks recorded by the band. The dates on
these tracks span from {{ time(t="2012") }} to {{ time(t="2014") }}, and some of the tracks will give some insight into
<b>NGOD</b>&rsquo;s later sound.

The second track off of &ldquo;XL&rdquo; (&ldquo;Talk With Hands&rdquo;) also
appears on the second Musical Mathematics compilation (&ldquo;Second Time
Lucky&rdquo;). This is reflected in the distributions listing.

{{ discog() }}

[screaming]: https://en.wikipedia.org/wiki/Screaming_(music)
[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
[chorus]: https://en.wikipedia.org/wiki/Chorus_(song)
