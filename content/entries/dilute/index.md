+++
title = "Dilute"
date = 2020-05-23

[taxonomies]
genres = [
    "experimental rock",
    "math rock",
    "post-rock",
    "post-hardcore",
    "midwest emo",
    "indie rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["2000"]
release-dates = ["2000", "2001", "2002", "2003", "2015"]
labels = [
    "54°40′ Or Fight!",
    "Sickroom Records",
    "It's Like False Advertising Records",
    "Toad Records",
    "Naked Ally Records",
    "Obsolete Media",
    "Tinyamp Records",
]
countries = ["United States"]
provinces = ["California, United States"]
areas = ["Alameda County, California, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Dilute</b> was an {{ genre(name="experimental rock") }} band from [Fremont,
California](/areas/alameda-county-california-united-states) at around the turn
of the millenium. <b>Dilute</b> are often associated with the
[Sacramento](/areas/sacramento-california-united-states)&ndash;[San
Francisco](/areas/san-francisco-california-united-states) {{
genre(name="math rock") }}/{{ genre(name="experimental rock") }} scene of the
2000s, particularly alongside bands such as <b>Hella</b> (a
[Sacramento](/areas/sacramento-california-united-states) band that performed
with <b>Dilute</b> on a number of occasions and did a split record with them)
and <b>Rumah Sakit</b> (a [San
Francisco](/areas/san-francisco-california-united-states) band closely
associated with <b>Dilute</b>, with <b>Dilute</b> even probably naming a song
after them, viz. &ldquo;Sickroom&rdquo;), as well as other bands of the scene
like <b>Deerhoof</b>, [<b>Ent</b>](/entries/ent), <b>Tera Melos</b>, and <b>The
Advantage</b>. Although they overall, as a band, remain fairly obscure ([their
Wikipedia page][wiki] has been created and deleted a number of times on account
of &ldquo;notability&rdquo; concerns), their {{ time(t="2001") }}
LP, &ldquo;Grape Blueprints Pour Spinach Olive Grape&rdquo; is considered a
classic of the {{ genre(name="math rock") }} canon, and the band remains
influential to this day. After the dissolution of <b>Dilute</b> some time in
{{ time(t="2003") }}, {{ hs(name="vocal") }}ist and {{
hs(name="guitar") }}ist <b>Marty Anderson</b> would go on to form <b>Okay</b>,
{{ hs(name="drummer") }} <b>Jay Pellicci</b> would join the [Portland,
Oregon](/areas/portland-oregon-united-states) {{ genre(name="math rock") }}
band <b>31Knots</b>, and both Pellicci brothers would take on audio engineering
variously in [San Francisco](/areas/san-francisco-california-united-states) and
[Portland](/areas/portland-oregon-united-states).

<b>Dilute</b>&rsquo;s music is, in some ways, part of the style of {{
genre(name="math rock") }} that emerged around their time (circa the turn of
the millenium) due to bands like <b>Don Caballero</b> and
[<b>Ent</b>](/entries/ent). <b>Dilute</b>, however, take on more deep influence
from both {{ genre(name="post-rock") }} and from {{ genre(name="indie rock") }}
(hence the use of the {{ genre(name="midwest emo") }} tag here). In many ways,
the focus of <b>Dilute</b>&rsquo;s music is on the long forms of their songs
and albums, which are often complex and ingeniously subtle, contrasting quiet
meditations against
[dissonant](https://en.wikipedia.org/wiki/Consonance_and_dissonance) &amp;
aggressive outbursts against
ultra[consonant](https://en.wikipedia.org/wiki/Consonance_and_dissonance)
[anthemic](https://en.wikipedia.org/wiki/Anthem) sections, contrasting
[composed](https://en.wikipedia.org/wiki/Musical_composition) parts with
[improvisations](https://en.wikipedia.org/wiki/Musical_improvisation),
contrasting lengthy [instrumental](https://en.wikipedia.org/wiki/Instrumental)
sections with sparse (and unusual) {{ hs(name="vocals") }} from <b>Marty
Anderson</b>, and so on. The music of <b>Dilute</b> is as much indebted to the
development of {{ genre(name="experimental rock") }} offshoots like {{
genre(name="math rock") }} and {{ genre(name="post-rock") }} as it is to a
tradition of subverting [popular music](/genres/pop) in strange ways, in the
vein of e.g. <b>Captain Beefheart &amp; His Magic Band</b> or <b>Cardiacs</b>;
<b>Dilute</b> may not sound like either of those bands, but their bizarre and
complex treatment of [popular music](/genres/pop) is nevertheless spiritually
kindred. <b>Dilute</b>&rsquo;s music can be difficult to get into, but rewards
thorough listens to each album as a whole.

### notes on the distributions

Seven of the tracks on &ldquo;Dilute&rdquo; also appear on &ldquo;The Gypsy
Valentine Curve&rdquo;.

The last two tracks listed for &ldquo;Grape Blueprints Pour Spinach Olive
Grape&rdquo; (&ldquo;Sickroom&rdquo; and &ldquo;Diet For A New America&rdquo;)
are bonus tracks from [Naked Ally Records](/labels/naked-ally-records)&rsquo;s
{{ time(t="2015", dt="2015-03-31") }} reissue, and are not present on the
original album. &ldquo;Diet For A New America&rdquo; is an excerpt from
&ldquo;Everyday&rdquo;. &ldquo;Sickroom&rdquo; was unreleased up to that point,
but a live version of it also appears on &ldquo;Dilute - Live&rdquo;, released
{{ time(t="later in 2015", dt="2015-09-10") }}. &ldquo;0&rdquo;,
&ldquo;Vs.&rdquo;, and &ldquo;1&rdquo; are sometimes combined into one long
track, &ldquo;0 Vs. 1&rdquo;.

The recording of &ldquo;Rock and or Roll&rdquo; that appears on &ldquo;Live
{{ time(t="2002-08-24", dt="2002-08-24") }} @ Edinburgh Castle, San
Francisco&rdquo; is the same recording that appears on &ldquo;Live
Recordings&rdquo;.

&ldquo;Live Recordings&rdquo; was released by [Toad
Records](/labels/toad-records) earlier in {{ time(t="2003") }},
before the same tracks were then reissued later that year as the bonus CD of
[Toad Records](/labels/toad-records)&rsquo;s re-release of &ldquo;The Gypsy
Valentine Curve&rdquo;.

The deadseapianorolls distribution for &ldquo;Hella + Dilute Live&rdquo; only
includes <b>Dilute</b>&rsquo;s contributions to the album; <b>Hella</b>&rsquo;s
side of the split can be found in the bandcamp distribution.

&ldquo;Dilute - Live&rdquo; was released in {{ time(t="2015", dt="2015-09-10") }}, but was recorded between {{ time(t="2002") }} and {{ time(t="2003") }}.

{{ discog() }}

[wiki]: https://en.wikipedia.org/wiki/Dilute_(band)
