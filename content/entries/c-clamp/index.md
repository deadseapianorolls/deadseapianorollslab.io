+++
title = "C-Clamp"
date = 2020-04-22

[taxonomies]
genres = [
    "slowcore",
    "math rock",
    "indie rock",
    "post-hardcore",
    "experimental rock",
    "post-rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["c"]
first-release-dates = ["1993"]
release-dates = ["1993", "1995", "1996", "1998", "1999"]
labels = [
    "Ohio Gold Records",
    "Seek Lamp",
    "Banter Records",
    "Flannel Camel",
    "The Rosewood Union",
    "ActionBoy 300 Records",
    "Divot",
]
countries = ["United States"]
provinces = ["Illinois, United States"]
areas = [
    "Champaign-Urbana, Illinois, United States",
    "Chicago, Illinois, United States",
]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>C-Clamp</b> was a {{ genre(name="slowcore") }}/{{ genre(name="math rock") }}
hybrid band from the
[Champaign-Urbana](/areas/champaign-urbana-illinois-united-states) region of
[Illinois](/provinces/illinois-united-states), although they later moved to
[Chicago](/areas/chicago-illinois-united-states). &ldquo;{{
genre(name="Slowcore") }}&rdquo; (sometimes, &ldquo;sadcore&rdquo;) is the name
given to a perceived subgenre of {{ genre(name="indie rock") }} that features
downtempo compositions with minimalistic {{ genre(name="rock") }}-like
arrangements and an overall feeling of sedation. The canonical record of the
genre is <b>Codeine</b>&rsquo;s {{ time(t="1990", dt="1990-08") }} LP,
&ldquo;Frigid Stars LP&rdquo;; notable are <b>Codeine</b>&rsquo;s connections
also to the {{ genre(name="math rock") }} genre, with one member
(<b>Doug Scharin</b>) also being a member of
[Louisville](/areas/louisville-kentucky-united-states) {{
genre(name="math rock") }}ers <b>June of 44</b>, and with legendary {{
genre(name="math rock") }} {{ hs(name="guitar") }}ist <b>David Grubbs</b>
serving as a kind of unofficial member of the band.

<b>C-Clamp</b> were associated with and/or contemporaries of several other
bands that blurred the lines between {{ genre(name="post-hardcore") }} stylings
(like {{ genre(name="math rock") }}) and {{ genre(name="indie rock") }}
stylings (like {{ genre(name="slowcore") }}), including fellow
[Champaign-Urbana](/areas/champaign-urbana-illinois-united-states) natives
<b>Braid</b> (who appear on both <abbr title="various artists">V/A</abbr>
compilations with <b>C-Clamp</b>) and the two closely-related
[Chicago](/areas/chicago-illinois-united-states) bands <b>Cap'n Jazz</b> and
<b>Joan of Arc</b> (the latter of which formed in the wake of the
former&rsquo;s breakup). Despite this, <b>C-Clamp</b> is rarely considered to
have been a {{ genre(name="midwest emo") }} band. Instead, I sometimes think of
<b>C-Clamp</b> as the more {{ genre(name="slowcore") }} predecessors of [Elgin,
Illinois](/areas/chicago-illinois-united-states) natives <b>Colossal</b>, who
formed 2 years after the breakup of <b>C-Clamp</b> (n.b. the two bands share no
members) &mdash; especially for the distinctive vocal style &mdash; although
<b>Colossal</b> are more clearly {{ genre(name="math rock") }} than
<b>C-Clamp</b> was.

### notes on the distributions

The distribution for &ldquo;Saving Daylight b/w Shorty&rdquo; unfortunately
only includes the A side (&ldquo;Saving Daylight&rdquo;). As usual, if you can
help improve the distribution, [you are welcome to](/contact).

The distribution for &ldquo;Ground Rule Double&rdquo; only includes
<b>C-Clamp</b>&rsquo;s contribution. Likewise, the distribution for
&ldquo;Cover The Earth: A Mud Records Compilation&rdquo; only includes
<b>C-Clamp</b>&rsquo;s contributions.

Different versions of both tracks on &ldquo;Passing b/w Fox &amp; The
Hound&rdquo; also appear on &ldquo;Meander + Return&rdquo;.

{{ discog() }}
