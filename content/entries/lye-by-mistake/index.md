+++
title = "Lye By Mistake"
date = 2020-07-15

[taxonomies]
genres = [
    "experimental metal",
    "progressive metal",
    "jazz metal",
    "mathcore",
    "jazz fusion",
    "metalcore",
    "experimental rock",
    "brutal prog",
    "whitebelt",
    "progressive rock",
    "grindcore",
    "post-hardcore",
    "metal",
    "hardcore punk",
    "jazz",
    "punk",
    "rock",
    "pop",
]
first-letters = ["l"]
first-release-dates = ["2004"]
release-dates = ["2004", "2006", "2008", "2009"]
labels = [
    "Black Market Activities",
    "Lambgoat Records",
    "Macaroni Records",
    "Sheepsxclothing",
]
countries = ["United States"]
provinces = ["Missouri, United States"]
areas = ["St. Louis, Missouri, United States"]

[extra]
last_edited = "2020-07-30"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="png") }}

<b>Lye By Mistake</b> was a {{ genre(name="jazz metal") }}/{{
genre(name="mathcore") }} outfit from [St. Louis,
Missouri](/areas/st-louis-missouri-united-states), formed some time in {{
time(t="2003") }}. They released their first album, a demo EP by the name of
&ldquo;The Fabulous&rdquo;, some time in {{ time(t="2004") }}. All of the tunes
from this demo would be re-recorded &mdash; along with three new tracks not on
the demo &mdash; for their debut full-length album, &ldquo;Arrangements For
Fulminating Vective&rdquo; ({{ time(t="2006", dt="2006-05-09") }}). The band
started recording demos for their next record around {{ time(t="2007") }}
&ndash; {{ time(t="2008") }} (these are listed here as &ldquo;Fea Jur
Demos&rdquo;), until the departure of {{ hs(name="vocal") }}ist <b>Tony
Saputo</b>. The band carried on, and released the new album as a totally
instrumental album &ldquo;Fea Jur&rdquo; in {{ time(t="2009", dt="2009-10-13")
}}. The members of the band went their separate ways some time in {{
time(t="2011") }}, although there was never any official announcement of their
dissolution. The members of <b>Lye By Mistake</b> went by a wide variety of
(typically goofy) nicknames, which are not necessarily reflected by the member
listing here.

While <b>Lye By Mistake</b>&rsquo;s music had plenty of constants throughout
their career, it is useful to divide it into two eras: that of
&ldquo;Arrangements For Fulminating Vective&rdquo;, and that of &ldquo;Fea
Jur&rdquo;. For many listeners, the most obvious difference will be the lack of
vocals in the latter album, versus the fairly prominent {{
genre(name="mathcore") }} vocals in the former. But the &ldquo;Fea Jur
Demos&rdquo; give us a decent idea of what &ldquo;Fea Jur&rdquo; would have
sounded like, had their {{ hs(name="vocal") }}ist not left the band &mdash; and
still, the differences between &ldquo;Arrangements&rdquo; and &ldquo;Fea
Jur&rdquo; are very clear. &ldquo;Arrangements&rdquo; displays clear influence
from [New Jersey](/provinces/new-jersey-united-states) {{
genre(name="mathcore") }} pioneers <b>The Dillinger Escape Plan</b>
(particularly &ldquo;Calculating Infinity&rdquo; ({{ time(t="1999",
dt="1999-09-28") }})), and perhaps also from [grindier](/genres/grindcore) {{
genre(name="mathcore") }} acts (read: {{ genre(name="whitebelt") }}) like early
<b>Daughters</b> &mdash; or even <b>The Locust</b>, considering the
experimentation with {{ hs(name="synthesizer") }}s (and later, {{
hs(name="theremin") }}). As a result, &ldquo;Arrangements&rdquo; is
characterised by [through-composition][through-composed], aimless
[chromatic][chromaticism] runs, rhythmic low-end stabs from the {{
hs(name="guitar") }}s, and perhaps most importantly, the incomplete
assimilation of {{ genre(name="jazz") }} influences with the overall {{
genre(name="mathcore") }} approach. Another good point of reference here is
<b>Between the Buried and Me</b> &mdash; although <b>Lye By Mistake</b> lacked
the {{ genre(name="death metal") }}/{{ genre(name="deathcore") }} origins of
<b>BtBaM</b>, both &ldquo;Arrangements&rdquo; and <b>BtBaM</b>&rsquo;s
pre-&ldquo;Parallax&rdquo; material share the tendency to place contrasting
[genres](/genres)/styles directly next to each other for the sake of contrast,
rather than for compositional reasons &mdash; usually with little in the way of
transitioning. As a result, if what you were looking for was {{
genre(name="jazz metal") }} proper, you will likely be disappointed with
&ldquo;Arrangements&rdquo; (except perhaps with the latter bit of
&ldquo;Nero&rsquo;s Intention&rdquo;&hellip;), even if it is technically {{
genre(name="metal") }} music with {{ genre(name="jazz") }} influences.

On the other hand, &ldquo;Fea Jur&rdquo; (and the &ldquo;Fea Jur Demos&rdquo;)
represents the much further maturation of this {{ genre(name="jazz metal") }}
approach into something that one could be proud to call &ldquo;{{
genre(name="jazz metal") }}&rdquo;. Indeed, &ldquo;Fea Jur&rdquo; is one of the
finest attempts at such a fusion that I know of, and comes highly recommended
to all fans of {{ genre(name="metal") }} and to all fans of {{
genre(name="jazz") }}. &ldquo;Fea Jur&rdquo; really is a more
[metallic](/genres/metal) form of {{ genre(name="jazz fusion") }}, retaining
its {{ genre(name="mathcore") }} origins through the use of often rhythmically
unsettling riffing and hyperactive {{ hs(name="drumming") }}, even if it has
lost one of the primary instruments of {{ genre(name="mathcore") }}: {{
hs(name="the human voice") }}. Humourously, the same thing occurs on
[RYM](https://rateyourmusic.com/artist/lye_by_mistake) with <b>Lye By
Mistake</b> as with [<b>Dysrhythmia</b>](/entries/dysrhythmia): because of the
instrumental nature of &ldquo;Fea Jur&rdquo;, it is marked as a {{
genre(name="math rock") }} album! And to think that all a {{
genre(name="mathcore") }} band has to do to be {{ genre(name="math rock") }} is
lose their vocalist&hellip; and that all {{ genre(name="math rock") }} with
[unclean vocals][unclean-vocals] is not really {{ genre(name="math rock") }}
after all! In all seriousness, &ldquo;Fea Jur&rdquo; has little, if any, {{
genre(name="math rock") }} influence; a safer bet for a diplomatic genre
labeling is {{ genre(name="progressive metal") }} and/or {{
genre(name="brutal prog") }}.

### notes on the distributions

&ldquo;Frozen Mandibles&rdquo; from the &ldquo;Fea Jur Demos&rdquo; was renamed
to &ldquo;Stag&rdquo; before being released as part of &ldquo;Fea Jur&rdquo;.

{{ discog() }}

[through-composed]: https://en.wikipedia.org/wiki/Through-composed_music
[chromaticism]: https://en.wikipedia.org/wiki/Chromaticism
[unclean-vocals]: https://en.wikipedia.org/wiki/Screaming_(music)
