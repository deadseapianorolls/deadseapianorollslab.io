+++
title = "Gaston"
date = 2020-04-06

[taxonomies]
genres = [
    "math rock",
    "post-rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "rock",
    "pop",
]
first-letters = ["g"]
first-release-dates = ["2001"]
release-dates = ["2001", "2004"]
labels = ["Becalmed Records", "Beau Rivage"]
countries = ["Germany"]
provinces = ["Berlin, Germany"]
areas = ["Berlin, Berlin, Germany"]

[extra]
last_edited = "2020-04-23"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Gaston</b> was an early 21<sup>st</sup>-century [German](/countries/germany)
{{ genre(name="math rock") }} band that, although largely unknown to this day,
were a quite early entry into the realm of {{ genre(name="math rock") }}/{{
genre(name="post-rock") }} hybrids. Their sound is superficially {{
genre(name="jazz fusion") }}-esque, and primarily based around {{
hs(name="bass guitar") }} and various pitched {{
hs(name="percussion idiophones") }}, thus foreshadowing the stylings of later
groups in the same genre like <b>Tyler Shoemaker</b>, <b>Renaissance Sound</b>,
and <b>Monobody</b>.

Although <b>Gaston</b> remains somewhat obscure, their music is nothing short
of superb &mdash; their compositions are as good as, or better than, any others
in the genre.

### notes on the distributions

&ldquo;#1&rdquo; was ripped from YouTube videos uploaded by DistroKid on behalf
of [Becalmed Records](/labels/becalmed-records), using `youtube-dl -x` to rip
the highest-quality audio possible.

&ldquo;What Time Does Your Train Leave Today?&rdquo; was offered via a
`https://mega.nz/*` link in the description of a YouTube video, courtesy of The
Math Rock Collective. The source of these MP3s is ultimately unknown, but they
were probably directly encoded to MP3 from a CD rip. This particular album was
released simultaneously on CD and vinyl by [Beau Rivage](/labels/beau-rivage).

Also, the album art for &ldquo;#1&rdquo; found on Discogs was very low-quality,
so the &ldquo;#1&rdquo; album art given here was obtained as follows:

```bash
youtube-dl "https://www.youtube.com/watch?v=ppV3Yww9pjc"
ffmpeg -i "Cargo-ppV3Yww9pjc.mp4" -vf "select=eq(n\,0)" -vframes 1 1.png
```

{{ discog() }}
