+++
title = "Hot Cross"
date = 2020-07-22

[taxonomies]
genres = [
    "post-hardcore",
    "screamo",
    "math rock",
    "punk",
    "experimental rock",
    "rock",
    "pop",
]
first-letters = ["h"]
first-release-dates = ["2001"]
release-dates = ["2001", "2002", "2003", "2004", "2005", "2007"]
labels = [
    "Level Plane Records",
    "Paranoid Records",
    "EarthWaterSky Connection",
    "Equal Vision Records",
    "Hope Division",
    "Robodog Records",
    "Electric Human Project",
    "Watson and the Shark",
    "Moustache Ride",
    "Sonzai Records",
    "Nova Recordings",
]
countries = ["United States"]
provinces = ["Pennsylvania, United States"]
areas = ["Philadelphia, Pennsylvania, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="png") }}

<b>Hot Cross</b> was a {{ genre(name="post-hardcore") }}/{{
genre(name="screamo") }} band from
[Philadelphia](/areas/philadelphia-pennsylvania-united-states) that formed in
{{ time(t="2000") }} out of the ashes of <b>Saetia</b>. <b>Saetia</b> {{
hs(name="guitar") }}ist <b>Adam Marino</b> was originally part of <b>Hot
Cross</b>, but he is not represented in the member listing here because he left
<b>Hot Cross</b> to focus on his other band <b>Errortype: 11</b> before <b>Hot
Cross</b> had any material. {{ hs(name="Drummer") }} <b>Greg Drudy</b> was a
veteran of both <b>Saetia</b> and [New
York](/areas/new-york-city-new-york-united-states) {{ genre(name="post-punk")
}}/{{ genre(name="indie rock") }} outfit <b>Interpol</b>, and also the one
responsible for [Level Plane Records](/labels/level-plane-records). <b>Casey
Boland</b> was a veteran of [New
Brunswick](/areas/middlesex-county-new-jersey-united-states) {{
genre(name="screamo") }} outfit <b>You &amp; I</b>. <b>Matt Smith</b> played {{
hs(name="bass guitar") }} in <b>Saetia</b>&rsquo;s final performance, and was
also [<b>Off Minor</b>](/entries/off-minor)&rsquo;s original {{
hs(name="bass guitar") }}ist. <b>Matt Smith</b> was originally a {{
hs(name="guitar") }}ist for the band, and so <b>Josh Jakubowski</b> slightly
later joined the band as their {{ hs(name="bass guitar") }}ist;
<b>Jakubowski</b> had previously done {{ hs(name="guitar") }}work and {{
hs(name="vocals") }} in other {{ genre(name="screamo") }} outfits like
<b>Joshua Fit For Battle</b>, <b>Neil Perry</b>, and <b>The Now</b>. <b>Josh
Jakubowski</b> and <b>Matt Smith</b> would trade off {{ hs(name="bass guitar")
}} and {{ hs(name="guitar") }} duties within <b>Hot Cross</b> until
<b>Jakubowski</b>&rsquo;s departure from the band to focus on musical
production in his own studio (Cannon Road Studio) in [New
Jersey](/provinces/new-jersey-united-states); <b>Jakubowski</b> produced, but
did not perform on, <b>Hot Cross</b>&rsquo;s final album, &ldquo;Risk
Revival&rdquo; ({{ time(t="2007", dt="2007-02-20") }}). Lead {{
hs(name="vocal") }}ist <b>Billy Werner</b>, also the lead {{ hs(name="vocal")
}}ist of <b>Saetia</b>, was studying abroad at the time of <b>Hot
Cross</b>&rsquo;s initial formation, but received a demo recording from the
band and was able to play his first show with <b>Hot Cross</b> less than a week
after returning to the [United States](/countries/united-states).

In comparison to sibling band [<b>Off Minor</b>](/entries/off-minor), <b>Hot
Cross</b> played a style of {{ genre(name="screamo") }} that was more
straightforwardly focussed on {{ hs(name="guitar") }} and {{ hs(name="vocals")
}}, but still technical and angular enough to warrant some association with the
{{ genre(name="math rock") }} genre. By the time of &ldquo;Cryonics&rdquo; ({{
time(t="2003", dt="2003-05-20") }}) and onwards, <b>Hot Cross</b>&rsquo;s style
had veered further away from that of <b>Saetia</b> into a realm usually
addressed as simply &ldquo;{{ genre(name="post-hardcore") }}&rdquo; (rather
than specifically {{ genre(name="screamo") }}, although &ldquo;Cryonics&rdquo;
and their other albums are still usually considered {{ genre(name="screamo") }}
as well). Particularly, comparisons with other technically-inclined {{
genre(name="post-hardcore") }} bands like <b>At The Drive-In</b> and
particularly <b>The Fall of Troy</b> (whose first LP was released not too long
after <b>Hot Cross</b>&rsquo;s) are appropriate here, although <b>Hot
Cross</b>&rsquo;s {{ genre(name="screamo") }} lineage shines through
consistently. Their final album, &ldquo;Risk Revival&rdquo; ({{ time(t="2007",
dt="2007-02-20") }}), proved to be their most controversial release due to its
slicker production (courtesy of <b>Jakubowski</b>) and {{ hs(name="vocal") }}
stylings most removed from {{ genre(name="screamo") }} out of all of their
records.

### notes on the distributions

<b>Hot Cross</b>&rsquo;s side of their split with <b>Light the Fuse and Run</b>
appears as bonus tracks on their bandcamp distribution of &ldquo;A New Set Of
Lungs&rdquo;. The deadseapianorolls distribution for this album only contains
<b>Light the Fuse and Run</b>&rsquo;s contributions. Also, a re-recorded
version of &ldquo;In Memory Of Morvern&rdquo; appears on
&ldquo;Cryonics&rdquo;.

<b>Lickgoldensky</b>&rsquo;s track &ldquo;Little Dots&rdquo;, on their split
with <b>Hot Cross</b>, is sometimes listed as &ldquo;Little Dotz&rdquo;. A
different recording of &ldquo;Patience and Prudence&rdquo; from this split
appears on &ldquo;Cryonics&rdquo;.

<b>Hot Cross</b>&rsquo;s side of their split with <b>The Holy Shroud</b>
appears as a bonus track on their bandcamp distribution of
&ldquo;Cryonics&rdquo;. The deadseapianorolls distribution for this album only
contains <b>The Holy Shroud</b>&rsquo;s contribution.

{{ discog() }}
