+++
title = "the tumbled sea"
date = 2020-04-18

[taxonomies]
genres = [
    "ambient",
    "western chamber",
    "minimalism",
    "western classical",
]
first-letters = ["t"]
first-release-dates = ["2008"]
release-dates = ["2008", "2009"]
labels = ["futurerecordings"]
countries = ["United States"]
provinces = ["Massachusetts, United States"]
areas = ["Boston, Massachusetts, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>the tumbled sea</b> was a solo project of one <b>Sam Lee</b> of [Boston,
Massachusetts](/areas/boston-massachusetts-united-states). The music of <b>the
tumbled sea</b> sits somewhere between {{ genre(name="ambient") }} and {{
genre(name="western chamber") }} music, with sparse textures typically
involving {{ hs(name="piano") }} and {{ hs(name="synthesizer") }}, as well as
other elements like nonmusical {{ hs(name="samples") }}, and {{
hs(name="mellotron") }}-style accompaniment by {{ hs(name="violin") }}-family
instruments and {{ hs(name="woodwinds") }}. Although the music of <b>the
tumbled sea</b> insists on simplicity and even {{ genre(name="minimalism") }},
it conveys a hauntingly beautiful quality that feels more genuine and
amateurish than it does cynical or calculated.

&ldquo;songs by the tumbled sea&rdquo; was originally self-released, but later
was re-issued in {{ time(t="2009") }} alongside the original
release of &ldquo;melody summer&rdquo; by
[futurerecordings](/labels/futurerecordings).
[futurerecordings](/labels/futurerecordings) was a label started by <b>Adam
Nanaa</b> of the highly influential early-1990s {{ genre(name="post-hardcore")
}} band <b>Indian Summer</b>. Sometime in {{ time(t="2019") }},
[futurerecordings](/labels/futurerecordings)&rsquo;s bandcamp and Facebook
pages (among other things) were deleted.

### notes on the distributions

[futurerecordings](/labels/futurerecordings) used to have a bandcamp page
([archived][bandcamp-archived]), although it can no longer be found. Back when
it existed, I downloaded <b>the tumbled sea</b>&rsquo;s music &mdash;
unfortunately, the same problem occured here that occured with [<b>The Brave
Little Abacus</b>](/entries/the-brave-little-abacus). At the time, I downloaded
in CBR 320kb/s MP3, so that&rsquo;s all I have here.

&ldquo;songs by the tumbled sea&rdquo; had two bonus tracks (&ldquo;untitled
for piano and violins&rdquo; and &ldquo;untitled for piano&rdquo;) added to the
end of the album for the [futurerecordings](/labels/futurerecordings) re-issue.

&ldquo;melody summer&rdquo; is sometimes spelled with a forward slash, like
e.g. &ldquo;Melody/Summer&rdquo;. Here, we use the spelling given by
[futurerecordings](/labels/futurerecordings)&rsquo;s bandcamp listing.

{{ discog() }}

[bandcamp-archived]: https://web.archive.org/web/20190110112428/https://futurerecordings.bandcamp.com/
