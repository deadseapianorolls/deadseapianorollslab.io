+++
title = "Cheval de Frise"
date = 2020-04-20

[taxonomies]
genres = [
    "math rock",
    "brutal prog",
    "avant-prog",
    "experimental rock",
    "post-hardcore",
    "American primitive",
    "progressive rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["c"]
first-release-dates = ["2000"]
release-dates = ["2000", "2001", "2003", "2005"]
labels = [
    "Sonore",
    "RuminanCe",
    "Frenetic Records",
    "Sickroom Records",
    "Minority Records",
]
countries = ["France"]
provinces = ["Gironde, France"]
areas = ["Bordeaux, Gironde, France"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Cheval de Frise</b> (literally &ldquo;Frisian horse&rdquo;, but more
commonly [an anti-cavalry array of
spears/spikes](https://en.wikipedia.org/wiki/Cheval_de_frise)) was a
[French](/countries/france) {{ genre(name="math rock") }}/{{
genre(name="brutal prog") }} outfit of the early {{ time(t="2000") }}s.
Utilizing the somewhat unusual instrumentation of an
[amplified](https://en.wikipedia.org/wiki/Acoustic-electric_guitar) (sometimes
using [overdrive/distortion][distortion]) [classical {{ hs(name="guitar")
}}](https://en.wikipedia.org/wiki/Classical_guitar) combined with an acoustic
{{ hs(name="drumkit") }}, <b>Cheval de Frise</b> made music recalling the
{{ genre(name="American primitive") }}-inspired {{ hs(name="guitar") }}work of
<b>Gastr del Sol</b>, although <b>Cheval de Frise</b>&rsquo;s music is utterly
unique. Their sense of harmony &amp; melody is fairly abstract, thus the {{
genre(name="avant-prog") }} label; this sense has lead some to erroneously
label <b>Cheval de Frise</b> with terms like &ldquo;jazz rock&rdquo; and
&ldquo;{{ genre(name="jazz punk") }}&rdquo;, although in actuality there is no
{{ genre(name="jazz") }} influence. <b>Cheval de Frise</b> does not feature
much (if any) improvisation, and the song structures, melodies, and harmonies
are all unrelated to those of {{ genre(name="jazz") }} music.

Admittedly, <b>Cheval de Frise</b> is one of my favorite bands; if you like
them, I recommend checking out {{ hs(name="guitar") }}ist <b>Thomas
Bonvalet</b>&rsquo;s solo project, [<b>l'ocelle
mare</b>](https://ocellemare.bandcamp.com/) (literally
[Catalan](https://en.wikipedia.org/wiki/Catalan_language) for &ldquo;the mother
bird&rdquo;). {{ hs(name="Drummer") }} <b>Vincent Beysselance</b> went on to
form a band called [<b>Tormenta</b>](https://tormenta1.bandcamp.com/) (q.v.
their {{ time(t="2011", dt="2011-07-02") }} LP, &ldquo;La ligne âpre&rdquo;).

### notes on the distributions

Their debut self-titled LP is sometimes erroneously listed with a release year
of {{ time(t="2004") }}. This is due to the [Sickroom
Records](/labels/sickroom-records) re-issue; the original release was in {{
time(t="2000") }} on [Sonore](/labels/sonore).

<b>Cheval de Frise</b>&rsquo;s side of the split with <b>Rroselicoeur</b> also
appears on &ldquo;Fresques Sur Les Parois Secrètes Du Crâne&rdquo;.
<b>Rroselicoeur</b>&rsquo;s side also appears on their {{ time(t="2003") }} LP,
&ldquo;Demios Oneiron&rdquo;.

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
