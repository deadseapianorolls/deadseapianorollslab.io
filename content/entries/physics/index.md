+++
title = "Physics"
date = 2020-05-29

[taxonomies]
genres = [
    "post-rock",
    "experimental rock",
    "electronic",
    "ambient",
    "drone",
    "math rock",
    "rock",
    "post-hardcore",
    "punk",
    "pop",
]
first-letters = ["p"]
first-release-dates = ["1994"]
release-dates = [
    "1994",
    "1996",
    "1997",
    "1998",
    "1999",
    "2000",
    "2002",
    "2018",
]
labels = [
    "Gravity",
    "Gold Standard Laboratories",
    "Dagon Productions Audio Autism",
    "Flapping Jet Records",
    "Joyful Noise Recordings",
    "Neurot Recordings",
    "Slowdance Records",
    "Titanium Exposé",
    "Ace Fu Records",
    "Vinyl Communications",
]
countries = ["United States"]
provinces = ["California, United States"]
areas = ["San Diego County, California, United States"]
+++

{{ artist() }}

<!-- more -->

<b>Physics</b> was an early {{ genre(name="post-rock") }}/{{
genre(name="experimental rock") }} band from [San Diego,
California](/areas/san-diego-county-california-united-states) active during the
1990s. Originally formed in {{ time(t="1993") }} by <b>John
Goff</b> and <b>Denver Lucas</b> (both previously of <b>Johnny Superbad &amp;
the Bullet Catchers</b>, and the latter also of
[<b>powerdresser</b>](https://powerdresser.bandcamp.com/)), <b>Physics</b>
featured a rotating cast of musicians, so the member listing represented here
is not exhaustive nor representative of the band at any particular point in
time. <b>Physics</b> were, in their time and in many extant sources (including
what is effectively the band&rsquo;s own description of themselves, from
[<b>Aspects Of Physics</b>&rsquo;s bandcamp
page](https://aspectsofphysics.bandcamp.com/): &ldquo;the enigmatic math rock
band Physics&rdquo;), considered &ldquo;{{ genre(name="math rock") }}&rdquo;.
They are not primarily tagged as {{ genre(name="math rock") }} here because the
band&rsquo;s music is not _usually_ reflective of the label &mdash; there are a
number of possible reasons for this seemingly strange categorization, including
the widespread loose association between {{ genre(name="post-rock") }} and {{
genre(name="math rock") }}; <b>Physics</b> sharing members with {{
genre(name="math rock") }} groups like
[<b>powerdresser</b>](https://powerdresser.bandcamp.com/), <b>Heavy
Vegetable</b>, and <b>Drive Like Jehu</b> (<b>Rick Froberg</b> reportedly
played as part of <b>Physics</b>, although he is not credited on any of their
known recordings, so he is not represented in the member listing here); and
possibly <b>Physics</b>&rsquo;s association with southern
[California](/provinces/california-united-states) {{ genre(name="math rock")
}}ers like <b>Upsilon Acrux</b> (with whom they appear on a <abbr
title="various artists">V/A</abbr> record). The band did sometimes play {{
genre(name="math rock") }}, though: see e.g. &ldquo;Seven-11ths&rdquo;. In any
case, most of the recordings that we now have of the band are live recordings,
and the one &ldquo;proper&rdquo; album in the usual sense (full-length studio
record) that they released was &ldquo;Physics²&rdquo;, released on the
legendary [San Diego](/areas/san-diego-county-california-united-states) label
[Gravity Records](/labels/gravity) in {{ time(t="1998") }}. After
the dissolution of <b>Physics</b> some time in {{ time(t="2000") }}, <b>Jeff Coad</b>, <b>Jason Soares</b>, and
<b>Thatcher Orbitashi</b> went on to form <b>Aspects Of Physics</b>, a more {{
genre(name="electronic") }}ally-focused continuation of <b>Physics</b>.

Perhaps the most obvious reference point for the music of <b>Physics</b> is
<b>Tortoise</b>; both started releasing music at around the same time, both are
considered {{ genre(name="post-rock") }}, both shared members with {{
genre(name="post-hardcore") }}/{{ genre(name="math rock") }} bands
(<b>Bastro</b>, <b>Slint</b>, and <b>Gastr del Sol</b> in
<b>Tortoise</b>&rsquo;s case), and both take influence from a wide variety of
musical styles, including {{ genre(name="krautrock") }}, {{
genre(name="minimalism") }}, {{ genre(name="drone") }}, {{
genre(name="electronic") }}, {{ genre(name="noise") }}, &amp;c. In comparison
to <b>Tortoise</b>, <b>Physics</b> were less likely to make use of
straightforward melodies in their music, and also more likely to feature
crescendi into loud, [distorted][distortion] sections (a feature that would
later come to be associated with the {{ genre(name="post-rock") }} label).
Ultimately, the music of <b>Physics</b> is intentionally experimental and
challenged the limits of [popular music](/genres/pop) to a degree that makes
them difficult to categorize other than with the retrospective and perhaps
overly-broad &ldquo;{{ genre(name="post-rock") }}&rdquo;. <b>Physics</b> comes
highly recommended for fans of <b>Tortoise</b>, {{ genre(name="post-rock") }}
in general, and of <b>Rob Crow</b>&rsquo;s various projects.

### notes on the distributions

&ldquo;&lsquo;Black&rsquo; 7″&rdquo; is usually listed with both tracks being
untitled.

&ldquo;America Salutes Merzbow&rdquo; consists entirely of covers of
<b>Merzbow</b> tunes. The distribution here only includes
<b>Physics</b>&rsquo;s contribution to the record.

&ldquo;Physics¹&rdquo; is a compilation album of the band&rsquo;s work up to
that point (including all the material from the {{ time(t="1994") }} 7″).

The tracks on &ldquo;2.7.98&rdquo; have no titles.

The distribution for &ldquo;Keep Left, Vol. 1: A Benefit for David Barsamian
&amp; Alternative Radio&rdquo; only contains <b>Physics</b>&rsquo;s
contribution to the record.

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
