+++
title = "Drowningman"
date = 2020-08-10

[taxonomies]
genres = [
    "mathcore",
    "metalcore",
    "post-hardcore",
    "experimental rock",
    "experimental metal",
    "punk",
    "metal",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["1996"]
release-dates = [
    "1996",
    "1997",
    "1998",
    "1999",
    "2000",
    "2001",
    "2004",
    "2005",
]
labels = [
    "Hydra Head Records",
    "Revelation Records",
    "Equal Vision Records",
    "Thorp Records",
    "Law of Inertia",
]
countries = ["United States"]
provinces = ["Vermont, United States"]
areas = ["Chittenden County, Vermont, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Drowningman</b> was a {{ genre(name="mathcore") }}/{{
genre(name="metallic hardcore") }} band from [Burlington,
Vermont](/areas/chittenden-county-vermont-united-states) that formed in {{
time(t="1995") }}. The only constant member of the band throughout all of its
iterations has been {{ hs(name="vocal") }}ist <b>Simon Brody</b>, who had
previously been the {{ hs(name="vocal") }}ist of [Burlington,
Vermont](/areas/chittenden-county-vermont-united-states) {{
genre(name="hardcore punk") }} outfit <b>The Champions</b>. After releasing
their demo in {{ time(t="1996") }}, the band signed to [Hydra Head
Records](/labels/hydra-head-records) and released their first single
(&ldquo;Weighted In and Weighted Down <abbr title="backed with">b/w</abbr>
Static Mouth&rdquo; ({{ time(t="1997") }})) and first full length record
(&ldquo;Busy Signal at the Suicide Hotline&rdquo; ({{ time(t="1998",
dt="1998-05-20") }})) on that label. Also on [Hydra
Head](/labels/hydra-head-records) was their split with [New
Jersey](/provinces/new-jersey-united-states) {{ genre(name="mathcore") }}
outfit <b>The Dillinger Escape Plan</b> ({{ time(t="1999") }}). The band then
moved to [Revelation Records](/labels/revelation-records) and released an EP
(&ldquo;How They Light Cigarettes In Prison&rdquo; ({{ time(t="2000",
dt="2000-03-07") }})), their second LP (&ldquo;Rock and Roll Killing
Machine&rdquo; ({{ time(t="2000", dt="2000-09-12") }})), and another EP
(&ldquo;Still Loves You&rdquo; ({{ time(t="2001", dt="2001-07-24") }})) on
[Revelation](/labels/revelation-records)/[Equal
Vision](/labels/equal-vision-records). The band broke up in {{ time(t="2002")
}}, but reformed in {{ time(t="2004") }} and released a compilation of mostly
early material (&ldquo;Learn To Let It Go (The Demos)&rdquo; ({{ time(t="2004",
dt="2004-08-24") }})) on [Law of Inertia](/labels/law-of-inertia) (later known
as [Reignition Recordings](/label/reignition-recordings)) and their final
record, the LP &ldquo;Don't Push Us When We're Hot&rdquo; ({{ time(t="2005",
dt="2005-08-23") }}) on [Pennsylvania](/provinces/pennsylvania-united-states)
{{ genre(name="hardcore punk") }} label [Thorp Records](/label/thorp-records).
<b>Drowningman</b> then broke up for the second (and final, other than for a
string of three reunion shows in {{ time(t="2014") }}) time in {{
time(t="2005") }}.

The similarity of <b>Drowningman</b>&rsquo;s name to the name of seminal [New
Jersey](/provinces/new-jersey-united-states) {{ genre(name="metallic hardcore")
}}/{{ genre(name="mathcore") }} outfit [<b>Deadguy</b>](/entries/deadguy) is no
coincidence: <b>Drowningman</b> continued the angrily sarcastic early {{
genre(name="mathcore") }} style of [<b>Deadguy</b>](/entries/deadguy). But
<b>Drowningman</b> had other ideas: their music was a more modern-sounding
take, reaching even more [mathematical](/genre/mathcore) heights and
incorporating {{ genre(name="indie rock") }}/{{ genre(name="pop-punk") }}-ified
{{ genre(name="post-hardcore") }} sections inspired by bands like <b>Sunny Day
Real Estate</b> and <b>The Promise Ring</b>. This made <b>Drowningman</b> one
of the early pioneers of the kind of [genre](/genres) bending that would later
come to characterize many {{ genre(name="mathcore") }} acts &mdash; and
<b>Drowningman</b> did it even more seamlessly.

### notes on the distributions

Both the {{ time(t="1996") }} demo as well as &ldquo;Weighted In and Weighed
Down <abbr title="backed with">b/w</abbr> Static Mouth&rdquo; are included as
part of the {{ time(t="2004", dt="2004-08-24") }} compilation record,
&ldquo;Learn To Let It Go (The Demos)&rdquo;. &ldquo;Weighted In and Weighed
Down&rdquo; also appears on &ldquo;Still Loves You&rdquo;, but is instead
titled &ldquo;Weighted And Weighed Down&rdquo;.

The distributions for &ldquo;Busy Signal at the Suicide Hotline&rdquo; are for
the {{ time(t="2013") }} remaster.

&ldquo;My First Restraining Order&rdquo; from <b>Drowningman</b>&rsquo;s side
of their split with [New Jersey](/provinces/new-jersey-united-states) {{
genre(name="mathcore") }} outfit <b>The Dillinger Escape Plan</b> also appears
(re-recorded?) on &ldquo;Rock and Roll Killing Machine&rdquo; ({{
time(t="2000", dt="2000-09-12") }}).

&ldquo;Still Loves You&rdquo; has a subtitle of &ldquo;Even If No One Else
Will&rdquo;.

&ldquo;Learn To Let It Go (The Demos)&rdquo; was released in {{ time(t="2004",
dt="2004-08-24") }}, but mostly recorded from {{ time(t="1996") }} &ndash; {{
time(t="1998") }}, with the exception of &ldquo;Kiss the Canvas&rdquo; and
&ldquo;Where the Heart is&rdquo;, which were recorded in {{ time(t="2002") }}.

{{ discog() }}
