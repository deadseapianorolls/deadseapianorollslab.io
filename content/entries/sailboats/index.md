+++
title = "Sailboats"
date = 2020-07-11

[taxonomies]
genres = [
    "screamo",
    "emoviolence",
    "post-hardcore",
    "powerviolence",
    "hardcore punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["s"]
first-release-dates = ["2003"]
release-dates = ["2003"]
labels = []
countries = ["United States"]
provinces = ["California, United States"]
areas = ["Alameda County, California, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="png") }}

<b>Sailboats</b> was a {{ genre(name="screamo") }} band from [Berkeley,
California](/areas/alameda-county-california-united-states) that probably
formed some time around {{ time(t="2003") }} and probably
dissolved some time around {{ time(t="2004") }}. Information on
the band is scarce; all that we know is that they were a power trio, at least
one member of the band went on to form [<b>Loma
Prieta</b>](/entries/loma-prieta), and they released a two-song demo some time
in {{ time(t="2003") }}. The band is occasionally referred to as
&ldquo;Behind Trees&rdquo;, which is also the title of the first song on their
demo; [their MySpace page](https://myspace.com/behindtrees)&rsquo;s URL has a
slug of `behindtrees`, and reportedly, the members of [<b>Loma
Prieta</b>](/entries/loma-prieta) refer to the band as &ldquo;Behind
Trees&rdquo; and will occasionally play the song of the same name live. There
was a rumor floating around the internet when the band dissolved that their
dissolution was due to two out of the three members dying in a car crash, but
most sources seem to discount this as a hoax started by the band themselves.
Some sources also claim that the band recorded an LP before breaking up, but
that the LP was never released. Their two-song demo is also sometimes extended
to a four-song demo (with two extra tracks at the end, &ldquo;53 Second
Song&rdquo; and &ldquo;Four&rdquo;), but these two tracks are disputed and
probably have been confused with the actual demo (possibly being songs by a
different band); still, they are included for completeness.

It&rsquo;s unfortunate that the copies of the demo now floating around are in a
very low-quality encoding (128kb/s MP3), especially since the quality of the
recording itself seems to be quite good, and the songs and performances are
very strong. In the words of [a RateYourMusic
user](https://rateyourmusic.com/release/single/sailboats/demo/),
<b>Sailboats</b> are &ldquo;the greatest {{ genre(name="screamo") }} band that
never was&rdquo;. The two-song demo comes highly recommended to any {{
genre(name="screamo") }} fan.

### notes on the distributions

The tracks &ldquo;Two&rdquo; and &ldquo;Four&rdquo; are sometimes listed as
&ldquo;2&rdquo; and &rdquo;4&rdquo;, respectively. It may or may not be the
case that &ldquo;53 Second Song&rdquo; and &ldquo;Four&rdquo; are by
<b>Sailboats</b>.

{{ discog() }}
