+++
title = "Daïtro"
date = 2020-05-19

[taxonomies]
genres = [
    "screamo",
    "post-hardcore",
    "post-rock",
    "experimental rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["d"]
first-release-dates = ["2002"]
release-dates = ["2002", "2003", "2004", "2005", "2007", "2009"]
labels = [
    "Echo Canyon",
    "adagio830",
    "PurePainSugar",
    "Clean Plate Records",
    "Alchimia",
    "Ape Must Not Kill Ape",
    "Flower Of Carnage",
    "oto Records",
    "Last Day Of June",
    "Puzzle Records",
    "Utarid Tapes",
    "Music Fear Satan",
    "Red Cars Go Faster Records",
    "Code Of Ethics",
    "Impregnate Noise Laboratories",
    "React With Protest",
    "Satire Records",
]
countries = ["France"]
provinces = ["Rhône, France"]
areas = ["Lyon, Rhône, France"]

[extra]
last_edited = "2020-08-20"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Daïtro</b> was a [French](/countries/france) {{ genre(name="screamo") }}
band that played in what some might call &ldquo;the [French](/countries/france)
\[or, European\] style&rdquo; (see also: [<b>Le Pré Où Je Suis
Mort</b>](/entries/le-pre-ou-je-suis-mort)). They were highly influential as a
band, influencing countless {{ genre(name="post-hardcore") }} bands from
Europe, the Americas, [Japan](/countries/japan), and elsewhere. {{
hs(name="Drummer") }} <b>Benoît Desvignes</b> was also a member of {{
genre(name="screamo") }}/{{ genre(name="post-rock") }}/{{
genre(name="math rock") }} outfit [<b>Mihai
Edrisch</b>](/entries/mihai-edrisch). The members of <b>Daïtro</b> went on to
pursue other musical projects, like <b>Bâton Rouge</b>, <b>12XU</b>, the solo
works of <b>Samuel Moncharmont</b>, &amp;c. Founding <b>Daïtro</b> member
<b>Julien Paget</b> created the [Echo Canyon record
label](/labels/echo-canyon).

<b>Daïtro</b>&rsquo;s music is characterised by somewhat elaborate use of
[block chords](https://en.wikipedia.org/wiki/Block_chord);
<b>Daïtro</b>&rsquo;s sound is easily recognized by the kinds of voicings they
used. The melodies of <b>Daïtro</b> are often subtle yet poignant, weaving
through the gradual [counterpoint](https://en.wikipedia.org/wiki/Counterpoint)
of the walls of chords that they use. Even the detatched melodies and riffs are
themselves often derived from [broken
chords](https://en.wikipedia.org/wiki/Arpeggio). Their {{ hs(name="vocal") }}
stylings are almost exclusively screamed and/or spoken. In spite of this,
<b>Daïtro</b>&rsquo;s music comes off as strangely &ldquo;melodic&rdquo;, in
the sense of {{ genre(name="pop-punk") }} or perhaps even {{
genre(name="melodic hardcore") }}. Combining this with a clear {{
genre(name="post-rock") }} influence (think <b>Envy</b>), <b>Daïtro</b> had a
very unique sound that helped to cement them in the pantheon of {{
genre(name="screamo") }} greats.

### notes on the distributions

The first track on &ldquo;Des Cendres, Je Me Consumme&rdquo; (here listed as
&ldquo;\[intro\]&rdquo;) is actually unlisted on the original release. As a
result, this track is not only given various names by various sources, but it
is also sometimes listed as &ldquo;Mon Corps&rdquo;, which is actually the
second track on the album. This results in such listings getting every single
track name wrong, and leaving the last one (rather than the first) untitled.

<b>Daïtro</b>&rsquo;s contribution to &ldquo;This Is Your Life&rdquo; is
sometimes listed as being untitled. The distribution of &ldquo;This Is Your
Life&rdquo; given here only includes <b>Daïtro</b>&rsquo;s contribution.

Because <b>Daïtro</b> appears on two distinct split records that also feature
[Italian](/countries/italy) [punks](/genres/screamo) <b>Raein</b> &mdash; on
each of which <b>Daïtro</b> effectively contributes the same tunes &mdash; some
sources (including Rate Your Music and Discogs) confound and/or conflate the
two albums. This sometimes leads to inconsistent track listings. Also,
&ldquo;La Rock = Un Bateau Ivre&rdquo; is often simply &ldquo;Un Bateau
Ivre&rdquo; (or sometimes even &ldquo;La Rock&rdquo;).

The untitled <b>Daïtro</b> / <b>Raein</b> split has two distributions: the
first contains the entire record in VBR (V0) MP3, and the second contains
<b>Daïtro</b>&rsquo;s side of the split in CBR 320kb/s MP3, apparently sourced
from the &ldquo;2002-2005&rdquo; compilation record. Because both are of
dubious quality and origins, both are supplied.

<b>Raein</b>&rsquo;s contributions to &ldquo;The Harsh Words As The Sun&rdquo;
are sometimes listed as &ldquo;Döden Marscherar Åt Väst Part 1&rdquo; and
&ldquo;Döden Marscherar Åt Väst Part 2&rdquo;, as they are originally from
<b>Raein</b>&rsquo;s &ldquo;Döden Marscherar Åt Väst&rdquo;.

The distribution of &ldquo;The Emo Armageddon&rdquo; given here only includes
<b>Daïtro</b>&rsquo;s contribution.

<b>Ampere</b>&rsquo;s and <b>Daïtro</b>&rsquo;s contributions to their split
record are spread across two digital releases. <b>Ampere</b>&rsquo;s is
represented by the first two tracks of &ldquo;The First Five Years&rdquo;
(linked within the distributions below).

At least one release of the <b>Daïtro</b> / <b>Sed Non Satiata</b> split places
<b>Sed Non Satiata</b>&rsquo;s material _before_ <b>Daïtro</b>&rsquo;s, i.e.
swaps sides A and B. As a result, some listings will reflect that order. The
listing used here is the one used most often, and the one that appeared on the
original {{ time(t="2007") }} release.

The tracks on &ldquo;Y&rdquo; have no real titles, so some sources will give
different names to the tracks because the first two (here listed as
&ldquo;1a&rdquo; and &ldquo;1b&rdquo;) are sometimes treated as a single track:
&ldquo;1&rdquo;, or sometimes as two separate tracks, but instead titled
&ldquo;1&rdquo; and &ldquo;2&rdquo;. The listing here follows the listing on
<b>Daïtro</b>&rsquo;s official bandcamp page.

{{ discog() }}
