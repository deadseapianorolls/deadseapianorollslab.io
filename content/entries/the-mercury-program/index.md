+++
title = "The Mercury Program"
date = 2020-06-03

[taxonomies]
genres = [
    "post-rock",
    "experimental rock",
    "math rock",
    "rock",
    "post-hardcore",
    "punk",
    "pop",
]
first-letters = ["m"]
first-release-dates = ["1998"]
release-dates = [
    "1998",
    "1999",
    "2000",
    "2001",
    "2002",
    "2003",
    "2009",
    "2016",
]
labels = [
    "Tiger Style",
    "Lovitt Records",
    "Boxcar Records",
    "Kindercore Records",
    "Stiff Slack",
    "Counterflow Recordings",
    "Hello Sir Records",
]
countries = ["United States"]
provinces = ["Florida, United States"]
areas = ["Gainesville, Florida, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>The Mercury Program</b> (named for [Project
Mercury](https://en.wikipedia.org/wiki/Project_Mercury), whose flights launched
from the band&rsquo;s home state) is a {{ genre(name="post-rock") }} band from
[Gainesville, Florida](/areas/gainesville-florida-united-states). <b>The
Mercury Program</b> was highly influential to {{ genre(name="post-rock") }} and
its development, as well as having a continued influence on
instrumentally-focused {{ genre(name="experimental rock") }} and {{
genre(name="jazz") }}-inspired {{ genre(name="rock") }}. Although they have
been around since {{ time(t="1997", dt="1997-08") }}, their lineup has been
stable ever since {{ time(t="1999") }}, when <b>Whit Travisano</b> joined the
band as a dedicated {{ hs(name="vibraphonist") }}/{{ hs(name="keyboardist")
}}/{{ hs(name="pianist") }}. <b>The Mercury Program</b> were initially very
active around the turn of the millenium ({{ time(t="1997", dt="1997-08") }}
&ndash; {{ time(t="2003") }}), and then settled into a more casual cadence,
releasing full-length albums in {{ time(t="2009", dt="2009-11-24") }} and {{
time(t="2016", dt="2016-05-20") }}.

<b>The Mercury Program</b> are noted for their pervasive use of {{
hs(name="vibraphone") }}, although their sound very much centers around the {{
hs(name="drums") }} and (largely clean) {{ hs(name="bass guitar") }}. Not all
of their music is instrumental, as earlier works featured {{ hs(name="vocals")
}}. Earlier works also sometimes featured [distorted][distortion] {{
hs(name="guitar") }}/{{ hs(name="bass guitar") }} &mdash; the sound that they
are now known for did not fully develop until their {{ time(t="2001", dt="2001-04-24") }} EP, &ldquo;All The Suits Began To Fall
Off&rdquo;. The following {{ time(t="2002", dt="2002-09-10") }} LP, &ldquo;A
Data Learn The Language&rdquo;, would become their most well-known album,
cementing them in the pantheon of {{ genre(name="post-rock") }} greats and also
showcasing moderate influence from {{ genre(name="math rock") }} (c.f.
[Physics](/entries/physics)). Although <b>The Mercury Program</b> have been
compared to seminal [Chicago](/areas/chicago-illinois-united-states) {{
genre(name="post-rock") }}ers <b>Tortoise</b>, <b>The Mercury
Program</b>&rsquo;s musical approach is quite different: they lack the strong
{{ genre(name="krautrock") }}/{{ genre(name="electronic") }} influence of
<b>Tortoise</b>, instead opting for arrangements reminiscent of {{
genre(name="western chamber") }} music or a {{ genre(name="jazz") }} rhythm
section. In the same vein (and also reminiscent of {{ genre(name="jazz") }}) is
their focus on producing rich
[harmonies](https://en.wikipedia.org/wiki/Harmony), often awash with chords
that interweave driving basslines, repetitive riffs, and longer-form melodies
into a cohesive whole.

### notes on the distributions

Some sources list the release year of &ldquo;Lights Out In Georgia <abbr
title="backed with">b/w</abbr> Small Projects&rdquo; as {{ time(t="1997") }}, although it was apparently recorded in {{ time(t="December of that year", dt="1997-12") }}, so if the initial release was
in {{ time(t="1997") }}, it must have been _very_ late.

&ldquo;Confines Of Heat&rdquo; was a combined CD+DVD release, with the DVD
containing video of both bands. This listing only reflects the CD portion.
&ldquo;Crusading Theme&rdquo; was composed by <b>Savath &amp; Savalas</b>.

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
