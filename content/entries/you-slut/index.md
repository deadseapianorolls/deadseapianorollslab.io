+++
title = "You Slut!"
date = 2020-07-11

[taxonomies]
genres = [
    "math rock",
    "experimental rock",
    "post-hardcore",
    "post-rock",
    "punk",
    "rock",
    "pop",
]
first-letters = ["y"]
first-release-dates = ["2006"]
release-dates = ["2006", "2008", "2011"]
labels = ["Stressed Sumo Records", "Field Records", "Xtal Records"]
countries = ["England"]
provinces = ["Derbyshire, England", "Nottinghamshire, England"]
areas = ["Derby, Derbyshire, England", "Nottingham, Nottinghamshire, England"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>You Slut!</b> was an [English](/countries/england) {{
genre(name="math rock") }} band that formed some time in {{ time(t="2004") }}.
Their first release was in {{ time(t="2006") }}: an EP by the name of
&ldquo;Grit Eyed &amp; Greasy Tailed&rdquo;, which appears to only have been
released digitally as a set of MP3s. Later {{ time(t="that year", dt="2006")
}}, they released their first full-length and perhaps their most well-known
album, &ldquo;Critical Meat&rdquo;, featuring re-recordings of half of the
songs on &ldquo;Grit Eyed &amp; Greasy Tailed&rdquo;. Their other major release
was another full-length, released in {{ time(t="2011") }}, by the name of
&ldquo;Medium Bastard&rdquo;. {{ hs(name="Guitar") }}ist <b>Richard Collins</b>
has also played {{ hs(name="drums") }} for
[Derby](/areas/derby-derbyshire-england) {{ genre(name="indie rock") }} outfit
<b>Mascot Fight</b> since {{ time(t="2004") }}, and {{ hs(name="bass guitar")
}}ist <b>Sam Lloyd</b> now plays for
[Nottingham](/areas/nottingham-nottinghamshire-england) {{
genre(name="psychedelic rock") }} outfit <b>Church of the Cosmic Skull</b>.

The music of <b>You Slut!</b> is entirely instrumental, and portrays a kind of
raucous &ldquo;party math&rdquo; aesthetic; <i>cf.</i>
[<b>noumenon</b>](/entries/noumenon) or even
[Belfast](/areas/belfast-belfast-northern-ireland) {{ genre(name="post-rock")
}}ers <b>And So I Watch You From Afar</b>. The band themselves claim
[Baltimore](/areas/baltimore-maryland-united-states) {{ genre(name="math rock")
}} outfit <b>Oxes</b> as an influence, and this is clear in their music, with
the focus on meaty guitar riffs &mdash; but <b>You Slut!</b> take less of a cue
from {{ genre(name="noise rock") }} and more of a cue from {{
genre(name="post-rock") }}, although their goofball sense of humor _is_
something that they share with <b>Oxes</b>. &ldquo;Critical Meat&rdquo; is a
must-know for all fans of {{ genre(name="math rock") }}.

### notes on the distributions

&ldquo;On the Count of Thirteen&rdquo; from &ldquo;Critical Meat&rdquo; is
sometimes listed as &ldquo;On the Count of 13&rdquo; or similar.
&ldquo;Critical Meat&rdquo; was remastered in {{ time(t="2012") }}, and some
(but not all) of the distributions below are of the remastered version.

{{ discog() }}
