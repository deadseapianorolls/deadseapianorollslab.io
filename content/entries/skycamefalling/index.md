+++
title = "skycamefalling"
date = 2020-07-30

[taxonomies]
genres = [
    "metalcore",
    "post-hardcore",
    "experimental rock",
    "punk",
    "metal",
    "pop metal",
    "rock",
    "pop",
]
first-letters = ["s"]
first-release-dates = ["1997"]
release-dates = ["1997", "1998", "2000", "2003"]
labels = ["Ferret Music", "Good Life Recordings"]
countries = ["United States"]
provinces = ["New York, United States"]
areas = ["New York City, New York, United States"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>skycamefalling</b> was a {{ genre(name="metalcore") }}/{{
genre(name="post-hardcore") }} band from [Long Island
City](/areas/new-york-city-new-york-united-states) that formed some time in {{
time(t="1996") }}. Playing as a quintet, <b>skycamefalling</b> released a
four-song cassette demo in {{ time(t="1997") }}; half of these songs would be
re-recorded for their first &ldquo;real&rdquo; album, &ldquo;&hellip;To Forever
Embrace the Sun&rdquo;, which was released {{ time(t="the next year",
dt="1998") }}. For their debut and only (if you don&rsquo;t count
&ldquo;&hellip;To Forever Embrace the Sun&rdquo; as being a full-length)
full-length record, <b>skycamefalling</b> signed with [New
Jersey](/provinces/new-jersey-united-states) label [Ferret
Music](/labels/ferret-music) to release &ldquo;10.21&rdquo; in {{
time(t="2000", dt="2000-11-30") }}. <b>skycamefalling</b> disbanded in {{
time(t="2003") }}, but left [Ferret Music](/labels/ferret-music) with two rough
recordings that did not make it onto &ldquo;10.21&rdquo;; these recordings were
released as &ldquo;skycamefalling&rdquo; in {{ time(t="2003", dt="2003-04-01")
}}. Several of the band&rsquo;s members went on to form a band by the name of
<b>The Sleeping</b>, and <b>skycamefalling</b> later reunited as a quartet in
{{ time(t="2010") }}.

&ldquo;10.21&rdquo; is usually considered one of the masterpieces of the {{
genre(name="metalcore") }} genre. The most obvious point of comparison would be
[Salem, Massachusetts](/areas/essex-county-massachusetts-united-states) {{
genre(name="metalcore") }} outfit <b>Converge</b> (<i>cf.</i> particularly the
contemporary and even more influential &ldquo;Jane Doe&rdquo; ({{
time(t="2001", dt="2001-09-04") }})), but where <b>skycamefalling</b> may have
lacked the [technicality](/genres/mathcore) and crushing
[brutality](/genres/hardcore-punk) of <b>Converge</b>, they made up for it with
their willingness to experiment texturally, with varied vocal styles and
extended instrumental pieces that make use of disparate instruments like {{
hs(name="tabla") }} and {{ hs(name="piano") }} &mdash; as well as their own
form of brutality. This has led to <b>skycamefalling</b> being more closely
associated with the {{ genre(name="post-hardcore") }} genre, at least more so
than <b>Converge</b>. Their posthumous EP, &ldquo;skycamefalling&rdquo; ({{
time(t="2003", dt="2003-04-01") }}), reflects much of the same style seen in
&ldquo;10.21&rdquo; (understandable, as these two tracks were scrapped material
that would have ended up on &ldquo;10.21&rdquo;), but also interestingly
displays some of the band&rsquo;s more {{ genre(name="pop metal") }} side as
well.

{{ discog() }}
