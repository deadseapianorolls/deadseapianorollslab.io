+++
title = "Loma Prieta"
date = 2020-07-01

[taxonomies]
genres = [
    "screamo",
    "emoviolence",
    "powerviolence",
    "post-hardcore",
    "hardcore punk",
    "punk",
    "rock",
    "pop",
]
first-letters = ["l"]
first-release-dates = ["2005"]
release-dates = [
    "2005",
    "2006",
    "2007",
    "2008",
    "2009",
    "2010",
    "2011",
    "2012",
    "2013",
    "2015",
    "2020",
]
labels = [
    "Discos Huelga",
    "Deathwish",
    "React With Protest",
    "Ape Must Not Kill Ape",
    "Rok Lok Records",
    "Cosmic Note",
    "Daymare Recordings",
    "Hearts Bleed Blue",
    "Slave Union",
    "Inkblot",
]
countries = ["United States"]
provinces = ["California, United States"]
areas = ["San Francisco, California, United States"]

[extra]
last_edited = "2020-08-04"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Loma Prieta</b> is a {{ genre(name="screamo") }}/{{
genre(name="emoviolence") }} band from [San
Francisco](/areas/san-francisco-california-united-states), formed in {{ time(t="2005") }}. <b>Loma Prieta</b> formed out of a previous band
by the name of [<b>Sailboats</b>](/entries/sailboats), who only ever released
[a demo](https://rateyourmusic.com/release/single/sailboats/demo/) (in {{ time(t="2003") }}), and then mysteriously broke up; <b>Loma
Prieta</b> have been known to cover
[<b>Sailboats</b>](/entries/sailboats)&rsquo; &ldquo;Behind Trees&rdquo; when
playing live. The band released their demo in {{ time(t="late
2005", dt="2005-12") }}, and from then on, released an impressive amount of material, even
touring Europe in the summer of {{ time(t="2008") }} in support of
their LP by the name of &ldquo;Last City&rdquo;, which remains one of their
most revered works. Most of their material has been released through [Discos
Huelga](/labels/discos-huelga), which is run by {{ hs(name="drummer") }}
<b>Valeriano Saucedo</b>, although they are now signed to
[Deathwish](/labels/deathwish).

Starting with their {{ time(t="2009", dt="2009-05-18") }} LP, &ldquo;Dark
Mountain&rdquo;, <b>Loma Prieta</b> began working with <b>Comadre</b> and
<b>Everybody Row</b> {{ hs(name="guitar") }}ist <b>Jack Shirley</b> (although
the mixing was done by [<b>Ian</b> and <b>Jay Pellicci</b>](/entries/dilute)),
who runs The Atomic Garden recording studios in
[Oakland](/areas/oakland-california-united-states) (perhaps now best known for
producing [San Francisco](/areas/san-francisco-california-united-states) {{
genre(name="blackgaze") }} outfit <b>Deafheaven</b>&rsquo;s breakthrough
record, &ldquo;Sunbather&rdquo;). <b>Jack Shirley</b> then also recorded (and
was a guest performer on) <b>Loma Prieta</b>&rsquo;s next LP,
&ldquo;Life/Less&rdquo;, and all of their records after that (including the
very next record, and perhaps their most well-known, &ldquo;I.V.&rdquo;) have
been fully produced by Shirley. <b>Loma Prieta</b> shares members with numerous
other [hardcore](/genres/hardcore-punk) acts, perhaps most notably <b>Punch</b>
(with whom they did a split single), <b>Elle</b>, and <b>Beau Navire</b>. As
stated on their bandcamp page: &ldquo;<b>Loma Prieta</b> have been a mainstay
of the [Bay Area](/areas/san-francisco-california-united-states)
[hardcore](/genres/hardcore-punk) community for more than {{ time(t="half a decade", dt="P1826D") }}.&rdquo; Perhaps that statement can now
be extended to &ldquo;more than {{ time(t="a decade", dt="P3652D") }}&rdquo;.

The timespans listed here for the band&rsquo;s members are somewhat guesswork,
with the exception of the two constant members, <b>Sean Leary</b> and
<b>Valeriano Saucedo</b>.

<b>Loma Prieta</b>&rsquo;s music started out in a relatively modest {{
genre(name="emoviolence") }} vein; while quite a bit of their early material is
very good, it lacks the maturity that they would display on &ldquo;Last
City&rdquo; and onwards. It is this more mature material that has earned
<b>Loma Prieta</b> a place in the {{ genre(name="screamo") }} pantheon, with
its unique sound and incredible energy. Some of the hallmarks of <b>Loma
Prieta</b>&rsquo;s sound include:

- Incredibly noisy sound, including desperate-sounding screamed (and often
  [distorted][distortion]) vocals &mdash; <b>Loma Prieta</b> are almost
  _impossibly_ loud, incoporating [clipping][clipping] artifacts as part of
  their sound.
- Incorporation of strong melodies into the {{ genre(name="powerviolence")
  }}/{{ genre(name="emoviolence") }} framework, even sometimes to the point of
  being catchy.
- Difficult song structures and stop-start dynamics, including the intricate
  rhythms and time signature abuses that are characteristic of certain styles
  of {{ genre(name="emoviolence") }}, as exemplified by bands like
  <b>Ampere</b> and <b>Hassan I Sabbah</b>.

### notes on the distributions

All three tunes on the {{ time(t="2005", dt="2005-12") }} &ldquo;Demo&rdquo;
appear, re-recorded, on &ldquo;Our LP Is Your EP&rdquo;. This demo was released
on a business-card-shaped [CD-R](https://en.wikipedia.org/wiki/CD-R).

The deadseapianorolls distribution of &ldquo;The Emo Apocalypse&rdquo; listed
here only includes <b>Loma Prieta</b>&rsquo;s contribution.

The split record between [Mexico City](/provinces/mexico-city-mexico) {{
genre(name="emoviolence") }} outfit <b>Arse Moreira</b> and <b>Loma Prieta</b>
was never released, and also might not have even been planned to be a split
record(?)

The first track off of &ldquo;Last City&rdquo; (&ldquo;Worn Path&rdquo;) is
occasionally erroneously listed as &ldquo;Warn Path&rdquo;.

The split record between <b>Loma Prieta</b> and [Rockingham County, New
Hampshire](/areas/rockingham-county-new-hampshire-united-states) {{
genre(name="screamo") }} outfit <b>l&rsquo;antietam</b> is similarly dubious to
the split with <b>Arse Moreira</b>. Information about
<b>l&rsquo;antietam</b>&rsquo;s side of the split is scarce, and they may not
have ever recorded it. The inclusion of one of <b>l&rsquo;antietam</b>&rsquo;s
tracks on the listing (but **not** in the distribution) is a reflection of
[this YouTube video](https://www.youtube.com/watch?v=Yt5ab6X3Ehg), which has a
description that reads: &ldquo;L'Antietam in Portland, Oregon. This song is off
the split w/ Loma Prieta. Filmed by Alex Gaziano. July 2008.&rdquo;

The deadseapianorolls distribution of &ldquo;Discography 05-09&rdquo; only
includes &ldquo;The Needle In The Haystack (Unreleased Version)&rdquo;,
&ldquo;The Needle In The Haystack&rdquo;, and &ldquo;Running Over A Community
Service Worker (Get Get Go cover)&rdquo;, as all the other tracks are from
other releases.

The [Japanese](/countries/japan) release of &ldquo;I.V.&rdquo;, due to [Daymare
Recordings](/labels/daymare-recordings), has a bonus track at the end that is
just <b>Loma Prieta</b>&rsquo;s side of the split with fellow [San
Franciscan](/areas/san-francisco-california-united-states) {{
genre(name="hardcore punk") }}s <b>Punch</b> (the track itself being a cover of
one of <b>Punch</b>&rsquo;s tunes).

{{ discog() }}

[distortion]: https://en.wikipedia.org/wiki/Distortion_(music)
[clipping]: https://en.wikipedia.org/wiki/Clipping_%28audio%29
