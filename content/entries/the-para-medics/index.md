+++
title = "The Para-medics"
date = 2020-06-27

[taxonomies]
genres = [
    "math rock",
    "experimental rock",
    "post-hardcore",
    "punk",
    "rock",
    "pop",
]
first-letters = ["p"]
first-release-dates = ["2009"]
release-dates = ["2008", "2009", "2012", "2014"]
labels = ["Swerp Records"]
countries = ["United States"]
provinces = ["Illinois, United States"]
areas = ["Chicago, Illinois, United States"]

[extra]
last_edited = "2020-07-14"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>The Para-medics</b> was a {{ genre(name="math rock") }} band from
[Chicago](/areas/chicago-illinois-united-states), formed some time in {{ time(t="2008") }}. Originally releasing music independently, they
were one of the first signees of [Swerp Records](/labels/swerp-records) in
{{ time(t="2012") }}, a label specialising in the music of the
south suburbs of [Chicago](/areas/chicago-illinois-united-states). <b>The
Para-medics</b> dissolved some time around when Swerp dissolved in {{ time(t="2014") }} (see [Fecking Bahamas&rsquo;s retrospective on the
label][fb]; [archived][fb-archived]). With the band&rsquo;s dissolution, {{
hs(name="drummer") }} <b>Nnamdi Ogbonnaya</b> would go on to be the {{
hs(name="drummer") }} for, and later {{ hs(name="bass guitar") }}ist <b>Steve
Marek</b> would go on to be a {{ hs(name="bass guitar") }}ist for, current
[Chicago](/areas/chicago-illinois-united-states) {{ genre(name="math rock")
}}/{{ genre(name="jazz fusion") }} outfit
[<b>Monobody</b>](https://monobody.bandcamp.com/). {{ hs(name="Guitar") }}ist
<b>Dylan Piskula</b> went on to join
[Chicago](/areas/chicago-illinois-united-states) {{ genre(name="noise rock")
}}/{{ genre(name="sludge metal") }} outfit
[<b>Den</b>](https://den-noise.bandcamp.com/) as a {{ hs(name="bass guitar")
}}ist, later {{ hs(name="guitar") }}ist <b>Matthew Frank</b> plays as part of
[Chicago](/areas/chicago-illinois-united-states) {{ genre(name="midwest emo")
}} outfit [<b>Lifted Bells</b>](https://liftedbellsmusic.bandcamp.com/), and
earlier {{ hs(name="bass guitar") }}ist <b>Brian Baliga</b> has done solo work
under the name <b>Luscious Duncan</b>.

The music of <b>The Para-medics</b> takes after the music of their
[Chicago](/areas/chicago-illinois-united-states) (or perhaps, &ldquo;lava
land&rdquo;) forebears <b>piglet</b>; perhaps because the music is (very
nearly) entirely instrumental, some sources label <b>The Para-medics</b> as
&ldquo;{{ genre(name="progressive rock") }}&rdquo; or similar, but <b>The
Para-medics</b> are thoroughly {{ genre(name="math rock") }}. Like the music of
<b>piglet</b>, that of <b>The Para-medics</b> places equal emphasis on all
three components of its standard {{ genre(name="rock") }} instrumentation
(although they would later be a four-piece, including additional {{
hs(name="guitar") }}ist <b>Matthew Frank</b>): {{ hs(name="drumkit") }}, {{
hs(name="guitar") }}, and {{ hs(name="bass guitar") }}. They feature many of
the mainstays of the {{ genre(name="math rock") }} genre, including complex
song structures, agile (and often
[tapped](https://en.wikipedia.org/wiki/Tapping)) {{ hs(name="bass guitar") }}
and {{ hs(name="guitar") }} riffing, and oddball rhythms. Their music is also
highly dynamic, alternating between busy mid-tempo sections, sparse and quiet
sections, aggressive {{ genre(name="hardcore punk") }} riffing, and even partly
improvised &ldquo;freakout&rdquo; sections. Unfortunately, their music suffers
just a bit from the poor production; however, check out &ldquo;Patsy Cline Gun
Squid&rdquo; and later releases for some production that is improved over
&ldquo;Switch It On Em!!!&rdquo;.

### notes on the distributions

The information on the first two releases (&ldquo;The Para-medics&rdquo; and
&ldquo;Switch It On Em!!!&rdquo;) is scarce and dubious. &ldquo;Burkina Faso
(We Can Dance)&rdquo; [showed up at some point on their bandcamp
page][bandcamp-archived], and the release date there is also used here.
However, the other two tracks only seem to appear [on their MySpace
page][myspace] ([archived][myspace-archived]). Also, the cover art used here
for &ldquo;Switch It On Em!!!&rdquo; appears to have been used for &ldquo;The
Para-medics&rdquo; (listed as &ldquo;The Para-medics EP&rdquo; on their
bandcamp). Most sources list the release date of &ldquo;Switch It On
Em!!!&rdquo; as {{ time(t="2006-06-06", dt="2006-06-06") }} (as it was listed
on their bandcamp), but the band apparently had only formed by {{ time(t="2008") }} &mdash; this date is presumably [a
joke](https://en.wikipedia.org/wiki/Number_of_the_Beast).
[MusicBrainz][musicbrainz] lists the release date as {{ time(t="2009") }}, which makes more sense, and also agrees with their
MySpace page, so that is the date used here.

{{ discog() }}

[fb]: http://feckingbahamas.com/focus-on-some-serious-swerp-remembering-the-music-of-swerp-records
[fb-archived]: https://web.archive.org/web/20191216164624/http://feckingbahamas.com/focus-on-some-serious-swerp-remembering-the-music-of-swerp-records
[bandcamp-archived]: https://web.archive.org/web/20100106140305/http://thepara-medics.bandcamp.com/
[myspace]: https://myspace.com/musicforthehardofhearing/music/album/the-para-medics-5693554
[myspace-archived]: https://web.archive.org/web/20200627004706/https://myspace.com/musicforthehardofhearing/music/album/the-para-medics-5693554
[musicbrainz]: https://musicbrainz.org/release-group/255f3adf-35cd-4614-821a-1fe107a65245
