+++
title = "Corea"
date = 2020-06-24

[taxonomies]
genres = [
    "screamo",
    "post-rock",
    "post-hardcore",
    "experimental rock",
    "punk",
    "noise",
    "sound collage",
    "harsh noise",
    "ambient",
    "rock",
    "EDM",
    "pop",
]
first-letters = ["c"]
first-release-dates = ["2004"]
release-dates = ["2004", "2006"]
labels = [
    "Existencia",
    "Error Records",
    "Astoria Records",
    "Escucha! Records",
    "Horror Vacui Theatre",
    "I’ve Come For Your Children",
    "TDD Records",
    "MeatCubeLabel",
]
countries = ["Spain"]
provinces = ["Alicante, Spain", "Provincia de León, Spain"]
areas = [
    "El Baix Segura, Alicante, Spain",
    "Valdesabero, Provincia de León, Spain",
]

[extra]
last_edited = "2020-06-27"
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Corea</b> (presumably named for [the region of East
Asia](https://es.wikipedia.org/wiki/Corea), but possibly named for [Chick
Corea](https://en.wikipedia.org/wiki/Chick_Corea)) was a {{
genre(name="screamo") }}/{{ genre(name="post-rock") }} band from
[Alicante](/provinces/alicante-spain), and later from the north of la
[Provincia de León](/provinces/provincia-de-leon-spain) (according to [their
MySpace page](https://myspace.com/coreanomolan)), formed in {{ time(t="2003") }}.

<b>Corea</b> only released two records, both of which were full-lengths. The
first, &ldquo;Los peores 7 km de mi vida&rdquo; (in English: &ldquo;The worst
7km of my life&rdquo;), mixes {{ genre(name="screamo") }} with {{
genre(name="post-rock") }}, but perhaps not in the way that you would expect if
you had listened to, say, <b>Envy</b> (who more-or-less originated the style).
&ldquo;Los peores 7 km de mi vida&rdquo; sounds perpetually cold and
unforgiving, with extremely harsh distorted guitars and screaming popping up
seemingly randomly throughout the record. Other parts make use of some {{
genre(name="post-rock") }} elements as well as heavy elements of {{
genre(name="noise") }}/{{ genre(name="harsh noise") }} and {{ genre(name="sound
collage") }}. Some of the use of sampling is comparable to the music of
<b>Jeromes Dream</b> (particularly &ldquo;Seeing Means More Than
Safety&rdquo;), and the mixture of {{ genre(name="post-hardcore") }} with {{
genre(name="post-rock") }} will remind some of [<b>The Newfound Interest In
Connecticut</b>](/entries/the-newfound-interest-in-connecticut) (although
&ldquo;Tell Me About The Long Dark Path Home&rdquo; would not be released until
a year later). &ldquo;Los peores 7 km de mi vida&rdquo;, however, incorporates
a variety of textures that surpasses the output of both of these bands, and the
song structures are perhaps more complex. The production, however, is
unfortunately quite poor (although, arguably, this enhances the harshness and
abrasion of the record). <b>Corea</b>&rsquo;s second and final album,
&ldquo;Quien encuentra a la madre conoce a los hijos&rdquo; (in English:
&ldquo;\[One\] who finds the mother knows the children), comes off as more
straightforward, being more easily compared to the aforementioned <b>Envy</b>.
The production, however, is considerably improved over the first record, and
this album is still solid as well. Interestingly enough, this album ends with
an {{ genre(name="EDM") }} track.

### notes on the distributions

The original issue of &ldquo;Los peores 7 km de mi vida&rdquo; actually spells
the title as &ldquo;Los peores 7 peores Km de mi vida&rdquo; (see [side of
original CD cover](los-peores-7-side.jpg)). This is presumably a typo.

{{ discog() }}
