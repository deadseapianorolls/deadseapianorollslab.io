+++
title = "saleontomorrow"
date = 2020-04-25

[taxonomies]
genres = [
    "math pop",
    "math rock",
    "experimental rock",
    "post-rock",
    "post-hardcore",
    "punk",
    "midwest emo",
    "indie rock",
    "rock",
    "pop",
]
first-letters = ["s"]
first-release-dates = ["2005"]
release-dates = ["2005", "2006"]
labels = ["Backlashed", "Smalltown America"]
countries = ["England"]
provinces = ["Norfolk, England"]
areas = ["Norwich, Norfolk, England"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="webp") }}

<b>saleontomorrow</b> was an [English](/countries/england) {{
genre(name="math rock") }}/{{ genre(name="math pop") }} band that, along with
<b>Colour</b>, prefigured the [English](/countries/england) {{
genre(name="math pop") }} scene of the late 2000s and early 2010s. Their sound
was somewhat more clearly {{ genre(name="punk") }}-y (or rather, early-2000s {{
genre(name="post-hardcore") }}) than <b>Colour</b>&rsquo;s, and is
characterized by snaky {{ hs(name="guitar") }} and {{ hs(name="bass guitar") }}
lines, dynamics that borrow somewhat from {{ genre(name="post-rock") }}, and
<b>Henry Tremain</b>&rsquo;s (now of <b>TTNG</b>) vocal style. Unfortunately,
<b>saleontomorrow</b>&rsquo;s career ended prematurely and tragically with the
death of {{ hs(name="bassist") }} <b>David Thorpe</b> in a motorcycling
accident (R.I.P.).

What we still have of <b>saleontomorrow</b>&rsquo;s recorded output is
unfortunately little and low in fidelity, as it is clear that this band were
very precocious, and a fitting forebear of the [English](/countries/england) {{
genre(name="math pop") }} scene.

Also worth noting is <b>Pennines</b>, the band that Tremain formed in between
<b>saleontomorrow</b> and joining <b>TTNG</b>.

Two photos of the band have been ripped from Last.fm and can be viewed here:

* [saleontomorrow](saleontomorrow-0.webp)
* [saleontomorrow \[1\]](band.webp)

### notes on the distributions

<b>Brailing</b>&rsquo;s side of the split with <b>saleontomorrow</b> is not
included &mdash; indeed, all information about the other side of the split is
unknown.

All entries in the discography are tentative, and may be incomplete.
Information about the band and their music is scarce.

The distribution for the track &ldquo;intheunlikelyeventofafire&rdquo; (the
2<sup>nd</sup> on the &ldquo;\[demo\]&rdquo;) has an effective sample rate of
22.05<abbr title="thousand samples per second">kHz</abbr>, and a bitrate of
96<abbr title="kilobits per second">kb/s</abbr>.

{{ discog() }}
