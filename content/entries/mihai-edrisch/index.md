+++
title = "Mihai Edrisch"
date = 2020-08-20

[taxonomies]
genres = [
    "screamo",
    "post-rock",
    "math rock",
    "experimental rock",
    "post-hardcore",
    "emoviolence",
    "powerviolence",
    "mathcore",
    "hardcore punk",
    "punk",
    "metalcore",
    "experimental metal",
    "rock",
    "metal",
    "pop",
]
first-letters = ["m"]
first-release-dates = ["2003"]
release-dates = ["2003", "2005"]
labels = [
    "Alchimia",
    "Purepainsugar",
    "Echo Canyon",
    "Denovali Records",
    "GodsChildMusic",
]
countries = ["France"]
provinces = ["Rhône, France"]
areas = ["Lyon, Rhône, France"]
+++

{{ artist() }}

<!-- more -->

{{ band_photo(ext="jpg") }}

<b>Mihai Edrisch</b> was a {{ genre(name="screamo") }} band from
[Lyon](/areas/lyon-rhone-france) that formed some time in {{ time(t="2002") }}.
The band&rsquo;s entire recorded output consists of two LPs: one released in {{
time(t="2003") }} and one released in {{ time(t="2005", dt="2005-03") }}. {{
hs(name="Vocal") }}ist <b>Johan Girardeau</b> would go on to form
[blackened](/genres/black-metal) {{ genre(name="screamo") }} outfit
<b>Celeste</b> with later <b>Mihai Edrisch</b> {{ hs(name="guitar") }}ist
<b>Guillaume Rieth</b>, with <b>Johan Girardeau</b> on {{ hs(name="vocal") }}
and {{ hs(name="bass guitar") }} duties, and <b>Guillaume Rieth</b> on {{
hs(name="guitar") }}. <b>Mihai Edrisch</b> {{ hs(name="drummer") }} and {{
hs(name="pianist") }} <b>Benoît Desvignes</b> is perhaps best known for his
work as the {{ hs(name="drummer") }} of fellow [Lyon](/areas/lyon-rhone-france)
{{ genre(name="screamo") }} outfit [<b>Daïtro</b>](/entries/daitro).

The music of <b>Mihai Edrisch</b> is often compared to that of their
[French](/countries/france) {{ genre(name="screamo") }}/{{
genre(name="post-hardcore") }} peers, like [<b>Daïtro</b>](/entries/daitro) and
[Toulousian](/areas/toulouse-haute-garonne-france) {{ genre(name="screamo")
}}/{{ genre(name="post-rock") }}ers [<b>Sed Non
Satiata</b>](https://sednonsatiata.bandcamp.com/). It is true that some of the
comments made on [<b>Daïtro</b>](/entries/daitro) apply here as well; in
particular, <b>Mihai Edrisch</b> fill out many of their arrangements with
complex and winding sequences of [block chords][block-chord] that take up the
middle-to-high register of the {{ hs(name="guitar") }}, and their records have
a production style that is reminiscent of the moody ambience &mdash; throughout
both soft and loud sections &mdash; of <b>Envy</b>. But the music of <b>Mihai
Edrisch</b>, while it may sound similar to that of their
[French](/countries/france) peers on the surface, is unique in its commitment
to tight, yet quite complex, song structures and riffs. Because of this,
<b>Mihai Edrisch</b> are here tagged with {{ genre(name="math rock") }}, {{
genre(name="emoviolence") }}, and {{ genre(name="mathcore") }}, none of which
they are typically associated with. The unusual rhythms and general
&ldquo;skronkiness&rdquo; of many of <b>Mihai Edrisch</b>&rsquo;s {{
hs(name="guitar") }} riffs recalls {{ genre(name="math rock") }} influence, and
even {{ genre(name="mathcore") }} &mdash; even if <b>Mihai Edrisch</b> cannot
realistically be considered a {{ genre(name="metalcore") }} band. And the {{
hs(name="bass guitar") }} playing and {{ hs(name="drumming") }} of this
band are no slouches, either; many of the individual performances on <b>Mihai
Edrisch</b>&rsquo;s records are nothing short of athletic, often featuring the
high tempi, the maximalist {{ hs(name="drumming") }} &mdash; and even sometimes
quasi-[blast beats][blast-beat] &mdash; typical of {{ genre(name="emoviolence")
}}.

{{ discog() }}

[block-chord]: https://en.wikipedia.org/wiki/Block_chord
[blast-beat]: https://en.wikipedia.org/wiki/Blast_beat
